<?php
	/**
	* @since 1.0
	* @author Bhaskar Pramanik <splashingbee@gmail.com>
	* @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
	*/

	// Define web ROOT
	define ( 'ROOT', dirname( __FILE__ ) );
	require_once ROOT.'/lib/classes/impl/AnimusWebApp.php';
	
	// Start AnimusWebApp
	AnimusWebApp::startApp();