<?php
/**
 * 
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 * 
 * Description of User :
 * Holds the User object
 */

class User {
	//Representation of User
	private $_userIdentifier;
	private $_alias;
	private $_auth_signature;
	
	public function __construct() {
	  LogInfoHandler::log();
	}
	
	public function getUserIdentifier() {
	  LogInfoHandler::log();
		return $this->_userIdentifier;
	}

	public function getAlias() {
		return $this->_alias;
	}
	
	public function setAuthSignature( $authSignature ){
	  LogInfoHandler::log();
		$this->_auth_signature = $authSignature;
	}
	
	public function getAuthSignature(){
	  LogInfoHandler::log();
		return $this->_auth_signature;
	}

	public function setUserIdentifier( $userIdentifier ) {
	  LogInfoHandler::log();
		$this->_userIdentifier = $userIdentifier;
	}

	public function setAlias( $alias ) {
		$this->_alias = $alias;
	}
	
	public function __destruct() {
	  LogInfoHandler::log();
	}
}
