<?php
/**
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 * 
 * Description of HttpResponse :
 * Provides an object to hold and manipulate the HTTP Response
 */
require_once 'HeaderInfoHandler.php';
class HTTPResponse {

    private $_headerArray;
    private $_session;
    private $_headerInfoHandler;
    
    public function __construct() {
       LogInfoHandler::log();
       $this->_headerArray = array();
    }
    
    public function getHeaderInfoHandler() {
        LogInfoHandler::log();
        if( is_null( $this->_headerInfoHandler ) ) {
            $this->_headerInfoHandler = new HeaderInfoHandler();
        }
        return $this->_headerInfoHandler;
    }
    
    public function prepareHTTPHeader( $serverProtocolVersion, $headerCode ){
        LogInfoHandler::log();
        $headerString = $this->getHeaderInfoHandler()->generateHTTPHeader( $serverProtocolVersion, $headerCode );
        $this->setHeaderArray( $headerString );
    }
    
    public function setHeaderArray( $headerString ){
        LogInfoHandler::log();
        array_push( $this->_headerArray, $headerString );
    }
    
    public function getHeaderArray(){
        return $this->_headerArray;
    }
    
    public function injectSession( Session $session ){
        $this->_session = $session;
    }
    
    public function sendHeaders(){
        $headerArray = $this->getHeaderArray();
        foreach( $headerArray as $header ){
            // Sending out the headers
            header( $header );
        }
    }
}
