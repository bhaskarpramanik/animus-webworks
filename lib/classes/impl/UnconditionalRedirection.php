<?php

/**
 * 
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 * 
 * Description of UnconditionalRedirect
 * Redirection handling class - This class is meant to be used to process unconditional redirections.
 * This Handler doesn't preserve the GET/POST array which might be populated as part of user's input
 */
require_once CLASSPATH_IMPL.'/RedirectionImpl.php';
class UnconditionalRedirection extends RedirectionImpl{
    
    private $_initial_URL;
    private $_callback_URL;
    private $_redirection_cause;
    private $_redirection_header_code;
    private $_redirection_location;
    private $_redirection_type;
    
    public function __construct(){
        LogInfoHandler::log();
        $this->_redirection_header_code = array();
    }
    
    public function setInitialURL( $initial_url ) {
        LogInfoHandler::log();
        $this->_initial_URL = $initial_url;
    }
    
    public function setCallbackURL( $callback_url ) {
        LogInfoHandler::log();
        $this->_callback_URL = $callback_url;
    }

    public function setRedirectionCause( $redirect_cause ) {
        LogInfoHandler::log();
        $this->_redirection_cause = $redirect_cause;
    }

    public function setRedirectionHeaderCode( $redirect_header_code ) {
        LogInfoHandler::log();
        array_push( $this->_redirection_header_code ,$redirect_header_code );
    }

    public function setRedirectionLocation( $redirect_location ) {
        LogInfoHandler::log();
        $this->_redirection_location = $redirect_location;
    }

    public function setRedirectionType( $redirect_type ) {
        LogInfoHandler::log();
        $this->_redirection_type = $redirect_type;
    }

    public function getInitialURL() {
        LogInfoHandler::log();
        return $this->_initial_URL;
    }
    
    public function getCallbackURL(){
        LogInfoHandler::log();
        return $this->_callback_URL;
    }
    
    public function getRedirectionCause() {
        LogInfoHandler::log();
        return $this->_redirection_cause;
    }
    
    public function getRedirectionHeaderCode() {
        LogInfoHandler::log();
        return $this->_redirection_header_code;
    }
    
    public function getRedirectionLocation() {
        LogInfoHandler::log();
        return $this->_redirection_location;
    }
    
    public function getRedirectionType() {
        LogInfoHandler::log();
        return $this->_redirection_type;
    }
    
    public function __destruct(){
        LogInfoHandler::log();
    } 
}
