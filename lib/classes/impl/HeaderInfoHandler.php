<?php
/**
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 */
class HeaderInfoHandler {
	/*
	 * Contain defined header constants
	 */
	const HTTP_200 = 'Ok';
	const HTTP_301 = 'Moved Permanently';
	const HTTP_302 = 'Moved Temporarily';
	const HTTP_400 = 'Bad Request';
	const HTTP_401 = 'Unauthorized';
	const HTTP_403 = 'Forbidden';
	const HTTP_404 = 'Not Found';
	const HTTP_500 = 'Internal Server Error';
	const HTTP_503 = 'Service Available';

	/*
	 * Contain location header path
	 */
	private $_locationHeaderPath;

	/*
	 * Contain header array
	 */
	private $_headerArray;

	public function __construct() {
		LogInfoHandler::log();
		$this->_headerArray = array();
	}

	public function getResponseCode( $headerCode ){
		LogInfoHandler::log();
		switch( $headerCode ) {
			case 200: return self::HTTP_200;
				break;
			
			case 301: return self::HTTP_301;
				break;
			
			case 302: return self::HTTP_302;
				break;
			
			case 400: return self:: HTTP_400;
				break;
			
			case 401: return self:: HTTP_401;
				break;
			
			case 403: return self:: HTTP_403;
				break;
			
			case 404: return self:: HTTP_404;
				break;
			
			case 500: return self:: HTTP_500;
				break;
			
			case 503: return self:: HTTP_503;
				break;
			
		}
	}

	public function generateLocationHeader( $_httpProtocolVersion, $_locationHeaderOverrideFlag = true ) {
		LogInfoHandler::log();
		// If redirection is forbidden by settings no location header would be generated
		$_redirect_location   = $this->getLocationHeaderPath();
		$locationHeaderString = 'Location: '.$_redirect_location;
		if(!is_null( $this->getLocationHeaderPath() ) && $_locationHeaderOverrideFlag ) {
			$this->setHeaderArray($locationHeaderString);
		}
		return;
	}

	/*
	 * Getters and Setters
	 */

	public function getLocationHeaderPath() {
		LogInfoHandler::log();
		return $this->_locationHeaderPath;
	}

	public function getHeaderArray() {
		LogInfoHandler::log();
		return $this->_headerArray;
	}

	public function generateHTTPHeader( $serverProtocol, $headerCode ){
		LogInfoHandler::log();
		$headerString = $serverProtocol . ' ' .$headerCode . ' ' . $this->getResponseCode( $headerCode );
		LogInfoHandler::log($headerString);
		return $headerString;
	}

	public function setLocationHeaderPath( $locationHeaderPath ) {
		LogInfoHandler::log();
		$this->_locationHeaderPath = $locationHeaderPath;
	}

	public function setHeaderArray($headerString) {
		LogInfoHandler::log();
		array_push( $this->_headerArray, $headerString );
	}

	public function getStaticAssetHeader($ext){
		return 'Content-Type: '.constant( strtoupper( $ext ) . '_CONTENT').'; charset=' . DEFAULT_CHARSET;
	}
}
