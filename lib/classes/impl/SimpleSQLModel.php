<?php
/**
 * Description of SimpleSQLModel
 *
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 */
class SimpleSQLModel extends ModelImpl {
	//put your code here
	private $_queryString; // Pass a query string here ...
	private $_outputArray;
	private $_emptyResultSetFlag;
	private $_rowCount;
	private $_dbTransactionStatus;
	private $_SQLConnectionObject;
	
	public function __construct() {
		parent::__construct();
		$this->_SQLConnectionObject = null;
		$this->_emptyResultSetFlag  = null;
		$this->_outputArray         = array();
		parent::setSQLWorkflow();
	}
	
	public function defineWorkflow() {
		$connObj = parent::getWorkflowArtifact( 'connectionObject' );
		
		// Calling prepare/execute on pre-generated queries
		$stmt = $connObj->prepare( $this->_queryString );
		$stmt->execute();
		$SQLerrorInfo = $stmt->errorInfo();
		if( 0 === $stmt->rowCount() ){
			$this->setEmptyResultSet();
		}
		else if( 0 !== $stmt->rowCount() ){
			$this->_emptyResultSetFlag = false;
			$this->setRowCount($stmt->rowCount() );
		}
		if( '0000' !== $SQLerrorInfo[0] ){
			try{
				throw new SQLException($SQLerrorInfo[0].' '.$SQLerrorInfo[1].' '.$SQLerrorInfo[2]);
			} catch ( SQLException $ex ) {
				LogInfoHandler::log(' SQL operation unsuccessful!' );
				LogInfoHandler::log( $SQLerrorInfo );
				$this->setDbTransactionStatus( false );
			}
		}
		else{
			while($row = $stmt->fetch( PDO::FETCH_ASSOC ) ){
				$this->setOutputArray( $row );
			}
			LogInfoHandler::log( 'Database operation successful!' );
			$this->setDbTransactionStatus( true );
		}
		
	}
	
	public function callWorkflow() {
		$this->defineWorkflow();
		return $this->getDbTransactionStatus();
	}
	
	public function resetModel() {
		$this->_SQLConnectionObject = null;
		$this->_outputArray = array();
	}
	
	function getQueryString() {
		return $this->_queryString;
	}

	function getOutputArray() {
		return $this->_outputArray;
	}

	function getEmptyResultSetFlag() {
		return $this->_emptyResultSetFlag;
	}

	function getRowCount() {
		return $this->_rowCount;
	}

	function getDbTransactionStatus() {
		return $this->_dbTransactionStatus;
	}

	function getSQLConnectionObject() {
		return $this->_SQLConnectionObject;
	}

	function setQueryString( $queryString ) {
		$this->_queryString = $queryString;
	}

	function setOutputArray( $outputArray ) {
		array_push( $this->_outputArray,$outputArray );
	}

	function setEmptyResultSetFlag( $emptyResultSetFlag ) {
		$this->_emptyResultSetFlag = $emptyResultSetFlag;
	}

	function setRowCount( $rowCount ) {
		$this->_rowCount = $rowCount;
	}

	function setDbTransactionStatus( $dbTransactionStatus ) {
		$this->_dbTransactionStatus = $dbTransactionStatus;
	}

	function setSQLConnectionObject( $SQLConnectionObject ) {
		$this->_SQLConnectionObject = $SQLConnectionObject;
	}


	
}
