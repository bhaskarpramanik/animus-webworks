<?php
/**
 * Description of TempErrorRedirection
 *
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 */
class TempErrorRedirection extends TemporaryRedirection{
    private $_redirectionFlag;
    
    public function __construct() {
        parent::__construct();
        $this->_redirectionFlag = false;
    }
    
    public function getCallbackURL() {
        LogInfoHandler::log();
        return parent::getCallbackURL();
    }

    public function getInitialURL() {
        LogInfoHandler::log();
        return parent::getInitialURL();
    }

    public function getRedirectionCause() {
        LogInfoHandler::log();
        return parent::getRedirectionCause();
    }

    public function getRedirectionHeaderCode() {
        LogInfoHandler::log();
        return parent::getRedirectionHeaderCode();
    }

    public function getRedirectionLocation() {
        LogInfoHandler::log();
        return parent::getRedirectionLocation();
    }

    public function getRedirectionType() {
        LogInfoHandler::log();
        return parent::getRedirectionType();
    }

    public function getUserDataArray() {
        LogInfoHandler::log();
        return parent::getUserDataArray();
    }

    public function setCallbackURL( $callback_url ) {
        LogInfoHandler::log();
        parent::setCallbackURL( $callback_url );
    }

    public function setHeaderCode( $header_code ) {
        LogInfoHandler::log();
        parent::setHeaderCode( $header_code );
    }

    public function setInitialURL( $initial_url ) {
        LogInfoHandler::log();
        parent::setInitialURL( $initial_url );
    }

    public function setRedirectionCause( $redirect_cause ) {
        LogInfoHandler::log();
        parent::setRedirectionCause( $redirect_cause );
    }

    public function setRedirectionHeaderCode( $redirect_header_code ) {
        LogInfoHandler::log();
        parent::setRedirectionHeaderCode( $redirect_header_code );
    }

    public function setRedirectionLocation( $redirect_location ) {
        LogInfoHandler::log();
        parent::setRedirectionLocation( $redirect_location );
    }

    public function setRedirectionType( $redirect_type ) {
        LogInfoHandler::log();
        parent::setRedirectionType( $redirect_type );
    }

    public function setUserDataArray( $user_data_array ) {
        LogInfoHandler::log();
        parent::setUserDataArray( $user_data_array );
    }

    public function stackHTTPHeaders( $header_code ) {
        LogInfoHandler::log();
        parent::stackHTTPHeaders( $header_code );
    }
    
    public function getRedirectionFlag() {
        LogInfoHandler::log( $this->_redirectionFlag );
        return $this->_redirectionFlag;
    }

    public function setRedirectionFlag() {
        LogInfoHandler::log();
        //$this->_redirectionFlag = constant( "REDIRECT_".$this->getRedirectionHeaderCode() );
        $this->_redirectionFlag = REDIRECT_500;
    }
}
