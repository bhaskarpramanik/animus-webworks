<?php

/**
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 * XMLParser extends XMLReader
 * doc: http://php.net/manual/en/book.xmlreader.php
 */

require_once dirname( dirname( __FILE__ ) ).'/exceptions/NativeException.php';
class XMLParser extends XMLReader{
	
	// Name of the XML file to be read
	private $_XMLName;
	
	// Path of the XML file to be read
	private $_XMLPath;
	
	// Set URL mode - Default - False
	private $_URLMode;
	
	// Set URL path
	private $_URLPath;
	
	private $_XMLReader;
	
	private $_isValid;
	
	private $_inputExists;
	
	// Set File Path
	private $_filePath;
	
	public function __construct() {
		LogInfoHandler::log();
	}

	public function setXMLParams( $XMLPath = null, $XMLName = null, $isURL = false, $XMLURL = null ) {
	//Locals
	LogInfoHandler::log();
	$_exception_flag = false;
	$_exception_message = '';
	$return_flag = false;

	// This reader can read XML directly from a URL and parse it
	// If it is needed to run this module in URL mode, set URL mode as true, while setting URL params

	if( $_exception_flag == false ) {
		if( $isURL ) {
			if( is_null( $XMLURL ) ) {
				$_exception_flag = true;
				$_exception_message = 'URL to read XML file from, is missing, or not configured';
			}
			else{
				$this->_URLPath = $XMLURL;
				$return_flag = true;
			}
		}
		else{
		   // URL mode is false
		   // Setting this parser to work in file-system mode
		   //$this->_URLMode = $isURL;
		   if( is_null( $XMLName ) ) {
			  $_exception_flag = true;
			  $_exception_message = 'XML file name not defined!';
		   }
		   else if( is_null( $XMLPath ) ) {
			  $_exception_flag = true;
			  $_exception_message = 'XML file name not defined!';
		   }
		   else if( is_null( $XMLPath )&&is_null( $XMLName ) ) {
			  $_exception_flag = true;
			  $_exception_message = 'XML file name and file path is not defined!';
		   }
		   else{
			  $_exception_flag = false;
			  $this->_XMLName = $XMLName;
			  $this->_XMLPath = $XMLPath;
			  $return_flag = true;
		   }
		}
	}
	else if( $_exception_flag ) {
		try{
		   throw new NativeException( $_exception_message );
		}
		catch ( NativeException $ex ) {
		   $ex->log();
		   $return_flag = false;
		}
	}
	else $return_flag = true;
	return $return_flag;
	}
	
	public function loadXML() {
		LogInfoHandler::log();
		if( $this->fileCheck() ) {
			// Check the file ext in case file exists
			$fileExt = '/.xml/';
			// Check if input file has .xml extension
			if( preg_match( $fileExt,$this->_XMLName ) ) {
				parent::open( $this->_filePath );
				parent::setParserProperty( XMLReader::SUBST_ENTITIES,true );
				// If the user has declared the xml as valid ( Has a DTD declaration )
				if( $this->_isValid ) {
				// Set validate parser property to true
				parent::setParserProperty( XMLReader::VALIDATE, true );
				// Check if the loaded XML is valid
					if( !$this->_XMLReader->isValid() ) {
						$_exception_message = 'Invalid XML provided';
						try{
							throw new NativeException( $_exception_message );
						} catch ( NativeException $ex ) {
							$ex->log();
							return false;
						}
					}
				}
				else{
				   return true;
				}
			}
			else if( isset( $this->_URLPath ) ) {
				parent::open( $this->_URLPath );
				parent::setParserProperty( XMLReader::SUBST_ENTITIES,true );
				// If the user has declared the xml as valid ( Has a DTD declaration )
				if( $this->_isValid ) {
				// Set validate parser property to true
				parent::setParserProperty( XMLReader::VALIDATE, true );
				// Check if the loaded XML is valid
					if( !$this->_XMLReader->isValid() ) {
						$_exception_message = 'Invalid XML provided';
						try{
							throw new NativeException( $_exception_message );
						} catch ( NativeException $ex ) {
							$ex->log();
							return false;
						}
					}
				}
				else{
				   return true;
				}
			}
			else{
			   $_exception_message = 'Provided file is not an XML !';
			   try{
				   throw new NativeException( $_exception_message );
				} catch ( NativeException $ex ) {
				   $ex->log();
				   return false;
				}
			}
		}
		else{
		  // Set exception message in case no XML is found.
		  $_exception_message = 'No file to load !';
		  // Throw exception
		  try{
				throw new NativeException( $_exception_message );
			} catch ( NativeException $ex ) {
				$ex->log();
				return false;
			}
		}
	}
	public function setXMLValid() {
		$this->_isValid = true;
	}
	
	public function getElementName() {
	  // public instance variable inherited from parent
		return $this->localName;
	}
	
	public function hasAttributes() {
		return $this->hasAttributes;
	}
	
	public function getAttributeByName( $attributeName ) {
		return parent::getAttribute( $attributeName );
	}
	
	public function readInnerXML() {
		return parent::readInnerXML();
	}
	
	public function readOuterXML() {
		return parent::readOuterXML();
	}
	
	public function readString() {
		return parent::readString();
	}
	
	public function expand( $basenode = NULL ) {
		return parent::expand();
	}
	
	// Parser events
	
	public function isTagNoNode() {
		if(  0 === $this->nodeType  )
			return true;
		else return false;
	}
	
	public function isTagStartElement() {
		if(  1 === $this->nodeType  ) {
			return true;
		}
		else return false;
	}
	
	public function isTagEndElement() {
		if(  15 === $this->nodeType  )
			return true;
		else return false;
	}
	
	public function isTagAttribute() {
		if(  2 === $this->nodeType  )
		  return true;
		else return false;	
	}
	
	public function isTagWhitespace() {
		if(  14 === $this->nodeType  ) {
			return true;
		}
		else return false;
	}
	
	public function isTagText() {
		if(  3 === $this->nodeType  )
			return true;
		else return false;
	}
	
	public function isTagCDATA() {
		if(  4 === $this->nodeType  ) 
			return true;
		else return false;
	}
	
	public function fileCheck() {
	  /*
		* Tries to find XML at the given path
		* if - false - throws exception.
		* else - sets the inputExists as true
		*/
		LogInfoHandler::log();
	  
		if( $this->_URLMode ) {
		   $this->_filePath = $this->_XMLURLPath;
		}
		else{
		   $this->_filePath = $this->_XMLPath.'/'.$this->_XMLName;
			LogInfoHandler::log( $this->_filePath );
		}
	  
		if( file_exists( $this->_filePath ) ) {
		   $this->_inputExists = true;
		}
		else{
		   $this->_inputExists = false;

		}
		return $this->_inputExists;
	}
}
?>