<?php

/*
 * Copyright 2020 Bhaskar Pramanik <splashingbee@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Description of AuthenticationImpl
 * 
 * Implementation of the Authentication interface
 *
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 */
//require_once CLASSPATH_ABSTRACT.'/Authentication.php';
require_once dirname( dirname( __FILE__ ) ).'/abstract/Authentication.php';

require_once CLASSPATH_EXCEPTIONS."/AuthenticationException.php";

class AuthenticationImpl implements Authentication {
	
	private $_userAuthArray;
	
	private $_assertAuthArray;
	
	private $_authArtifactArray;
	
	private $_authVerdict;
	
	private $_targetURL;
	
	private $_userDataArray;
	
	private $_authSignature;

	public function __construct() {
		LogInfoHandler::log();
		
		// Instantiate the properties
		$this->_userAuthArray     = array();
		$this->_assertAuthArray   = array();
		$this->_authArtifactArray = array();
		$this->_userAuthArray     = array();
		$this->_authVerdict       = false;
		$this->_authSignature     = "";
	}

	public function setAssertAuthArray( $assertAuthArray ) {
		LogInfoHandler::log();
		$this->_assertAuthArray = $assertAuthArray;
	}

	public function setUserAuthArray( $userAuthArray ) {
		LogInfoHandler::log();
		$this->_userAuthArray = $userAuthArray;
	}

	public function getAuthArtifacts( ) {
		LogInfoHandler::log();
		return $this->_authArtifactArray;
	}

	public function setAuthVerdict( $authVerdict ) {
		LogInfoHandler::log();
		$this->_authVerdict = $authVerdict;
	}

	public function getAuthVerdict() {
		LogInfoHandler::log();
		return $this->_authVerdict;
	}

	public function authenticate() {
		// Override in child
    }

	public function setTargetURL( $targetURL ) {
		LogInfoHandler::log();
		$this->_targetURL = $targetURL;
	}
	
	public function getTargetURL() {
		LogInfoHandler::log();
		return $this->_targetURL;
	}
	
	function getUserDataArray() {
		LogInfoHandler::log();
		return $this->_userDataArray;
	}

	function setUserDataArray($userDataArray) {
		LogInfoHandler::log();
		$this->_userDataArray = $userDataArray;
	}
	
	function getAuthSignature() {
		LogInfoHandler::log();
		return $this->_authSignature;
	}

	function setAuthSignature($authSignature) {
		LogInfoHandler::log();
		$this->_authSignature = $authSignature;
	}
	
	public function __destruct() {
		LogInfoHandler::log();
	}
}