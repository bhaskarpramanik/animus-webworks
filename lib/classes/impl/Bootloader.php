<?php

/**
 * @since 1 . 0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 * 
 * Description:
 * Simple class to load Animus environment from application-settings.xml
 * There are the following quantities that can be defined through the settings file. Thse are
 * 1. Path - System paths
 * 2. Variables - System Variables / Constants
 * 3. Supported file types - The supported file types within the application
 */

// XMLParser class is loaded which is used to load the settings.
require_once 'XMLParser.php';

// Setting the log paths for error and debug logs
define( 'DEBUG_LOG', ROOT . '/logs/debug.log' );
define( 'ERROR_LOG', ROOT . '/logs/exception.log' );

class BootLoader{
	/**
	 * @since 1.0
	 * @author Bhaskar Pramanik
	 * @throws ConfigurationException
	 */
	public function setEnvironment(){
		LogInfoHandler::log();
		try {
				$appSettingsPath = ROOT . '/config';
				$appSettingsName = 'application-settings.xml';	// Framework expects application-settings.xml to be present at > /config/application-settings.xml
				$file			 = $appSettingsPath . '/' . $appSettingsName;
				$fileExists	     = file_exists( $file );
				if( false === ( bool) $fileExists ) {
					throw new ConfigurationException(  'Expecting application-settings.xml, but not found in path!' );
				}
				else {
					// Instance of XML parser is created
					$xmlParser		= new XMLParser( );
					$isXmlParserReady = $xmlParser -> setXMLParams(  $appSettingsPath, $appSettingsName );
					if(  $isXmlParserReady ) {
						$xmlParser -> loadXML();
						while( @$xmlParser -> read() ) {
							if( $xmlParser -> isTagStartElement() ) {
								$system_constant_name = $xmlParser -> localName;
								switch(  $system_constant_name ) {
									// Setting environment path constants for all system paths
									case ( 'path' ) : {
										LogInfoHandler::log();
										$constant_name  = $xmlParser -> getAttributeByName( 'name' );
										$constant_value = $xmlParser -> getAttributeByName( 'value' );
										self::setSystemConstant(  $constant_name, ROOT . $constant_value . DIRECTORY_SEPARATOR );
									}

									break;

									case ( 'error-property' ) : {
										LogInfoHandler::log( );
										$constant_name  = $xmlParser -> getAttributeByName(  'name' );
										$constant_value = $xmlParser -> getAttributeByName( 'value' );
										self::setSystemConstant( $constant_name, $constant_value );
									}

									break;

									case ( 'variable' ) : {
										LogInfoHandler::log( );
										$constant_name  = $xmlParser -> getAttributeByName( 'name' );
										$constant_value = $xmlParser -> getAttributeByName( 'value' );
										if ( 'true' === $constant_value  ){
											$constant_value = true;
										}
										else if ( 'false' === $constant_value ){
											$constant_value = false;
										}
										self::setSystemConstant( $constant_name, $constant_value );
									}

									break;

									case ( 'filetype' ) : {
										LogInfoHandler::log( );
										$constant_name  = $xmlParser -> getAttributeByName( 'name' );
										$constant_value = $xmlParser -> getAttributeByName( 'value' );
										self::setSystemConstant( $constant_name, $constant_value ); 
									}

									break;

									case( 'type' ) : {
										LogInfoHandler::log( );
										$constant_name  = $xmlParser -> getAttributeByName( 'name' );
										$constant_value = $xmlParser -> getAttributeByName( 'value' );
										self::setSystemConstant( $constant_name, $constant_value ); 
									}
									break;
								}
							}
						}
					}
					 // Environment loading was successful. Return true to the caller.
					LogInfoHandler::log( 'Environment loading complete!' );
					LogInfoHandler::log( 'Requiring generic system classes!' );
					$this -> requireGenericSystem();
					return true;
				}
		} catch ( ConfigurationException $ex ) {
			$ex -> log();
		}
		return false;
	}

	public function requireGenericSystem() {
		LogInfoHandler::log( );
		// Require all the required files
		require_once ROOT . '/lib/classes/exceptions/NativeException.php';
		require_once ROOT . '/lib/classes/exceptions/NullException.php';
		require_once ROOT . '/lib/classes/exceptions/ConfigurationException.php';
		require_once ROOT . '/lib/classes/exceptions/WebException.php';
		require_once ROOT . '/lib/classes/exceptions/SQLException.php';
		require_once ROOT . '/lib/classes/impl/HTTPRequest.php';
		require_once ROOT . '/lib/classes/impl/HTTPResponse.php';
		require_once ROOT . '/lib/classes/impl/Dispatch.php';
		require_once ROOT . '/lib/classes/impl/HeaderInfoHandler.php';
		require_once ROOT . '/lib/classes/impl/TemporaryRedirection.php';
		require_once ROOT . '/lib/classes/impl/TempErrorRedirection.php';
		require_once ROOT . '/lib/classes/impl/PermanentRedirection.php';
		require_once ROOT . '/lib/classes/impl/MaintenanceRedirection.php';
		require_once ROOT . '/lib/classes/impl/AuthenticationRedirection.php';
		require_once ROOT . '/lib/classes/impl/XMLParser.php';
		require_once ROOT . '/lib/classes/impl/Session.php';
		require_once ROOT . '/lib/classes/impl/ErrorRedirection.php';
		require_once ROOT . '/lib/classes/impl/MVCWrapper.php';
		require_once ROOT . '/lib/classes/impl/AssetManager.php';
		require_once ROOT . '/lib/classes/impl/ControllerImpl.php';
		require_once ROOT . '/lib/classes/impl/ViewImpl.php';
		require_once ROOT . '/lib/classes/impl/StaticView.php';
		require_once ROOT . '/lib/classes/impl/ModelImpl.php';
		require_once ROOT . '/lib/classes/impl/SQLModel.php';
		require_once ROOT . '/lib/classes/impl/PDOSQLConnManager.php';
		require_once ROOT . '/lib/classes/impl/SimpleSQLModel.php';
		require_once ROOT . '/lib/classes/impl/ThemeLoader.php';
	}

	public function setSystemConstant( $constant_name, $constant_value ) {
		LogInfoHandler::log( );
		if( 'true' === strtolower( $constant_value ) ) {
			$constant_value = true;
		}
		else if( 'false' === strtolower( $constant_value ) ) {
			$constant_value = false;
		}
		if( !defined( $constant_name ) ) {
			define( $constant_name, $constant_value );
		}
	}

	public function __destruct( ) {
		LogInfoHandler::log();
	}
}