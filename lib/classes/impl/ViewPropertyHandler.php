<?php
/**
 *
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 * Description of ViewPropertyHandler
 * This class serves as an object for manipulating view properties on the runtime
 */


require_once CLASSPATH_IMPL.'/ViewProperty.php';
class ViewPropertyHandler {
	private $_viewPropertyArray;
	private $_viewName;
	private $_statusCode;
	private $_redirectionURL;
	
	public function __construct(){
		LogInfoHandler::log();
		$this->_viewPropertyArray = new ViewProperty();
	}
	
	public function resolveViewProperties() {
		LogInfoHandler::log();
		$XMLParser        = new XMLParser();
		$viewName         = $this->getViewName();
		$isXMLParserReady = $XMLParser->setXMLParams( VIEW_PROPERTIES, $this->getViewName().'_properties.xml' );
		if( $isXMLParserReady ) {
			$XMLParser->loadXML();
			while( @$XMLParser->read() ) {
				if( $XMLParser->isTagStartElement() ) {
					$nodeName = $XMLParser->localName;
					switch( $nodeName ) {
						case ( 'property' ) : {
							$name  = $XMLParser->getAttributeByName( 'name' );
							$value = $XMLParser->getAttributeByName( 'value' );
							$this->setViewPropertyArray( $name, $value );
						}
						break;
					}
				}
			}
		}
	}
	
	public function getViewPropertyArray() {
		return $this->_viewPropertyArray;
	}

	public function getViewName() {
		LogInfoHandler::log();
		return $this->_viewName;
	}

	public function setViewPropertyArray( $viewPropertyArrayItemName, $viewPropertyArrayItemValue ) {
		LogInfoHandler::log();
		switch( $viewPropertyArrayItemName ) {
		  case( 'VIEW_ALLOW_NAMED_PARAMS' ):$this->_viewPropertyArray->needViewParams( 0 < strlen( $viewPropertyArrayItemValue ) ? $this->stringToBoolean( ( string ) $viewPropertyArrayItemValue ):null );	
			 break;
		  case( 'VIEW_PARAMS' ):$this->_viewPropertyArray->setViewParams( 0 < strlen( $viewPropertyArrayItemValue ) ? ( string ) $viewPropertyArrayItemValue:null );
			 break;
		  case( 'VIEW_NEEDS_AUTH' ):$this->_viewPropertyArray->setAuthRequired( 0 < strlen( $viewPropertyArrayItemValue ) ? $this->stringToBoolean( ( string ) $viewPropertyArrayItemValue ):null );
			 break;
		  case( 'AUTH_URL' ):$this->_viewPropertyArray->setAuthViewURL( 0 strlen( < $viewPropertyArrayItemValue ) ? ( string ) $viewPropertyArrayItemValue:null );
			 break;
		  case( 'AUTH_SIGNATURE' ):$this->_viewPropertyArray->setAuthSignature( 0 < strlen( $viewPropertyArrayItemValue ) ? ( string ) $viewPropertyArrayItemValue:null );
			 break;
		  case( 'VIEW_REDIRECTED' ):$this->_viewPropertyArray->setViewRedirected( 0 < strlen( $viewPropertyArrayItemValue ) ? $this->stringToBoolean( ( string ) $viewPropertyArrayItemValue ):null );
			 break;
		  case( 'VIEW_REDIRECTION_CODE' ):$this->_viewPropertyArray->setViewRedirectCode( 0 < strlen( $viewPropertyArrayItemValue ) ? ( string ) $viewPropertyArrayItemValue:null );
			 break;
		  case( 'VIEW_REDIRECTED_URL' ):$this->_viewPropertyArray->setViewRedirectRescURL( 0 < strlen( $viewPropertyArrayItemValue ) ? ( string ) $viewPropertyArrayItemValue:null );
			 break;
		  case( 'VIEW_PHASE_MAINT' ):$this->_viewPropertyArray->setPhaseMaint( 0 < strlen( $viewPropertyArrayItemValue ) ? $this->stringToBoolean( ( string ) $viewPropertyArrayItemValue ):null );
			 break;
		  case( 'VIEW_MAINT_START' ):$this->_viewPropertyArray->setPhaseMaintStart( 0 < strlen( $viewPropertyArrayItemValue ) ? ( string ) $viewPropertyArrayItemValue:null );
			 break;
		  case( 'VIEW_MAINT_STOP' ):$this->_viewPropertyArray->setPhaseMaintStop( 0 < strlen( $viewPropertyArrayItemValue ) ? ( string ) $viewPropertyArrayItemValue:null );
			 break;
		  case( 'MAPPED_CONTROLLERS' ):$this->_viewPropertyArray->setMappedControllers( 0 < strlen( $viewPropertyArrayItemValue ) ? ( string ) $viewPropertyArrayItemValue:null );
			 break;
		  case( 'MAPPED_MODELS' ):$this->_viewPropertyArray->setMappedModels( 0 < strlen( $viewPropertyArrayItemValue ) ? ( string ) $viewPropertyArrayItemValue:null );
			 break;
		  case( 'MAPPED_DATASETS' ):$this->_viewPropertyArray->setMappedDatasets( 0 < strlen( $viewPropertyArrayItemValue ) ? ( string ) $viewPropertyArrayItemValue:null );
			 break;
		  case( 'IS_DYNAMIC' ):$this->_viewPropertyArray->setViewAsDynamic( 0 < strlen( $viewPropertyArrayItemValue ) ? $this->stringToBoolean( ( string ) $viewPropertyArrayItemValue ):null );
			 break;
		  case( 'LOAD_STATIC_ASSET' ):$this->_viewPropertyArray->setLoadStaticAssets( 0 < strlen( $viewPropertyArrayItemValue ) ? $this->stringToBoolean( ( string ) $viewPropertyArrayItemValue ):null );
			 break;
		  case( 'MAPPED_STATIC_ASSETS' ):$this->_viewPropertyArray->setStaticAssets( 0 < strlen( $viewPropertyArrayItemValue ) ? ( string ) $viewPropertyArrayItemValue:null );
			 break;
		  case( 'OVERRIDE_DEFAULT_STATIC_LOADING' ):$this->_viewPropertyArray->setOverrideDefaultStaticLoad( 0 < strlen( $viewPropertyArrayItemValue ) ? $this->stringToBoolean( ( string ) $viewPropertyArrayItemValue ):null );
			 break;
		}
	}

	public function stringToBoolean( $var ) {
		LogInfoHandler::log();
		if( $var == 'true' ) {
		  return true;
		}
		else if( $var = 'false' ) {
		  return false;
		}
	}
	public function setViewName( $viewName ) {
		LogInfoHandler::log();
		$this->_viewName = $viewName;
	}

	public function __destruct() {
	  LogInfoHandler::log();
	}
	
	/*
	* Determine the redirection properties
	*/
	
	
	public function validateForcedRedirection() {
	  LogInfoHandler::log();
		//Define a flag to check
		$isRedirected   = false;
		$viewProperties = $this->getViewPropertyArray();
		$isRedirected   = $viewProperties->getViewRedirected();
		
		if( $isRedirected ){
		  $statusCode = $viewProperties->getViewRedirectCode();
		  $this->setRedirectionURL( $viewProperties->getViewRedirectRescURL() );
		}
		else{
		  // Nothing to do here - No redirections
		}
		// If forced redirection is in effect, but no URL exists
		// Status code changes to 500
		if( is_null( $this->getRedirectionURL() ) ) {
		  $statusCode = 500;
		}
		$this->setStatusCode( $statusCode );
		return $isRedirected;
	}
	
	public function validateAuthRedirection( $authSignature ) {
	  LogInfoHandler::log();
		//Define a flag to check
		$isRedirected   = false;
		$viewProperties = $this->getViewPropertyArray();
		$isAuthRequired = $viewProperties->getAuthRequired();
		if( $isAuthRequired ) {
		  $authSignatureRequired = $viewProperties->getAuthSignature();
			$this->setRedirectionURL( $viewProperties->getAuthViewURL() );
			if( !is_null( $authSignature ) ) {
				if( $authSignature == $authSignatureRequired ) {
				$statusCode = 202;
			}
			 else if( $authSignature != $authSignatureRequired ) {
					$statusCode = ERROR_403;
					$isRedirected = true;
				}
			}
			else if( is_null( $authSignature ) ) {
				$statusCode = ERROR_401;
				$isRedirected = true;
				$this->setRedirectionURL( $this->getRedirectionURL() );
			}
		}
		else{
		  // Nothing to do here
		}
		// If authentication needs to be checked, but no view exists
		// Status code changes to 500
		if( is_null( $this->getRedirectionURL() ) ) {
		 LogInfoHandler::log( 'Authentication view is missing!' );
		  $statusCode = 500;
		}
		// Set the status code accordingly
		$this->setStatusCode( $statusCode );
		return $isRedirected;
	}
	
	public function validateMaintenanceRedirection(){
	  LogInfoHandler::log();
		////Define a flag to check
		$isRedirected       = false;
		$viewProperties     = $this->getViewPropertyArray();
		$isMaintenanceSetUp = $viewProperties->getPhaseMaint();
		$maintenanceTimeout = TIMEOUT_MAINTENANCE; // Time in minutes
		if( $isMaintenanceSetUp ) {
			$currentTime          = time();
			$maintenanceStartTime = $viewProperties->getPhaseMaintStart();
			$maintenanceStopTime  = $viewProperties->getPhaseMaintStop();
		   /*
			* Redirection URL not availble here, import from application settings.
		 	*/
			if( !is_null( $maintenanceStartTime ) && !is_null( $maintenanceStopTime ) ) {
				if( ( $currentTime >= $maintenanceStartTime ) && ( $currentTime < $maintenanceStartTime ) ) {
					$statusCode = ERROR_503;
					$this->setStatusCode( $statusCode );
					// Get the redirection setting from bootloader defined values
					$isRedirected = REDIRECT_503;
					return $isRedirected;
				}
			}
			else if( !is_null( $maintenanceStartTime ) && is_null( $maintenanceStopTime ) ) {
			 	// Convert the timeout to seconds and add to $maintenanceStartTime
				$maintenanceTimeoutInSec     = $maintenanceTimeout * 60;
				$defaultMaintenancePeriodEnd = $maintenanceStartTime + $maintenanceTimeoutInSec;
				if( $currentTime >= $defaultMaintenancePeriodEnd ) {
					$isRedirected = false;
				}
				else if( $currentTime <= $defaultMaintenancePeriodEnd ) {
					$statusCode = ERROR_503;
					$this->setStatusCode( $statusCode );
					$isRedirected = true;
				}
			}
		}
		return $isRedirected;
	}
	
	public function setRedirectionURL( $redirectionURL ) {
	  LogInfoHandler::log();
		$this->_redirectionURL = $redirectionURL;
	}
	
	public function getRedirectionURL() {
	  LogInfoHandler::log();
		return $this->_redirectionURL;
	}
	
	public function setStatusCode( $statusCode ) {
	  LogInfoHandler::log();
		$this->_statusCode = $statusCode;
	}
	
	public function getStatusCode() {
	  LogInfoHandler::log();
		return $this->_statusCode;
	}
}
