<?php

/**
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 */

class LifeCycleManager {
	/**
	 * @var string Store the view name 
	 */
	private $_mapped_view_name;

	/**
	 *
	 * @var string Holds the request protocol version
	 */
	private $_server_protocol;

	/**
	 *
	 * @var Dispatch Holds instance of dispatch used to process the request
	 */
	private $_dispatch;

	/**
	 *
	 * @var boolean Flag to hold the error status generated while processing the request
	 */
	private $_error_flag;

	/**
	 *
	 * @var View Holds the View Instance
	 */
	private $_mappedView;

	/**
	 *
	 * @var MVCWrapper Holds the gathered dependencies for the view while processing the request
	 */
	private $_mvcWrapper;

	/**
	 *
	 * @var boolean Holds a flag that determines if the request is served as an emulated path
	 */
	private $_emulatedPathRequest;

	/**
	 *
	 * @var string Holds the tailing segment of the URL
	 */
	private $_requestURIEndSegment;

	/**
	 *
	 * @var PDOSQLConnManager Holds instance of the PDOSQLConnManager class 
	 */
	private $_connObj;

	/**
	 * @var boolean Flag to hold the condition for injection of Callback URL in the controller class
	 */
	private $_callbackURLFlag;

	/**
	 * @since 1.0
	 * 
	 */
	public function __construct() {
		LogInfoHandler::log();
		$this->_emulatedPathRequest = true;
		$this->_error_flag          = false;
		$this->_callbackURLFlag     = false;
	}

	/**
	 * @param null
	 * @return void
	 * @since 1.0
	 * @author Bhaskar Pramanik <splashingbee@gmail.com>
	 * Short Description
	 * The complete app lifecycle - Environment loading, Request Phase handling and Response phase handling
	 */
	public function initAppLifecycle() {

		if( $this->loadEnvironment() ) {
			date_default_timezone_set( DEFAULT_TIMEZONE ); // Set the timezone to default timezone

			if( !$this->startRequestPhase() ) { // Trigger the request phase, set error flag true if request phase fails
				$this->_error_flag = true;
			}

			if( !$this->startResponsePhase() ) { // Trigger response phase irrespective of success/failure of request phase
				$this->_error_flag = true;
				$this->startResponsePhase(); // Trigger response phase for a second time, if there were errors in first response call
			}

			else {
				$this->getMappedView()->output();
			}
		}
		else {
			$this->systemAbort( 'System abort happened due to failure in bootloader phase' );
		}

	}

	/**
	 * 
	 * @return void
	 * @since 1.0
	 * @return boolean Description
	 * @author Bhaskar Pramanik <splashingbee@gmail.com>
	 * 
	 * Description
	 * Trigger the BootLoader class and load environment
	 */
	public function loadEnvironment() {
		return ( new BootLoader() )->setEnvironment();
	}

	/**
	 * 
	 * @return boolean
	 * @since 1.0
	 * @return boolean Description
	 * @author Bhaskar Pramanik <splashingbee@gmail.com>
	 * 
	 * Description
	 * 
	 * This method determines, if the called URL is a valid URL. The URL may be an emulated system path or
	 * might end in a file extension. Also, in both cases, this method establishes if the URL is programmed
	 * for deployment. In case, the called URI is an emulated URI request, the method sets the flag and
	 * returns a boolean, indicating success / failure of the operation.
	 */
	public function startRequestPhase() {
		LogInfoHandler::log();

		$httpRequest = new HTTPRequest();
		$httpRequest->loadInputParams();
		$httpRequest->setServerProtocol();

		$this->_requestURIEndSegment = $httpRequest->getURLEndSegment(); // The end part of the URL is saved
		$this->_requestURI           = $httpRequest->getRequestURI();
		$dispatch                    = $this->getDispatch(); //	Get Dispatch class to work further on the request
		$session                     = ( new Session() )->getSession(); // Current session data is extracted from the session else new session is set

		if( !is_null( $session) ) {
			if( !is_null( $session->isValid() ) ) {
				$dispatch->setUser( $session->getUser() );	// Setting the user object to the Dispatch class
			}
		}

		$httpRequest->injectSession( $session );	// Injecting the session in Request object
		$serverProtocol = $httpRequest->getServerProtocol();

		$this->setServerProtocol( $serverProtocol ); // Saving the server protocol version http/nohttp

		if( $httpRequest->isFolderPathRequest() ) {	// Validating if the called URL is a folder path request - URL tail doesn't end in an extension
		   LogInfoHandler::log( 'Requested URL may point to a deployed server path!' );
		   $isURIValid = $this->validateRequestedURI( $httpRequest->getRequestURI(), true );
		   if( $isURIValid ) {	// Path is validated against list of programmed emulated paths
			   LogInfoHandler::log ( 'The requested URL is being processed as an emulated system path.' );
		   }
		   $this->_emulatedPathRequest = true; // The requested URL is being recognized as a programmed emulated path
		   return $isURIValid;	// The result of URI validation is returned
		}

		$isURIValid = $this->validateRequestedURI( $httpRequest->getRequestURI(), false );
		if( $isURIValid ) {	// The URI is being validated. In this case, the called URL ended in an file extension
			LogInfoHandler::log( 'The requested URL exists!' );
			$this->_emulatedPathRequest = false;
		}
		else{
			LogInfoHandler::log ( 'The requested URL is neither an emulated system path nor a deployed path.' );
		}
		unset( $httpRequest );
		return $isURIValid;	// The result of URI validation is returned
	}

	/**
	 * @param null
	 * @return void
	 * @since 1.0
	 * @author Bhaskar Pramanik <splashingbee@gmail.com>
	 * 
	 * Description
	 * 
	 * The method responsibe for processing the response, on the basis of the outcome of the request phase. The method does the following
	 * 1. Checks and sends out headers in case of 404 errors.
	 * 2. Processes redirections on the requested URI.
	 * 
	 */

	public function startResponsePhase() {
		LogInfoHandler::log();


		$redirectLocation       = null;
		$isErrorRedirectionReqd = false;
		$httpResponse           = new HTTPResponse();
		$responseHeaderArray    = null;
		$responseReady          = false;
		
	   
		/*
		 * Case-1: This method is triggered to handle error code.
		 * In this case, we must check if the admin has requested
		 * redirection to fancy page for the error.
		 * Hence, a re-route request is needed here, followed by redirection (if needed)
		 */
		$dispatch = $this->getDispatch();	// A copy of dispatch object is obtained
		/*
		 * Error case, where the deployed route was never found
		 */
		if( $this->_error_flag ){
			$this->_dispatch->requestReRoute();	// A re-route request refers to conditions, when this method is being called to process error.
			$isErrorRedirectionReqd = $this->_dispatch->processError();	// The error generated from the request phase is processed.
			if( $isErrorRedirectionReqd ){	// A redirection may be needed to handle the error. The same is being checked.					
				if( true === $this->processRedirection() ) {	// Check if infinite redirection loop is detected. This can be due to error in configuration of URLs. Called and redirected URLs are same
					$httpResponse->prepareHTTPHeader( $this->getServerProtocol(), $dispatch->getStatusCode() ); // The server protocol version and status code are used to derive a HTTP header which is sent as a part of response.
					$responseHeaderArray = $httpResponse->getHeaderArray();	// Headers are retreived
					$httpResponse->sendHeaders();	// Headers are sent to the output
				}
				else{
					$responseReady = true;	//	If there is no infinite loop, then response is ready to be sent
				}
			}
			else{

				$httpResponse->prepareHTTPHeader( $this->getServerProtocol(),$dispatch->getStatusCode() );	// In case of no redirection needed HTTP headers are prepared.
				$responseHeaderArray = $httpResponse->getHeaderArray();	//	The generated headers are loaded into an array
				
				// Execution has reached end point
				// No view class needed further - sending headers from response
				$httpResponse->sendHeaders(); // Headers are sent
			}
		}
		else{
			LogInfoHandler::log( 'Creating MVC Wrapper' );
			$this->setmvcWrapper( new MVCWrapper() );	// A new instance of MVCWrapper is created
			/* Case 202: Route requested was deployed and found!
			 * Check the status code before proceeding further
			 * To determine that there was no error while processing
			 */
			$isRedirectionNeeded = $this->_dispatch->processDeployedRoute();
			if( $isRedirectionNeeded ) {
				$infiniteLoop = $this->processRedirection();
				//Unset the viewPropertyArray
				$this->getDispatch()->unsetViewPropertyHandler();
				if( true === $infiniteLoop ){
					/* 
					 * In this particular case, it won't be possible to finalize to a view
					 * due to infinite loop of redirections, hence output headers from here
					 */
					$httpResponse->prepareHTTPHeader( $this->getServerProtocol(), $dispatch->getStatusCode() );
					$responseHeaderArray = $httpResponse->getHeaderArray();
					$httpResponse->sendHeaders();
				}
				else{
					$responseReady = true; //	Else response is ready for this response
				}
			}
			else{
					LogInfoHandler::log( 'Loading view / controllers / models / statics' ); // Write the code to load view/controllers/assets and other relevant things

					$dispatch = $this->getDispatch();	// Getting the dispatch object
					$viewName = $dispatch->getMappedViewName();	// Getting the view name which needs to be processed
					$this->setMappedViewName($viewName);
					$this->loadView();	// Load the view into the MVCWrapper class
					$viewIsDynamic = $dispatch->getViewPropertyArray()->isViewDynamic();	// Check if the view is dynamic
					if($viewIsDynamic){
						// Dynamic View -- Business logic is held in a controller
						/*
						 * Loading the controller(s), $controllerNameList = String name array of the controllers
						 */
						$controllerNameList = $dispatch->getViewPropertyArray()->getMappedControllers();
						try{
							 $this->loadController( $controllerNameList );
							 $responseReady = true;
						} 
						 catch (NativeException $ex) {
							 $ex->log();
							 $this->_dispatch->setStatusCode( 500 );
							 return false; // Return false here to trigger the error processing.
						}

						/*
						 * Loading the model(s), $modelNameList = String name array of the models
						 */
						 $modelNameList = $dispatch->getViewPropertyArray()->getMappedModels();
						 try{
							 $this->loadModels( $modelNameList );
							LogInfoHandler::log( 'Model list loaded successfully!' );
							 $responseReady = true;
						} catch (Exception $ex) {
							 $this->_dispatch->setStatusCode( 500 );
							 return false; // Return false here to trigger the error processing.
						}
					
						/*
						 * Loading the dataset(s), $datasetNameList = String name array of the datasets
						 */
						 $datasetNameList = $dispatch->getViewPropertyArray()->getMappedDatasets();

						 try{
							 $this->loadDatasets( $datasetNameList );
							 $responseReady = true;
						} catch (Exception $ex) {
							 $ex->log();
							 $this->_dispatch->setStatusCode( 500 );
							 return false; // Return false here to trigger the error processing.
						}
					
						/*
						 * Loading the static(s), $staticNameList = String name array of the statics
						 */
						 $staticAssetsLoadFlag = $dispatch->getViewPropertyArray()->getLoadStaticAssets();
						 $staticNameList = $dispatch->getViewPropertyArray()->getStaticAssets();
						 if( $staticAssetsLoadFlag ){
							 try{
								 $this->loadStatic( $staticNameList );
								 $responseReady = true;
							} catch (Exception $ex) {
								 $ex->log();
								 $this->_dispatch->setStatusCode( 500 );
								 return false; // Return false here to trigger the error processing.
							}
						}
				
					$this->executeMVC();	// Execute the MVC
				}
				else{
						LogInfoHandler::log("View is configured as STATIC. Loading the static components! - If configured");
						/*
						* Loading the static(s), $staticNameList = String name array of the statics
						*/
						$staticAssetsLoadFlag = $dispatch->getViewPropertyArray()->getLoadStaticAssets();
						$staticNameList = $dispatch->getViewPropertyArray()->getStaticAssets();
						if( $staticAssetsLoadFlag && !is_null( $staticNameList ) ){
							try{
								$this->loadStatic( $staticNameList );
								$responseReady = true;
							}catch ( Exception $ex ) {
								$ex->log();
								$this->_dispatch->setStatusCode(500);
								return false; // Return false here to trigger the error processing.
							}
							$this->executeMVC();
						}
						else{
							// No end file to be loaded
							// If system path emulation is needed, send 200, else send 404
							if(EMULATE_SYSTEM_PATH){
								$this->_dispatch->setStatusCode( 200 );
								$responseReady = true;
							}
							else{
								$this->_dispatch->setStatusCode( 404 );
								return false;
							}
							$this->executeMVC();
						}
					}
				}
			}
		return $responseReady;
	}

	/**
	 * @param  null
	 * @return Dispatch
	 * @since 1.0
	 * @author Bhaskar Pramanik <splashingbee@gmail.com>
	 * 
	 * Description
	 * 
	 * This function checks for an existing instance of Dispatch or returns a new Dispatch
	 * 
	 */
	public function getDispatch() {
	   LogInfoHandler::log();
		if(is_null($this->_dispatch)){
			$this->_dispatch = new Dispatch();
		}
		return $this->_dispatch;
	}

	/**
	 * @param  null
	 * @return Dispatch
	 * @since 1.0
	 * @author Bhaskar Pramanik <splashingbee@gmail.com>
	 * 
	 * Description
	 * 
	 * This function validates a URI, to check if its an emulated path request or a file
	 * 
	 */
	public function validateRequestedURI( $requestURI, $isServerPathRequest ) {
		/*
		 * Dispatch is created / called
		 */
		LogInfoHandler::log();
		$dispatch = $this->getDispatch();
		/*
		 * Initial check if the URL is programmed or not
		 * return false, when Requested URL non available
		 * return true, when Requested URL available = Proceed further
		 */
		if( $isServerPathRequest ){
			// Nothing to do
			$initial_routing_flag = $dispatch->dispatchURL( $requestURI, true );
		}
		else{
			$initial_routing_flag = $dispatch->dispatchURL( $requestURI, false );
		}
		LogInfoHandler::log( $initial_routing_flag );
		return $initial_routing_flag;
	}

	/**
	 * @param  string|null
	 * @return null
	 * @since 1.0
	 * @author Bhaskar Pramanik <splashingbee@gmail.com>
	 * 
	 * Description
	 * 
	 * This function kills the application in case of abrupt abort
	 * 
	 */
	public function systemAbort( $message = null ) {
		LogInfoHandler::log( $messsage = null );
		http_response_code( 500 );
		die();
	}


	/**
	 * @param  null
	 * @return boolean
	 * @since 1.0
	 * @author Bhaskar Pramanik <splashingbee@gmail.com>
	 * 
	 * Description
	 * 
	 * This method processes a redirection request which might be needed to complete the response
	 * 
	 */
	public function processRedirection() {
	   LogInfoHandler::log();
		// Flag to track infinite redirections
		$infiniteRedirectionLoop = false;
		

		// Retrieve the formdata requested by the view 
		$httpRequest = new HTTPRequest();
		$httpRequest->loadInputParams();

		$redirectLocation = $this->_dispatch->getRedirectionURL();	// If requestedURI and redirectionURI are same it becomes an infinite loop
		$requestedURI = $this->_dispatch->getRequestedURI();
		
		if($requestedURI == $redirectLocation){
			$infiniteRedirectionLoop = true;
			$this->_dispatch->setStatusCode( 500 );
			LogInfoHandler::log( 'Originally requested URL > '.$requestedURI );
			LogInfoHandler::log( 'Redirecting to URL > '.$redirectLocation);
			LogInfoHandler::log( 'Initially requested and redirected URLs are same: Infinite loop detected!' );
		}
		else{
			$statusCode             = $this->_dispatch->getStatusCode();
			$redirectHandler        = $this->_dispatch->getRedirectionHandlerName();
			$this->_callbackURLFlag = true;
			
			$redirectionHandlerInstance = new $redirectHandler();
			$redirectionHandlerInstance->setRedirectionLocation( $redirectLocation );
			$redirectionHandlerInstance->setRedirectionHeaderCode( $statusCode );
			// Set this handler back to Dispatch so that redirection can be executed
			$redirectionHandlerInstance->setRedirectionFlag();

			// Set the callback URL for redirectionHandlerInstance
			$redirectionHandlerInstance->setCallbackURL( $requestedURI );

			/*
			 * In case of SSO flows - The authenticator needs to get the form data supplied by IdP
			 * to generate user session
			 */
			$redirectionHandlerInstance->setFormData( $httpRequest->getFormData() );

			// Configure the dispatch class with redirection handler
			$this->_dispatch->setRedirectionHandler( $redirectionHandlerInstance );

			// Get the redirection view loaded with the header array
			$redirectionView = $this->_dispatch->executeRedirection( $this->_server_protocol );
			$this->setMappedView( $redirectionView );
		}
		
		return $infiniteRedirectionLoop;
	}

	/**
	 * @param  null
	 * @return null
	 * @since 1.0
	 * @author Bhaskar Pramanik <splashingbee@gmail.com>
	 * 
	 * Description
	 * 
	 * This method loads a list of controller classes from an assets descripter file
	 * 
	 */

	public function loadController( $controllerNameList ) {
		/*
		 * If the view is "dynamic" in nature, it needs to have atleast 1 controller
		 * If the view needs a list of controllers, then each controller class name
		 * must be separated by a comma
		 */
	   LogInfoHandler::log();
		if(!(bool)strlen( $controllerNameList ) ){
			throw new NativeException( 'Controller name list is empty. Error happend while trying to process the following View: ' . $this->getMappedViewName() );
		}
		else{
			$assetManager = new AssetManager();
			// Checking if multiple controllers have been assigned to the view
			if(strpos( $controllerNameList, ',' ) !== 0 || strpos( $controllerNameList, STANDARD_SEPARATOR ) !== false ){
				$controllerNameArray  = explode( ',', $controllerNameList );
				$mvcWrapper           = $this->getMVCWrapper();
				foreach( $controllerNameArray as $controllerName ) {
					$controllerName   = trim( $controllerName );
					// Capture the Controller Object returned here
					$controllerObject = $assetManager->loadController( $controllerName  );
					$mvcWrapper->addController( $controllerName, $controllerObject );
				}
			}
			unset( $assetManager ); 
		}
	}

	/**
	 * @param  null
	 * @return null
	 * @since 1.0
	 * @author Bhaskar Pramanik <splashingbee@gmail.com>
	 * 
	 * Description
	 * 
	 * This method loads the view class from an assets descripter file
	 * 
	 */
	public function loadView() {
	   LogInfoHandler::log();
		$viewName     = $this->getMappedViewName();
		$assetManager = new AssetManager();
		$mvcWrapper   = $this->getMVCWrapper();
		// This part throws exception
		try{
			$viewObject = $assetManager->loadView( $viewName );
			$mvcWrapper->addView( $viewObject );
		} catch ( NativeException $ex ) {
			$ex->log();
		}
		unset( $assetManager );
	}

	/**
	 * @param  null
	 * @return null
	 * @since 1.0
	 * @author Bhaskar Pramanik <splashingbee@gmail.com>
	 * 
	 * Description
	 * 
	 * This method loads a list of model classes from an assets descripter file
	 * 
	 */
	public function loadModels( $modelNameList ) {
		   /*
		 * If the view is "dynamic" in nature, it needs to have atleast 1 controller
		 * If the view needs a list of controllers, then each controller class name
		 * must be separated by a comma
		 */
		LogInfoHandler::log();
		$assetManager = new AssetManager();
		// Checking if multiple models have been assigned to the view
		if(strlen(trim( $modelNameList ) ) !== 0 ){
			if(strpos( $modelNameList, ',' )!== 0 || strpos( $modelNameList, STANDARD_SEPARATOR ) !== false ){
				$modelNameArray = explode( ',', $modelNameList );
				$mvcWrapper = $this->getMVCWrapper();
				foreach( $modelNameArray as $modelName ){
					$modelName   = trim( $modelName );
					$modelObject = $assetManager->loadModel( $modelName );	// This part throws exception from asset loader, hence handled in try-catch block
					$mvcWrapper->addModel( $modelName, $modelObject );
				}
			}
		}
		unset($assetManager); 
	}

	/**
	 * @param  null
	 * @return null
	 * @since 1.0
	 * @author Bhaskar Pramanik <splashingbee@gmail.com>
	 * 
	 * Description
	 * 
	 * This method loads a list of datasets classes from an assets descripter file
	 * 
	 */
	public function loadDatasets( $datasetNameList ) {
		/*
		 * If the view is "dynamic" in nature, it needs to have atleast 1 controller
		 * If the view needs a list of controllers, then each controller class name
		 * must be separated by a comma
		 */
	   LogInfoHandler::log();
		$assetManager = new AssetManager();
		if( strlen( trim( $datasetNameList ) ) != 0 ){
			// Checking if multiple datasets have been assigned to the view
			if(strpos( $datasetNameList, ',' )!== 0 || strpos( $datasetNameList, STANDARD_SEPARATOR ) !== false ){
				$datasetNameArray = explode( ',', $datasetNameList );
				$mvcWrapper = $this->getMVCWrapper();
				foreach( $datasetNameArray as $datsetName ) {
					try{
						// This part throws exception from asset loader, hence handled in try-catch block
						$datsetName    = trim( $datsetName );
						$datasetObject = $assetManager->loadDataset( $datsetName );
						$mvcWrapper->addDataset( $datsetName, $datasetObject );
						// Capture the returned Dataset Object here
					} catch( NativeException $ex ) {
						// Nothing here
					}
				}
			}
		}
		unset( $assetManager );
	}

	/**
	 * @param  null
	 * @return null
	 * @since 1.0
	 * @author Bhaskar Pramanik <splashingbee@gmail.com>
	 * 
	 * Description
	 * 
	 * This method loads a list of static files from an assets descripter file
	 * 
	 */
	public function loadStatic( $staticNameList ) {
		/*
		 * If the view is "dynamic" in nature, it needs to have atleast 1 controller
		 * If the view needs a list of controllers, then each controller class name
		 * must be separated by a comma
		 */
		LogInfoHandler::log();
		$dispatch = $this->getDispatch();
		$assetManager = new AssetManager();
		// Checking if multiple models have been assigned to the view
			if(strpos( $staticNameList, ',' ) !== 0|| strpos( $staticNameList, STANDARD_SEPARATOR ) !== false){
				$staticNameArray = explode( ',', $staticNameList );
				$mvcWrapper = $this->getMVCWrapper();
				if($this->_emulatedPathRequest) {
					if( ( bool ) strlen( $staticNameList ) ) { // If static name list is not empty
					   foreach( $staticNameArray as $staticName ) {
							$staticName  = trim( $staticName );
							$staticAsset = $assetManager->loadStatic( $staticName );
							try{
								$content_header = $assetManager->getContent_type_header();
								$dispatch   -> setStatusCode( 202 );
								$mvcWrapper -> addStatic( $staticName, $staticAsset );
								$mvcWrapper -> setContentTypeHeader( $staticName, $content_header );
							} catch ( Exception $ex ) {
								$ex->log();
								$dispatch->setStatusCode( 400 );
								return false;
							}
						}
					}
					else{
						$dispatch->setStatusCode( 202 );
						return true;
					}
				}
				else{
					try{
						if( !( bool )strlen( $staticNameList ) ) {
						   LogInfoHandler::log( 'The list of static files loaded from view properties is empty. Hence, no static content is loaded. Trying to load content from the extention of the URL' );
							$staticAsset = $assetManager->loadStatic( $this->_requestURIEndSegment );
							$dispatch->setStatusCode( 202 );
							$mvcWrapper->addStatic(explode('.', $this->_requestURIEndSegment )[0], $staticAsset );
							$mvcWrapper->setContentTypeHeader( explode('.', $this->_requestURIEndSegment )[0], $assetManager->getContent_type_header() );
							return true;
							
						}
						else{
							foreach( $staticNameArray as $staticName ){
								$staticName = trim( $staticName );
								$staticAsset = $assetManager->loadStatic( $staticName );
								try{
									$content_header = $assetManager->getContent_type_header();
									$dispatch->setStatusCode( 202 );
									$mvcWrapper->addStatic( $staticName, $staticAsset );
									$mvcWrapper->setContentTypeHeader( $staticName, $content_header );
								} catch( Exception $ex ) {
									$ex->log();
									$dispatch->setStatusCode( 400 );
									return false;
								}
							}
						}
					} catch ( NativeException $ex ) {
						$ex->log();
						$dispatch->setStatusCode( 404 );	// This will amount to a 404 error
						return false;
					}
				}
			}
		unset($assetManager);
	}

	/**
	 * @param  null
	 * @return boolean
	 * @since 1.0
	 * @author Bhaskar Pramanik <splashingbee@gmail.com>
	 * 
	 * Description
	 * 
	 * This method executes the collected classes to serve the response
	 * 
	 */
	public function executeMVC() {
		LogInfoHandler::log();

		$userData        = null;
		$controller      = null;
		$outputData      = null;

		$httpRequest     = new HTTPRequest();
		$httpRequest->loadInputParams();
		$formData        = $httpRequest->getFormData();
		
		$mvcWrapper      = $this->getMVCWrapper();
		$staticContent   = $mvcWrapper->getStatic();
		$viewParamFlag   = $this->getDispatch()->getViewPropertyArray()->isViewParamAllowed();
		$viewParamsList  = explode(',',$this->getDispatch()->getViewPropertyArray()->getViewParams() );
		
		$sessObj         = new Session();
		$lastCallBackURL = $sessObj->getSession()->getCallbackURL();
		$sessObj->flushFormData();

		if(  null !== $sessObj->getSession()->getRedirectionFormData() ) {
			foreach( $sessObj->getSession()->getRedirectionFormData() as $k => $v ) {
				$formData[$k] = $v;
			}
		}

		// If Session is invalid - Flush the user information from the session object and close the session
		if( ! $sessObj->isValid() ) {
			$sessObj->invalidate();
		}

		// Retrieve the view
		$view = $mvcWrapper->getView();
		
		// Retrieve the controller
		$controllerArray = $mvcWrapper->getControllers();
		foreach( $controllerArray as $controller ) {
			// Mandatory controller actions
			// Set the called URL to the controller
			$controller->setCalledURI( $httpRequest->getRequestURI() );

			if( $controller->getCallbackURIFlag() ) {
				$controller->setCallbackURI( $lastCallBackURL );
			}

			if( $$httpRequest->isFormDataSet()
				&& $$controller->isFormDataNeeded() 
				&& $viewParamFlag ) {
				$formDataToBeSet = array();
				if( 0 < sizeof( $viewParamsList ) ) {
					foreach( $viewParamsList as $key => $value ) {
						if( in_array( $value, array_keys( $formData ) ) && isset( $formData[$value] ) ) {
							$formDataToBeSet[$value] = $formData[$value];
//							printf( '%s=>%s<br/>', $value, $formData[$value] );
						}
					}
				}
				$controller->setFormData( $formData );
				unset( $formDataToBeSet );
			}

			/*
			 * In case of redirection, if the current has been redirected the last URL becomes the callback URL
			 * and would be added to the Session object. This variable must be extracted from the session object
			 * and must be injected to the controller(s)
			 */

			if( 0 < strlen( $sessObj->getSession()->getCallbackURL() ) ) {
				$controller->setCallbackURI( $sessObj->getSession()->getCallbackURL() );
				$sessObj->getSession()->flushCallbackURL();
				$sessObj->updateSession();
			}

			if( $controller->hasAddlDataKeys() ) {
				$addlFormData = $controller->getAddlFormDataKeys();
				$formDataToBeSet = array();
				if( 0 < sizeof( $addlFormData ) ) {
					foreach( $addlFormData as $key => $value ) {
						if( in_array( $value, array_keys( $formData ) ) ) {
							$formDataToBeSet[$value] = $formData[$value];
						}
					}
				}
				$controller->setFormData( $formDataToBeSet );
			}

			/* App Functionality - Sill under development
			 * Probe the controller for any other requests for form data - Apps may need access to additional form data
			 * Configure the controller to use DB connection handle
			 */
			if( $controller->hasAppCtxt() ) {
				if( !is_a( $this->_connObj, "PDO" ) ) {
					$this->_connObj = ( new PDOSQLConnManager() )->getConnObject(); // Save an instance of DB connection - Hack to improve DB performance
				}
				$controller->setDBConnObj( $this->_connObj );
			}


			// Check which model to inject in which controller
			// Inject DB connection handle in model class - only if the model is set to DatabaseWorkflow
			$modelNameArray = $controller->getModelNameArray();
			foreach( $modelNameArray as $model ){
				$modelObj = $mvcWrapper->getModelByName( $model );
				if( $this->_workflowArray['isDatabaseWorkflow'] ) {
					// Initialize the DB handle here - create DB connection only if needed
					if( !is_a( $this->_connObj, "PDO" ) ) {
						$this->_connObj = ( new PDOSQLConnManager() )->getConnObject(); // Save an instance of DB connection - Hack to improve DB performance
					}
					$modelObj->setDBConnectionArtifact( $this->_connObj );
				}
				$controller->setModel( $model, $modelObj );
			}
			
			// Inject User Info in the controller
			$session  = $sessObj->getSession();
			$userInfo = $session->getUser();
			$controller->setUserInfo( $userInfo );
			
			// Inject shared data between controllers
			if($controller->isIOShareEnabled()){
				$ioShare[$controller->getName()] = $controller->getIoShareData();
				if( sizeof( $ioShare )!=0 ){
					$controller->setIOShareDataArray( $ioShare );
				}
			}

			// Execute the controller action
			$controller->execute();

			if( ! $controller->getUserInfo()->isNull() ) {
				$sessObj->setUser( $controller->getUserInfo() );
				$sessObj->setSession();
			}
			else{
				$sessObj->invalidate();
			}

			// Inject shared data between controllers
			if($controller->isIOShareEnabled()){
				$ioShare[$controller->getName()] = $controller->getIoShareData();
				if( sizeof( $ioShare )!=0 ){
					$controller->setIOShareDataArray( $ioShare );
				}
			}
			else{
				// IOShare is not enabled in the controller.
				// No $ioShare to load!
			}
			$viewSideload = $controller->isViewSideloadEnabled();
			$view = $mvcWrapper->getView();
			
			if( $viewSideload ){
				// Side load is already enabled
				// Static content mapped in the properties will be shown here
			}
			else{
				// Side load is disabled here
				// Static content mapped from properties shall skip
				$view->setNoSideLoad();
			}

			 // Retrieve the User data to be injected in the view
			$outputData = $controller->getOutputData();
		}
		if( !is_null( $outputData ) ){
			$keys = array_keys( $outputData );
			foreach( $keys as $key ){
				$view->setUserData( $key, $userData[$key] );
			}
		}
		if(isset( $staticContent ) ){
			$content_type_header = $mvcWrapper->getContentTypeHeader( array_keys($staticContent)[0] );
			$view->setContent( $staticContent, $content_type_header );
		}

		if( null !== $controller ) {
			if( null !== $controller->getCallbackURI() ) {
				// If controller had requested callback url - redirect it to the set location
				$headerInfoHander = new HeaderInfoHandler();
				$headerInfoHander->setLocationHeaderPath( $controller->getCallbackURI() );
				$headerInfoHander->generateLocationHeader($httpRequest->getServerProtocol());
				$headerArray      = $headerInfoHander->getHeaderArray();

				// Set the view instance as mapped view
				$view->setHeaderArray($headerArray);
			}
		}

		// Set the view instance as mapped view
		$this->setMappedView( $view );
		return true;
	}

	public function setMappedViewName( $viewName ) {
		LogInfoHandler::log();
		$this->_mapped_view_name = $viewName;
	}

	public function getMappedViewName() {
		LogInfoHandler::log();
		return $this->_mapped_view_name;
	}
	   
	public function setServerProtocol( $serverProtocol ) {
		LogInfoHandler::log();
		$this->_server_protocol = $serverProtocol;
	}

	public function getServerProtocol() {
		LogInfoHandler::log();
		return $this->_server_protocol;
	}

	public function setMappedView( View $view ) {
		LogInfoHandler::log();
		$this->_mappedView = $view;
	}

	public function getMappedView() {
		LogInfoHandler::log();
		return $this->_mappedView;
	}

	public function setMVCWrapper( MVCWrapper $mvcWrapper ) {
		LogInfoHandler::log();
		$this->_mvcWrapper = $mvcWrapper;
	}

	public function getMVCWrapper() {
	   LogInfoHandler::log();
		return $this->_mvcWrapper;
	}

	public function __destruct() {
		LogInfoHandler::log();
	}
}

