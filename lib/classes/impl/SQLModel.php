<?php
/**
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 * 
 * Description:
 * Class to manage the SQL transactions, to create a seamless data layer integration
 */

class SQLModel extends ModelImpl{
	
	/*
	 * Contain the DB Table Name
	 */
	private $_SQLTargetName;
	
	/*
	 * Contain the discriminator
	 */
	private $_constraint;
	
	/*
	 * Contain the connection object
	 */
	private $_SQLConnectionObject;
	
	/*
	 * Contain the query type
	 */
	private $_queryType;
	
	/*
	 * Contain the array of column names from the table
	 */
	private $_fieldNameArray;
	
	/*
	 * Contain the update value set
	 */
	private $_updateArray;
	
	/*
	 * Contain the output of the SQL transaction
	 */
	private $_outputArray;
	
	/*
	 * Contain the data set to insert
	 */
	private $_insertArray;
	
	/*
	 * Contain the db transaction status
	 */
	private $_dbTransactionStatus;
	
	/*
	 * Contain empty resultset flag
	 */
	private $_emptyResultSetFlag;
	
	/*
	 * Contain result set sort preference
	 */
	private $_returnSortedResultSet; // Defaults to ASC
	
	/*
	 * Sort result in descending order
	 */
	private $_sortDesc;
	
	/*
	 * Contain row count of the resultset
	 */
	private $_rowCount;
	
	/*
	 * Contain set limit on number of results
	 */
	private $_resultSetLimit;
	
	/*
	 * Contain the sort field
	 */
	private $_sortByField;
	
	/*
	 * Contain limited resultset preference
	 */
	private $_limitedResultSet;
	
	/*
	 * Contain the created query string
	 */
	private $_queryString;
	
	/*
	 * Contain the P_KEY of the last insert operation
	 */
	private $_lastInsertId;
	
	public function __construct() {
		parent::__construct();
		$this->_SQLTargetName	     = '';
		$this->_constraint            = array();
		$this->_SQLConnectionObject   = null;
		$this->_fieldNameArray        = array();
		$this->_updateArray           = array();
		$this->_outputArray           = array();
		$this->_insertArray           = array();
		$this->_deleteArray           = array();
		$this->_emptyResultSetFlag    = null;
		$this->_returnSortedResultSet = false;
		parent::setSQLWorkflow();
	}

	public function callWorkflow() {
		LogInfoHandler::log();
		$this->defineWorkflowArtifacts();
		$this->defineWorkflow();
		return $this->getDbTransactionStatus();
	}
	
	public function defineWorkflow() {
		// Overidden here
		// Variable declaration
		$fieldArray = $this->getFieldNameArray();
		$numberOfFields = sizeof( $fieldArray );
		LogInfoHandler::log( 'Size of fields: ' . $numberOfFields );
		$fieldString	 = '';
		$updateString	= '';
		$constraintLength = sizeof( $this->_constraint );
		$constraintString = '';
		$tableName        = $this->getSQLTargetName();
		$queryType        = $this->getQueryType();
		
		/* This section deals with sorting and limited the resultset in case of a select statement */
		$isResultSetLimited     = $this->isResultSetLimited();
		$resultSetLimit         = $this->getResultSetLimit();
		$isResultSetSorted      = $this->isResultSetSorted();
		$resultSetSortByField   = $this->getSortByField();
		$resultSetSortFlag      = $this->isResultSetSorted();
		$resultSetSortOrderDesc = $this->isResultSetSortedDesc(); // Default sort order is always ascending
		
		$updateArray = $this->_updateArray;
		
		// Implementation to generate the string for constraints
		// This scenario involves getting the key value pair ready
		// For multiple constraints
		foreach( $this->_constraint as $key => $value ) {
		  $separator_string  = 'AND ';
		  $constraintString .= '`' . $key . '` = "' . $value . '" ' . $separator_string;
		}
		@$constraintString = rtrim( $constraintString, $separator_string );
		// Fetch each field provided and build a string
		if( $numberOfFields > 0 ) {
			foreach( $fieldArray as $field ) {
				$separator_string = ',';
				$fieldString     .= ' `' . $field . '` ' . $separator_string;
		}
			$fieldString = rtrim( $fieldString, $separator_string );
			LogInfoHandler::log( 'The field string... ' . $fieldString);
		}
		if( 'INSERT' === $this->_queryType ) {
			foreach( $this->_insertArray as $key => $value ) {
				$separator_string = ',';
				if( 'NULL' === $value ) {
					@$inputValuesString .= ''.$value.','; 
					@$inputKeysString	.= '`'.$key.'`,';
				}
			 else{
					@$value              = trim( $value,'"' );
					@$inputValuesString .= '"' . $value . '",'; 
					@$inputKeysString   .= '`' . $key . '`,';
				}
			}
			@$inputValuesString = rtrim( $inputValuesString, ',' );
			@$inputKeysString   = rtrim( $inputKeysString, ',' );
			LogInfoHandler::log( 'The input string... ' . $inputValuesString );
		}
		$separator_string = ',';
		foreach( $updateArray as $key => $value ) {
			$updateString .= '`'.$key.'` = "'.$value.'" '.$separator_string;
		}
		$updateString = rtrim( $updateString, $separator_string );

		LogInfoHandler::log( $updateString );
		
		// Build the query
		switch($queryType){
			case 'SELECT':{
				if( 0 === sizeof($this->_constraint) ){
					$this->_queryString = $queryType.' '.$fieldString.' FROM `'.DATABASE_NAME.'`.`'.$tableName.'`';
				}
				else{
					$this->_queryString = $queryType.' '.$fieldString.' FROM `'.DATABASE_NAME.'`.`'.$tableName.'` WHERE '.$constraintString;
				}
				
				// If the resultset is sorted, then ...
				if( $isResultSetSorted ) {
					LogInfoHandler::log( 'Sorting resulset' );
					$this->_queryString .= ' ORDER BY `'.$resultSetSortByField.'`';
					if($this->isResultSetSortedDesc()){
						$this->_queryString .= ' DESC';
					}
					else{
						$this->_queryString .= ' ASC';
					}
				}
				if($isResultSetLimited){
					$this->_queryString .= ' LIMIT '.$resultSetLimit;
				}
				LogInfoHandler::log( 'Dumping the query generated ...' );
				//var_dump($this->_queryString);
				// Execute query and collect results
				$connObj = parent::getWorkflowArtifact( 'connectionObject' );
				LogInfoHandler::log( $this->_queryString );
				$stmt = $connObj->prepare( $this->_queryString );
				$stmt->execute();
				$SQLerrorInfo = $stmt->errorInfo();
				if( 0 === $stmt->rowCount() ){
					$this->setEmptyResultSet();
				}
				else if( 0 !== $stmt->rowCount() ) {
					$this->_emptyResultSetFlag = false;
					$this->setRowCount( $stmt->rowCount() );
				}
				//var_dump($SQLerrorInfo[0] !== "00000");
				// Check if the error string is non-zero - This means, query failed due to some kind of error.
				// Throw an exception and log the message in the log file.
				if( '0000' !== $SQLerrorInfo[0] ){
					try{
						throw new SQLException( $SQLerrorInfo[0].' '.$SQLerrorInfo[1].' '.$SQLerrorInfo[2] );
					} catch (SQLException $ex) {
						LogInfoHandler::log( 'SQL operation unsuccessful!' );
						LogInfoHandler::log( $SQLerrorInfo );
						$ex->log();
						$this->setDbTransactionStatus( false );
					}
				}
				else{
					while( $row = $stmt->fetch( PDO::FETCH_ASSOC ) ){
					$this->setOutputArray( $row );

					}
					LogInfoHandler::log( 'Database operation successful!' );
					$this->setDbTransactionStatus( true );
				}
			}
			break;
			case 'INSERT':{
				//INSERT INTO pdotest (`id`, `name`) VALUES ('1', 'bhaskar')
				$this->_queryString = $queryType.' INTO `'.DATABASE_NAME.'`.`'.$tableName.'` ('.$inputKeysString.') VALUES ('.$inputValuesString.')';
				// Execute query and collect results
				$connObj = parent::getWorkflowArtifact( 'connectionObject' );
				LogInfoHandler::log( $this->_queryString );
				$stmt = $connObj->prepare( $this->_queryString );
				$stmt->execute();
				$SQLerrorInfo = $stmt->errorInfo();
				if( 0 === $stmt->rowCount() ){
					$this->setEmptyResultSet();
				}
				else if( 0 !== $stmt->rowCount() ) {
					$this->_emptyResultSetFlag = false;
					$this->setRowCount( $stmt->rowCount() );
				}
				
				//var_dump($SQLerrorInfo[0] !== "00000");
				// Check if the error string is non-zero - This means, query failed due to some kind of error.
				// Throw an exception and log the message in the log file.
				if( '0000' !== $SQLerrorInfo[0] ){
					try{
						throw new SQLException( $SQLerrorInfo[0].' '.$SQLerrorInfo[1].' '.$SQLerrorInfo[2] );
					}catch ( SQLException $ex ) {
						LogInfoHandler::log( 'SQL operation unsuccessful!' );
						LogInfoHandler::log( $SQLerrorInfo );
						$ex->log();
						$this->setDbTransactionStatus( false );
					}
				}
				else{
					while( $row = $stmt->fetch( PDO::FETCH_ASSOC ) ){
							$this->setOutputArray( $row );
					}
					LogInfoHandler::log( 'Database operation successful!' );
					$this->setDbTransactionStatus(true);
					$this->setLastInsertId( $connObj->lastInsertId() );
				}
			}
			break;
			case 'UPDATE':{
				//UPDATE pdotest SET `id`='2',`name`='Bhaskar' WHERE `id` = '1'
				$this->_queryString = $queryType.' '.'`'.DATABASE_NAME.'`.`'.$tableName.'` SET '.$updateString.' WHERE '.$constraintString ;
				//var_dump($this->_queryString);
				// Execute query and collect results
				$connObj = parent::getWorkflowArtifact( 'connectionObject' );
				LogInfoHandler::log( $this->_queryString );
				$stmt = $connObj->prepare( $this->_queryString );
				$stmt->execute();
				$SQLerrorInfo = $stmt->errorInfo();
				if( 0 === $stmt->rowCount() ){
					$this->setEmptyResultSet();
				}
				else if( 0 !== $stmt->rowCount() ) {
					$this->_emptyResultSetFlag = false;
					$this->setRowCount( $stmt->rowCount() );
				}
				//var_dump($SQLerrorInfo[0] !== "00000");
				// Check if the error string is non-zero - This means, query failed due to some kind of error.
				// Throw an exception and log the message in the log file.
				if($SQLerrorInfo[0] !== '00000' ) {
					try{
						throw new SQLException( $SQLerrorInfo[0].' '.$SQLerrorInfo[1].' '.$SQLerrorInfo[2] );
					} catch (SQLException $ex) {
						LogInfoHandler::log( 'SQL operation unsuccessful!' );
						LogInfoHandler::log($SQLerrorInfo);
						$ex->log();
						$this->setDbTransactionStatus( false );
					}
				}
				else{
					while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
							$this->setOutputArray($row);

					}
					LogInfoHandler::log( 'Database operation successful!' );
					$this->setDbTransactionStatus( true );
				}
			}
			break;
			case 'DELETE':{
				if( 0 === sizeof($this->_constraint) ) {
					throw new SQLException( 'DELETE query needs atleast 1 CONSTRAINT field to proceed!' ); // Throw exception here, to prevent unchecked deletion
				}
				else{
					$this->_queryString = $queryType.' FROM `'.DATABASE_NAME.'`.`'.$tableName.'` WHERE '.$constraintString;
				}
				$connObj = parent::getWorkflowArtifact( 'connectionObject' );
				LogInfoHandler::log( $this->_queryString );
				$stmt = $connObj->prepare( $this->_queryString );
				$stmt->execute();
				$SQLerrorInfo = $stmt->errorInfo();
				if( 0 === $stmt->rowCount() ) {
					$this->setEmptyResultSet();
				}
				else if( 0 !== $stmt->rowCount() ) {
					$this->_emptyResultSetFlag = false;
					$this->setRowCount( $stmt->rowCount() );
				}
				if( $SQLerrorInfo[0] !== "00000" ) {
					try{
						throw new SQLException( $SQLerrorInfo[0].' '.$SQLerrorInfo[1].' '.$SQLerrorInfo[2] );
					} catch ( SQLException $ex ) {
						LogInfoHandler::log( 'SQL operation unsuccessful!' );
						LogInfoHandler::log( $SQLerrorInfo );
						$ex->log();
						$this->setDbTransactionStatus( false );
					}
				}
				else{
					while( $row = $stmt->fetch( PDO::FETCH_ASSOC ) ){
						$this->setOutputArray( $row );
					}
					LogInfoHandler::log( 'Database operation successful!' );
					$this->setDbTransactionStatus( true );
					$this->setLastInsertId( $connObj->lastInsertId() );
				}

			}
		}
		// Always returns null
	}

	public function defineWorkflowArtifacts() {
		// Overridden here - Defining workflow artifacts
		// Variable declaration
		parent::setWorkflowArtifact( 'SQLTargetName', $this->getSQLTargetName() );
		parent::setWorkflowArtifact( 'queryType', $this->getQueryType() );
		parent::setWorkflowArtifact( 'constraint', $this->getConstraint() );
		parent::setWorkflowArtifact( 'fieldNameArray', $this->getFieldNameArray() );
	}

	public function getDatasetArray() {
		return parent::getDatasetArray();
	}

	public function getDatasetByName( $datasetName ) {
		return parent::getDatasetByName( $datasetName );
	}

	public function getWorkflowArtifact( $artifactName ) {
		return parent::getWorkflowArtifact( $artifactName );
	}

	public function initModel() {
		parent::initModel();
	}

	public function isErrorFlagSet() {
		return parent::isErrorFlagSet();
	}

	public function loadWorkflowArtifacts() {
		parent::loadWorkflowArtifacts();
	}

	public function processError() {
		parent::processError();
	}

	public function processWorkflow() {
		parent::processWorkflow();
	}

	public function pushError($errorText) {
		parent::pushError($errorText);
	}

	public function setDatabaseWorkflow() {
		parent::setDatabaseWorkflow();
	}

	public function setDataset( $datasetName, \Dataset $Dataset ) {
		parent::setDataset( $datasetName, $Dataset );
	}

	public function setErrorFlag() {
		parent::setErrorFlag();
	}

	public function setJSONWorkflow() {
		parent::setJSONWorkflow();
	}

	public function setWorkflowArtifact( $artifactName, $artifact ) {
		parent::setWorkflowArtifact( $artifactName, $artifact );
	}

	public function setXMLWorkflow() {
		parent::setXMLWorkflow();
	}
	
	public function getQueryType() {
		return $this->_queryType;
	}
	
	public function getFieldNameArray() {
		return $this->_fieldNameArray;
	}

	public function setQueryType( $queryType ) {
		$this->_queryType = $queryType;
	}

	public function setFieldName( $fieldName ) {
		array_push( $this->_fieldNameArray,$fieldName );
	}
	
	public function setFieldNameArray( array $fieldNameArray ){
		$this->_fieldNameArray = $fieldNameArray;
	}
	
	public function setSelectQueryType(){
		$queryType = 'SELECT';
		$this->setQueryType($queryType);
	}
	
	public function setInsertQueryType(){
		$queryType = 'INSERT';
		$this->_queryType = $queryType;
	}
	public function setUpdateQueryType(){
		$queryType = 'UPDATE';
		$this->setQueryType( $queryType );
	}

	public function setDeleteQueryType(){
		$queryType = 'DELETE';
		$this->setQueryType( $queryType );
	}

	public function getConstraintNameList() {
		return array_keys( $this->_constraint );
	}

	public function setConstraint( $constraintName, $constraintValue ) {
		$this->_constraint[$constraintName] = $constraintValue;
	}
	
	public function getConstraintValue( $constraintName ) {
		return $this->_constraint[$constraintName];
	}
	
	public function getConstraint(){
		return $this->_constraint;
	}
	
	public function getSQLTargetName() {
		return $this->_SQLTargetName;
	}

	public function setSQLTargetName( $SQLTargetName ) {
		$this->_SQLTargetName = $SQLTargetName;
	}
	
	public function buildQueryString( $currentString, $addedSnippet, $separator_string ){
		LogInfoHandler::log();
		if( 0 < strlen($currentString) ){
			$currentString .= $separator_string.' '.$addedSnippet;
			LogInfoHandler::log($currentString);
		}
		else if( 0 === strlen($currentString) ){
			$currentString .= $addedSnippet;
		}
		return $currentString;
	}
	
	public function getUpdateArray() {
		return $this->_updateArray;
	}

	public function setUpdatedFieldValue( $updatedField, $updatedValue ) {
		$this->_updateArray[$updatedField] = $updatedValue;
	}
	public function setUpdatedFieldValueArray( array $UpdateArray ){
		$this->_updateArray = $UpdateArray;
		//var_dump($this->_updateArray);
	}
	
	public function getSQLConnectionObject() {
		return $this->_SQLConnectionObject;
	}
	
	public function getOutputArray() {
		return $this->_outputArray;
	}

	public function setOutputArray( $outputArrayItem ) {
		LogInfoHandler::log();
		array_push( $this->_outputArray, $outputArrayItem );
	}
	
	public function setInsertArray( array $insertArray ){
		foreach( $insertArray as $key=>$value ){
			$this->_insertArray[$key] = $value;
		}
	}
	public function resetModel(){
		// Set all work items to their initial value
		$this->_constraint          = array();
		$this->_SQLConnectionObject = null;
		$this->_fieldNameArray      = array();
		$this->_updateArray         = array();
		$this->_outputArray         = array();
		$this->_insertArray         = array();
		// $this->_deleteArray = array(); -- // This is not required as "Delete" is expected to eliminate 1 record upon execution.
	}
	
	public function getDbTransactionStatus() {
		LogInfoHandler::log();
		return $this->_dbTransactionStatus;
	}

	public function setDbTransactionStatus( $dbTransactionStatus ) {
		LogInfoHandler::log();
		$this->_dbTransactionStatus = $dbTransactionStatus;
	}
	
	public function isResultSetEmpty(){
		return $this->_emptyResultSetFlag;
	}
	
	public function setEmptyResultSet(){
		$this->_emptyResultSetFlag = true;
	}
		
	public function returnSortedResultSet(){
		$this->_returnSortedResultSet = true;
		$this->_sortDesc = false;
	}
	
	public function isResultSetSorted(){
		
		return $this->_returnSortedResultSet;
	}
	
	public function sortResultSetDesc(){
		LogInfoHandler::log();
		$this->_sortDesc = true;
	}
	
	public function isResultSetSortedDesc(){
		return $this->_sortDesc;
	}
	
	public function isResultSetLimited(){
		return $this->_limitedResultSet;
	}
	
	public function returnLimitedResultSet(){
		$this->_limitedResultSet = true;
	}
	
	public function setResultSetLimit( $limit ){
		$this->_resultSetLimit = $limit;
	}
	
	public function getResultSetLimit(){
		return $this->_resultSetLimit;
	}
	
	public function setSortByField( $fieldName ){
		LogInfoHandler::log();
		$this->_sortByField = $fieldName;
	}
	
	public function getSortByField(){
		return $this->_sortByField;
	}
	
	public function setRowCount( $count = 0 ){
		$this->_rowCount = $count;
	}
	
	public function getRowCount(){
		return $this->_rowCount;
	}
	
	public function setQueryString( $queryString ){
		$this->_queryString = $queryString;
	}
	
	public function getQueryString(){
		return $this->_queryString;
	}
	
	public function getLastInsertId() {
		return $this->_lastInsertId;
	}

	public function setLastInsertId( $lastInsertId ) {
		$this->_lastInsertId = $lastInsertId;
	}
	
	public function __destruct() {
		//unset(constant(LOG_EVENT));
		//@define("LOG_EVENT", false);
		//unset(constant(LOG_EVENT));
	}
}
