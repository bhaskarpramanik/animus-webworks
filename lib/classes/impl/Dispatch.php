<?php
/**
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 * 
 * Description:
 * Simple class to dispatch responses to the incoming request
 */

require_once 'URLRouter.php';
require_once 'ViewPropertyHandler.php';

class Dispatch {
	/*
	 * Contain the mapped view name
	 */
	private $_mapped_view_name;

	/*
	 * Contain view property handler instance
	 */
	private $_viewPropertyHandler;

	/*
	 * Contain status code
	 */
	private $_status_code;

	/*
	 * Contain re-route request flag
	 */
	private $_re_route;

	/*
	 * Contain user information
	 */
	private $_user;

	/*
	 * Contain redirection URL
	 */
	private $_redirectionURL;

	/*
	 * Contain redirection handler
	 */
	private $_redirectionHandler;

	/*
	 * Contain initially requested URL
	 */
	private $_requestedURI;

	public function __construct() {
		LogInfoHandler::log();
		$this->_viewPropertyHandler = new ViewPropertyHandler();
	}
	public function dispatchURL( $requested_uri, $forceStrictMatch ) {
		LogInfoHandler::log();
		$url_router          = new URLRouter();
		$isURLMapLoaded      = $url_router->loadDeploymentMap(); // Throws Managed Exception
		$this->_requestedURI = $requested_uri;
		if( $isURLMapLoaded ) {
			$deployedURLMap  = $url_router->getDeployedURLList();
			foreach( array_keys( $deployedURLMap ) as $key ) {
				$match = preg_match( $key, $requested_uri, $match_array );
				if( ( boolean )$match ) {
					$mapped_view_name = $deployedURLMap[$key];
					$this->setMappedViewName( $mapped_view_name );
					
					// If the path matches 'exactly' then continue to process or else declare 404
					if( $forceStrictMatch ) {
						if( $this -> _requestedURI === $match_array[0] ) {
							$this -> setMappedViewName( $mapped_view_name );
							$this -> setStatusCode( 202 );
							$url_found = true;
							break;
						}
						else {
							$this -> _mapped_view_name = null;
							$url_found                 = false;
							// No view name to set here - set status code and proceed
							// This will set mapped_view_name as null, even for deployed paths, if only called without file.exts*
							$this -> setStatusCode( ERROR_404 );
						}
					}
					else {
						/*
						* Status code set to 202
						* HTTP response code 202 - See def here - https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
						*/
						$this -> setMappedViewName($mapped_view_name);
						$this -> setStatusCode( 202 );
						$url_found = true;
						break;
					}
				}
				else {
					$this->_mapped_view_name = null;
					$url_found               = false;
					// No view name to set here - set status code and proceed
					$this->setStatusCode( ERROR_404 );
				}
			}
		}
		return $url_found;
	}

	/*
	 * Description:
	 * This method processes error codes
	 * 
	 * Usage:
	 * This function is called when error needs to be processed
	 */
	public function processError() {
		LogInfoHandler::log();
		/*
		 * Step-1: Resolve redirection on this error code
		 * Step-2: Once redirection is resolved
		 * Step-3: Handle redirection or just create headers
		 */
		$isRedirected = $this->resolveErrorRedirection();
		return $isRedirected;
	}
	 /*
	  * Method: processDeployedRoute()
	  * Description:
	  * This method processes the deployed route, once initial routing has been figured out
	  */
	public function processDeployedRoute() {
		LogInfoHandler::log();
		/*
		 * While calling the view property handler,
		 * the name of the view needs to be passed
		 * so that the appropriate property file can be loaded
		 */

		/*
		 * Here re-direction check is needed to
		 * determine if the programmer has configred
		 * any redirection on correct view
		 */
		$isRedirected = $this->resolveRedirection();
		return $isRedirected;
	}   
	/*
	 * Method: resolveRedirection
	 * Description: 
	 * This method is used to resolve redirection on status codes
	 * the admin of the app can provide settings which will
	 * guide, what is the behavior of the tool when it encounters
	 * an error.
	 * Returns: Bool
	 */
	public function resolveRedirection() {
		LogInfoHandler::log();
		// Scope variables declaration
		$isRedirected        = false;
		$isRedirectionForced = false;
		$isUnderMaintenance  = false;
		$isAuthRedirected    = false;
		$redirectionURL      = '';
				
		/*
		 * Step-1 Load the view properties
		 */
		$isReRouteRequest = $this->isReRouteRequest();
		if( !$isReRouteRequest ) {
			$this->_viewPropertyHandler->setViewName( $this->getMappedViewName() );
			$this->_viewPropertyHandler->resolveViewProperties();
			$this->_viewPropertyHandler->setStatusCode( $this->getStatusCode() );
		   /*
			* Step-2 Check for redirections
			* Order of preference:
			* 1. Forced redirections
			* 2. Maintenance redirections
			* 3. Authentication redirection
			*/
		   $isRedirectionForced = $this->_viewPropertyHandler->getViewPropertyArray()->getViewRedirected();
		   $isUnderMaintenance  = $this->_viewPropertyHandler->getViewPropertyArray()->getPhaseMaint();
		   $isAuthRedirected    = $this->_viewPropertyHandler->getViewPropertyArray()->getAuthRequired();

		   if( $isRedirectionForced ) {
			   LogInfoHandler::log( 'Forced redirection in effect!' );
			   $isRedirected   = $this->resolveForcedRedirection();
			   $redirectionURL = $this->_viewPropertyHandler->getRedirectionURL();
			   $this->setRedirectionURL($redirectionURL);
			   return $isRedirected;
		   }
		   else if($isUnderMaintenance) {
			   LogInfoHandler::log( 'Maintenance redirection in effect!' );
			   $isRedirected = $this->resolveMaintenanceRedirection();
			   return $isRedirected;
		   }
		   else if($isAuthRedirected) {
			   LogInfoHandler::log( 'Authentication redirection in effect!' );
			   $isRedirected   = $this->resolveAuthRedirection();
			   $redirectionURL = $this->_viewPropertyHandler->getRedirectionURL();
			   $this->setRedirectionURL($redirectionURL);
			   return $isRedirected;
		   }
		   else {
			   // No redirection is in place!
			   return $isRedirected;
		   }
		}
		else if($isReRouteRequest) {
			$isRedirected = $this->resolveErrorRedirection();
			unset($this->_viewPropertyHandler);
			LogInfoHandler::log( 'Resolving error redirection for status code '.$this->getStatusCode() );
			return $isRedirected;
		}
	}

	public function resolveForcedRedirection() {
		LogInfoHandler::log();
		$isRedirected = $this->_viewPropertyHandler-> validateForcedRedirection();
		$statusCode   = $this->_viewPropertyHandler->getStatusCode();
		$this->setStatusCode( $statusCode );
		return $isRedirected;
	}

	public function resolveMaintenanceRedirection() {
	   LogInfoHandler::log();
		$isRedirected = $this->_viewPropertyHandler->validateMaintenanceRedirection();
		$statusCode   = $this->_viewPropertyHandler->getStatusCode();
		$this->setStatusCode( $statusCode );
		return $isRedirected;
	}

	public function resolveAuthRedirection() {
		LogInfoHandler::log();
		$isRedirected = $this->_viewPropertyHandler->validateAuthRedirection( $this->getUser()->getAuthSignature() );
		$statusCode   = $this->_viewPropertyHandler->getStatusCode();
		$this->setStatusCode( $statusCode );
		return $isRedirected;
	}

	public function resolveErrorRedirection() {
		LogInfoHandler::log();
		//$this->setStatusCode(500);
		$statusCode            = $this->getStatusCode();
		$isRedirectionRequired = constant( 'REDIRECT_'.$statusCode );
		LogInfoHandler::log( 'Processing redirection settings for '.$statusCode );
		return $isRedirectionRequired;
	}

	/*
	 * Get the redirect location - From the existing status code
	 */
	public function getRedirectionURL() {
		LogInfoHandler::log();
		$statusCode = $this->getStatusCode();
		switch( ( int ) $statusCode ) {
			case 301: { 
				$redirectedURL = !is_null( $this->_redirectionURL )?$this->_redirectionURL : URL_500;
			}
			
			break;
		
			case 302: {
				$redirectedURL = !is_null( $this->_redirectionURL )?$this->_redirectionURL : URL_500;
			}
			
			break;
		
			case 401: {
				$redirectedURL = !is_null( $this->_redirectionURL )?$this->_redirectionURL : URL_500;
			}
			
			break;
		
			case 403: {
				$redirectedURL = !is_null( $this->_redirectionURL )?$this->_redirectionURL : URL_500;
			}
			
			break;
		
			case 404: {
				$redirectedURL = URL_404;
			}
			
			break;
		
			case 500: {
				$redirectedURL = URL_500;
			}
			
			break;
		
			case 503: {
				$redirectedURL = URL_503;
			}
			
			break;
		
		}
		return $redirectedURL;
	}

	public function getRedirectionHandlerName() {
		LogInfoHandler::log();
		$statusCode = $this->getStatusCode();
		switch((int)$statusCode) {
			case 301: {
				$redirectionHandler = 'PermanentRedirection';
			}
			
			break;
			
			case 302: {
				$redirectionHandler = 'TemporaryRedirection';
			}
			
			break;
		
			case 401: {
				$redirectionHandler = 'AuthenticationRedirection';
			}
			
			break;
		
			case 403: {
				$redirectionHandler = 'AuthenticationRedirection';
			}
			
			break;
		
			case 404: {
				$redirectionHandler = 'ErrorRedirection';
			}
			
			break;
		
			case 500: {
				$redirectionHandler = 'TempErrorRedirection';
			}
			
			break;
		
			case 503: {
				$redirectionHandler = 'MaintenanceRedirection';
			}
			
			break;
		
		}
		
		return $redirectionHandler;
	}

	public function executeRedirection( $serverProtocol ) {
		LogInfoHandler::log();
		require_once 'RedirectionView.php';
		$redirectionHandler = $this->getRedirectionHandler();
		$headerInfoHandler  = new HeaderInfoHandler();
		
		// Retrieve all the HTTP headers
		$statusCodes = $this->_redirectionHandler->getRedirectionHeaderCode();	
		// Generate all the headers
		foreach( $statusCodes as $headerCode ) {
			$headerString = $headerInfoHandler->generateHTTPHeader( $serverProtocol, $headerCode );
			$headerInfoHandler->setHeaderArray( $headerString );
		}
		
		// Get the redirection location
		$redirectionLocation = $redirectionHandler->getRedirectionLocation();

		// Set the URL to HeaderInfoHandler and request location header
		$headerInfoHandler->setLocationHeaderPath( $redirectionLocation );
		LogInfoHandler::log( 'Execute redirection' );
		$redirectionFlag = $redirectionHandler->getRedirectionFlag();
		if($redirectionFlag) {
			$headerInfoHandler->generateLocationHeader( $serverProtocol, $redirectionHandler->getRedirectionFlag() );
		}
		$headerArray = $headerInfoHandler->getHeaderArray();
		
		// Create an instance of redirection view
		$redirectionView = new RedirectionView();
		
		// Inject the header array into this view
		$redirectionView->setHeaderArray( $headerArray );
		
		// Return the view object
		return $redirectionView;
	}
	/*
	 * Getters and setters
	 */

	public function setMappedViewName( $viewName ) {
		LogInfoHandler::log();
		$this->_mapped_view_name = $viewName;
	}

	public function getMappedViewName() {
		LogInfoHandler::log();
		return $this->_mapped_view_name;
	}

	public function setStatusCode( $statusCode ) {
		LogInfoHandler::log();
		$this->_status_code = $statusCode;
	}

	public function getStatusCode() {
		LogInfoHandler::log();
		return $this->_status_code;
	}

	public function requestReRoute() {
		LogInfoHandler::log();
		$this->_re_route = true;
	}

	public function isReRouteRequest() {
		LogInfoHandler::log();
		return $this->_re_route;
	}

	public function setUser( User $user ) {
		LogInfoHandler::log();
		$this->_user = $user;
	}

	public function getUser() {
		LogInfoHandler::log();
		return $this->_user;
	}

	public function setRedirectionURL( $redirectionURL ) {
	  LogInfoHandler::log();
		$this->_redirectionURL = $redirectionURL;
	}

	public function setRedirectionHandler( Redirection $redirectionHandler ) {
		LogInfoHandler::log();
		$this->_redirectionHandler = $redirectionHandler;
	}

	public function getRedirectionHandler() {
		return $this->_redirectionHandler;
	}

	public function getRequestedURI() {
		return $this->_requestedURI;
	}

	public function getViewPropertyArray() {
		LogInfoHandler::log();
		$propertyArray = $this->_viewPropertyHandler->getViewPropertyArray();
		return $propertyArray;
	}

	public function unsetViewPropertyHandler() {
		unset( $this->_viewPropertyHandler );
	}
	public function __destruct() {
		LogInfoHandler::log();
	}
}
?>