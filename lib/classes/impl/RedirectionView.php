<?php
/**
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 * 
 * Description
 * View class used internally for processing redirections
 */
require_once 'StaticView.php';
class RedirectionView extends StaticView {
	public function __construct() {
	   LogInfoHandler::log();
	   parent::__construct();
	}

	public function getHeader( $key ) {
	   LogInfoHandler::log();
	   return parent::getHeader( $key );
	}

	public function getHeaderArray() {
	   LogInfoHandler::log();
	   return parent::getHeaderArray();
	}

	public function getPageContent() {
	   LogInfoHandler::log();
	   return parent::getPageContent();
	}

	public function getUserDataArray() {
	   LogInfoHandler::log();
	   return parent::getUserDataArray();
	}

	public function output() {
	   LogInfoHandler::log();
	   $headerArray = $this->getHeaderArray();
	   foreach( $headerArray as $header ){
		  header( $header );
	   }
	}

	public function setHeader( $key, $value ) {
	   LogInfoHandler::log();
	   parent::setHeader( $key, $value );
	}

	public function setHeaderArray( $headerArray ) {
	   LogInfoHandler::log();
	   parent::setHeaderArray( $headerArray );
	}

	public function setPageContent( $pageContent ) {
	   LogInfoHandler::log();
	   parent::setPageContent( $pageContent );
	}

	public function setUserData( $key, $value ) {
	   LogInfoHandler::log();
	   parent::setUserData( $key, $value );
	}

	public function setUserDataArray( $dataArray ) {
	   LogInfoHandler::log();
	   parent::setUserDataArray( $dataArray );
	}

	public function __destruct() {
	   LogInfoHandler::log();
	}

}
