<?php
/**
 *
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 * 
 * Description of ViewImpl
 * Implements the methods defined in the View interface. Performs the following functions.
 * 1. Attach header information to the view (HTTP headers)
 * 2. Sets the data in the view.
 * 3. Implements any actions on the view, as defined in the properties.
 */

require_once CLASSPATH_ABSTRACT.'/View.php';
class ViewImpl implements View  {
	private $_headerArray;
	private $_userDataArray;
	private $_loadStaticContent;
	private $_content;
	private $_theme;

	public function __construct() {
		LogInfoHandler::log();
		$this->_headerArray       = array();
		$this->_userDataArray     = array();
		$this->_loadStaticContent = true;
		$this->_content           = array();
	}
	
	public function setHeaderArray( $headerArray ) {
	  LogInfoHandler::log();
		$this->_headerArray = $headerArray;
	}
	
	public function setHeader( $key, $value ) {
		LogInfoHandler::log();
		$headerArray       = $this->getHeaderArray();
		$headerArray[$key] = $value;
	}
	
	public function getHeaderArray() {
	  LogInfoHandler::log();
		return $this->_headerArray;
	}
	
	public function getHeader( $key ) {
	  LogInfoHandler::log();
		return $this->_headerArray[$key];
	}
	
	public function setUserDataArray( $dataArray )  {
		$this->_userDataArray = $dataArray;
	}
	
	public function getUserDataArray() {
		return $this->_userDataArray;
	}
	
	public function setUserData( $key, $value ) {
		$this->_userDataArray[$key] = $value;
	}
	
	public function setNoSideload() {
		$this->_loadStaticContent = false;
	}
	
	public function checkSideload() {
		return $this->_loadStaticContent;
	}
	
	public function output() {
	 //Output everything to the browser
		LogInfoHandler::log();
		$this->initTheme();
		$headerArray   = $this->getHeaderArray();
		$userDataArray = $this->getUserDataArray();
		$sideload      = $this->checkSideload();
		$content       = $this->getContent();
		if( 0 < sizeof( $content ) ) {
			foreach( $content as $content ) {
				echo $content;
			}
		}
		if( isset( $userDataArray ) ) {
			foreach( $userDataArray as $userData ) {
				echo $userData;
		  }
		}
		else {
			foreach( $headerArray as $header ) {
				header( $header );
			}
		}
	}
	
	public function getContent()  {
		return $this->_content;
	}

	public function setContent( $content, $contentType )  {
		if( $this->checkSideload() ) {
		  header( $contentType );
		  $this->_content = $content;
		}
		else {
		  LogInfoHandler::log( 'View sideload is disabled after controller execution. No static content loaded!' );
		}
	}
	
	public function initTheme() {
		LogInfoHandler::log();
		$this->_theme = new ThemeLoader();
		$this->_theme->loadTheme( DEFAULT_THEME );
		require_once $this->_theme->getThemePath().'/src/index.php';
	}
}
