<?php

/**
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 * 
 * Description
 * Simple class keep sessions for users
 */
require_once 'User.php';
class Session {
	/*
	 * Contain if session valid
	 */
	private $_isValid;

	/*
	 * Contain session id
	 */
	private $_sessionId;

	/*
	 * Conain user
	 */
	private $_user;

	/*
	 * Contain last access time
	 */
	private $_lastRequestTime;

	/*
	 * Contain timeout
	 */
	private $_timeout;

	/*
	 * Contain the callback URL in case of redirections
	 */
	private $_callbackURL;
	
	
	/*
	 * Hold intermediate data during redirect
	 */
	private $_formData;

	public function __construct() {
		LogInfoHandler::log();
		@session_start();
		$this->_user = new User();
		if( is_null( $_SESSION[SESSION_NAME] ) ) {
			// Set a generic session without any user info
			$this->setSession();
		}
		else {
			$this->_isValid = $_SESSION[SESSION_NAME]->isValid();
		}
	}

	public function isValid() {
		LogInfoHandler::log();
		return $this->_isValid;
	}

	public function getSessionId() {
		LogInfoHandler::log();
		return $this->_sessionId;
	}

	public function getUser() {
		LogInfoHandler::log();
		return $this->_user;
	}

	public function setIsValid() {
		LogInfoHandler::log();
		$this->_isValid = true;
		$this->updateSession();
	}

	public function invalidate() {
		LogInfoHandler::log();
		$this->_isValid = false;
		$this->flushUser();
		$this->updateSession();
	}

	public function setSessionId( $sessionId ) {
		LogInfoHandler::log();
		$this->_sessionId = $sessionId;
		LogInfoHandler::log( $sessionId );
	}

	public function setUser(User $user) {
		LogInfoHandler::log();
		$this->_user = $user;
	}

	public function setSession( string $sessionId = NULL ) {
		LogInfoHandler::log();
		$randomNum = rand( 0, 99999 );
		if (!isset($sessionId)) {
			$sessionId = hash( 'md5', $this->getLastRequestTime() );
		}
		$this->setLastRequestTime();
		$this->setSessionId($sessionId);
		$this->setIsValid();
		$this->updateSession();
	}

	public function getLastRequestTime() {
		LogInfoHandler::log();
		return $this->_lastRequestTime;
	}

	public function setLastRequestTime() {
		LogInfoHandler::log();
		$this->_lastRequestTime = $_SERVER['REQUEST_TIME'];
	}

	public function manageSession() {
		LogInfoHandler::log();
		$sessionTimeout = TIMEOUT_SESSION;
		$timeout        = TIMEOUT_SESSION * 60;
		@session_start();
		$this->setTimeout($timeout);
	}

	public function getSession() {
		LogInfoHandler::log();
		@session_start();
		if (isset($_SESSION[SESSION_NAME])) {
			if ($_SESSION[SESSION_NAME]->isValid()) {
				$_SESSION[SESSION_NAME]->setLastRequestTime();
			}
		}
		else {
			$this->updateSession();
		}
		return $_SESSION[SESSION_NAME];
	}
	
	public function updateSession() {
		LogInfoHandler::log();
		$_SESSION[SESSION_NAME] = $this;
	}

	public function closeSession() {
		LogInfoHandler::log();
		@session_start();
		if ( isset( $_SESSION[SESSION_NAME] ) ) {
			if ( $_SESSION[SESSION_NAME]->isValid() ) {
				$_SESSION[SESSION_NAME]->invalidate();
			}
		}
	}

	public function setTimeout($timeout) {
		LogInfoHandler::log();
		$this->_timeout = $timeout;
	}

	function getCallbackURL() {
		LogInfoHandler::log();
		return $this->_callbackURL;
	}

	function setCallbackURL($callbackURL) {
		LogInfoHandler::log();
		$this->_callbackURL = $callbackURL;
	}
	
	function setRedirectionFormData($formdata) {
		LogInfoHandler::log();
		$this->_formData = $formdata;
	}
	
	function getRedirectionFormData() {
		LogInfoHandler::log();
		return $this->_formData;
	}
	
	function flushCallbackURL(){
		LogInfoHandler::log();
		$this->_callbackURL = NULL;
	}
	
	function flushFormData() {
		LogInfoHandler::log();
		$this->_formData = NULL;
	}
	
	function flushUser( ) {
		LogInfoHandler::log();
		$this->_user = new User();
	}
}
