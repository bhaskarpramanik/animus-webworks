<?php
/**
 * Description of StaticView extends ViewImpl
 * Class to present and prepare the view layer from user defined view properties
 *
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 */

class StaticView extends ViewImpl {

	public function __construct() {
		LogInfoHandler::log();
		parent::__construct();
	}

	public function output() {
		parent::output();
	}

	public function getHeader(  $key  ) {
		LogInfoHandler::log();
		return parent::getHeader(  $key  );
	}

	public function getHeaderArray() {
		LogInfoHandler::log();
		return parent::getHeaderArray();
	}

	public function getUserDataArray() {
		LogInfoHandler::log();
		return parent::getUserDataArray();
	}

	public function setHeader( $key, $value ) {
		LogInfoHandler::log();
		parent::setHeader( $key, $value );
	}

	public function setHeaderArray( $headerArray ) {
		LogInfoHandler::log();
		parent::setHeaderArray( $headerArray );
	}

	public function setUserData( $key, $value ) {
		LogInfoHandler::log();
		parent::setUserData( $key, $value );
	}

	public function setUserDataArray( $dataArray ) {
		LogInfoHandler::log();
		parent::setUserDataArray( $dataArray );
	}

	public function checkSideload() {
		return parent::checkSideload();
	}

	public function setNoSideload() {
		parent::setNoSideload();
	}

	public function getContent() {
		return parent::getContent();
	}

	public function setContent( $content, $contentType ) {
		@parent::setContent( $content, $contentType );
	}
}
