<?php
/**
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 */

/*
 * Require the interface
 */
require_once dirname( dirname( __FILE__ ) ).'/abstract/Model.php';
require_once 'LogInfoHandler.php';

class ModelImpl implements Model {
    
	/*
	 * Contain the workflow array
	 */
    private $_workflowArray;

	/*
	 * Contain the error array
	 */
    private $_errorArray;

	/*
	 * Contain the error flag
	 */
    private $_errorFlag;

	/*
	 * Contain workflow artifacts
	 */
    private $_workflowArtifacts;

	/*
	 * Contain dataset array
	 */
    private $_datasetArray;
    
    public function __construct(){
        LogInfoHandler::log();
        $this->_workflowArray     = array();
        $this->_errorArray        = array();
        $this->_errorFlag         = array();
        $this->_workflowArtifacts = array();
        $this->_datasetArray      = null;
    }
    
    public function initModel(){
        $this->_workflowArray['isDatabaseWorkflow'] = false;
        $this->_workflowArray['isXMLWorkflow']      = false;
        $this->_workflowArray['isJSONWorkflow']     = false;
    } 
    
    public function setSQLWorkflow() {
        $this->_workflowArray['isDatabaseWorkflow'] = true;
        $this->_workflowArray['isXMLWorkflow']      = false;
        $this->_workflowArray['isJSONWorkflow']     = false;
    }
    
    public function setXMLWorkflow() {
        $this->_workflowArray['isXMLWorkflow']      = true;
        $this->_workflowArray['isDatabaseWorkflow'] = false;
        $this->_workflowArray['isJSONWorkflow']     = false;
    }
    
    public function setJSONWorkflow() {
        $this->_workflowArray['isJSONWorkflow']     = true;
        $this->_workflowArray['isDatabaseWorkflow'] = true;
        $this->_workflowArray['isDatabaseWorkflow'] = false;
    }
    
    public function processWorkflow() {
        foreach($this->_workflowArray as $workflow_exists){
            if($workflow_exists){
                $this->callWorkflow();
            }
        }
    }
    
	/*
	 * @override
	 */
    public function defineWorkflowArtifacts(){
        /*
         * Workflow definition:
         * In case the model is running is DB mode, one needs to provide the query - Model executes the query and returns the resultset or error as applicable
         * In case the model is running in XML mode, one needs to provide the XML path/URL from which the XML has to be read
         * In case the model is running in JSON mode, one needs to provide the JSON path/URL from which the JSON has to be read
         */
    }
    
    public function defineWorkflow() {
        // Overriden in the child class
    }
    
    public function loadWorkflowArtifacts() {
        // Overridden in child class
    }
    
    public function callWorkflow(){
        // Overridden in child class
    }
    
    public function processError(){
        // Overriden in child class
    }
    
    public function setErrorFlag(){
        $this->_errorFlag = true;
    }
    
    public function isErrorFlagSet(){
        return $this->_errorFlag;
    }
    
    public function pushError( $errorText ){
        array_push( $this->_errorArray, $errorText );
    }
    
    public function getDatasetArray() {
        return $this->_datasetArray;
    }

    public function getDatasetByName( $datasetName ) {
        return $this->_datasetArray[$datasetName];
    }

    public function setDataset( $datasetName, Dataset $Dataset ) {
        $this->_datasetArray[$datasetName] = $Dataset;
    }
    
    public function setWorkflowArtifact( $artifactName, $artifact ){
        $this->_workflowArtifacts[$artifactName] = $artifact;
    }
    
    public function getWorkflowArtifact( $artifactName ){
        return $this->_workflowArtifacts[$artifactName];
    }
    
    public function setDBConnectionArtifact( $connObj ){
        $this->_workflowArtifacts['connectionObject'] = $connObj;
    }
    
    public function getDBConnectionArtifact(){
        return $this->_workflowArtifacts['connectionObject'];
    }
}
