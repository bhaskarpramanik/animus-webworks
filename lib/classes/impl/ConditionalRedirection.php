<?php

/**
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 * 
 * Description:
 * Class to handle conditional redirection
 */
require_once CLASSPATH_IMPL.'/RedirectionImpl.php';

class ConditionalRedirection extends RedirectionImpl {

	/*
	 * Contain initial URL
	 */
	private $_initial_URL;

	/*
	 * Contain callback URL
	 */
	private $_callback_URL;

	/*
	 * Contain redirection cause
	 */
	private $_redirection_cause;

	/*
	 * Contain redirection header code
	 */
	private $_redirection_header_code;

	/*
	 * Contain redirection location
	 */
	private $_redirection_location;

	/* 
	 * Contain redirection type
	 */
	private $_redirection_type;

	/*
	 * User data array
	 */
	private $_user_data_array;

	public function __construct(){
		LogInfoHandler::log();
		$this->_redirection_header_code = array();
	}

	public function setInitialURL( $initial_url ) {
		LogInfoHandler::log();
		$this->_initial_URL = $initial_url;
	}

	public function setCallbackURL( $callback_url ) {
		LogInfoHandler::log();
		$this->_callback_URL = $callback_url;
	}

	public function setRedirectionCause( $redirect_cause ) {
		LogInfoHandler::log();
		$this->_redirection_cause = $redirect_cause;
	}

	public function setRedirectionHeaderCode( $redirect_header_code ) {
		LogInfoHandler::log();
		array_push( $this->_redirection_header_code ,$redirect_header_code );
	}

	public function setRedirectionLocation( $redirect_location ) {
	LogInfoHandler::log();
	$this->_redirection_location = $redirect_location;
	}

	public function setRedirectionType( $redirect_type ) {
		LogInfoHandler::log();
		$this->_redirection_type = $redirect_type;
	}

	public function setUserDataArray( $user_data_array ){
		LogInfoHandler::log();
		$this->_user_data_array = array();
		$this->_user_data_array = $user_data_array;
	}

	public function getInitialURL() {
		LogInfoHandler::log();
		return $this->_initial_URL;
	}

	public function getCallbackURL(){
		LogInfoHandler::log();
		return $this->_callback_URL;
	}

	public function getRedirectionCause() {
		LogInfoHandler::log();
		return $this->_redirection_cause;
	}

	public function getRedirectionHeaderCode() {
		LogInfoHandler::log();
		return $this->_redirection_header_code;
	}

	public function getRedirectionLocation() {
		LogInfoHandler::log();
		return $this->_redirection_location;
	}

	public function getRedirectionType() {
		LogInfoHandler::log();
		return $this->_redirection_type;
	}

	public function getUserDataArray(){
		LogInfoHandler::log();
		return $this->_user_data_array;
	}

	public function __destruct(){
		LogInfoHandler::log();
	}
}
