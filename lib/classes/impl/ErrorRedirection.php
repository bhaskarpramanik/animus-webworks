<?php
/**
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 * 
 * Description:
 * Simple class handling error redirection
 */
class ErrorRedirection extends PermanentRedirection{
    
	/*
	 * Contain the redirection flag
	 */
    private $_redirectionFlag;
    
    public function __construct() {
        $this->_redirectionFlag = false;
    }
    
    public function getCallbackURL() {
        return parent::getCallbackURL();
    }

    public function getInitialURL() {
        return parent::getInitialURL();
    }

    public function getRedirectionCause() {
        return parent::getRedirectionCause();
    }

    public function getRedirectionFlag() {
        return $this->_redirectionFlag;
    }

    public function getRedirectionHeaderCode() {
        return parent::getRedirectionHeaderCode();
    }

    public function getRedirectionLocation() {
        return parent::getRedirectionLocation();
    }

    public function getRedirectionType() {
        return parent::getRedirectionType();
    }

    public function setCallbackURL( $callback_url ) {
        parent::setCallbackURL( $callback_url );
    }

    public function setInitialURL( $initial_url ) {
        parent::setInitialURL( $initial_url );
    }

    public function setRedirectionCause( $redirect_cause ) {
        parent::setRedirectionCause( $redirect_cause );
    }

    public function setRedirectionFlag() {
        $this->_redirectionFlag = REDIRECT_404;
    }

    public function setRedirectionHeaderCode( $header_code ) {
        parent::setRedirectionHeaderCode( $header_code );
    }

    public function setRedirectionLocation( $redirect_location ) {
        parent::setRedirectionLocation( $redirect_location );
    }

    public function setRedirectionType( $redirect_type ) {
        parent::setRedirectionType( $redirect_type );
    }

    public function stackHTTPHeaders() {
        parent::stackHTTPHeaders();
    }
}
