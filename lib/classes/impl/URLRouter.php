<?php

/**
 * 
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 *
 * Description URLRouter
 * This class routes the incoming requests and routes towards the proper
 */

require_once CLASSPATH_IMPL.'/XMLParser.php';
class URLRouter {
	private $_deployed_URLs;
	
	public function __construct(){
		LogInfoHandler::log();
		$this->_deployed_URLs = array();
		$deployment_file_path = DEPLOYMENT_CONFIG_PATH;
		$deployment_file_name = DEPLOYMENT_CONFIG_NAME;
	}
	
	public function stackDeployedURL( $mapped_pattern, $mapped_view ){
		LogInfoHandler::log();
		$this->_deployed_URLs[$mapped_pattern] = $mapped_view;
	}
	
	public function loadDeploymentMap(){
		/*
		* Deployment descriptor file name is defined as: DEPLOYMENT_CONFIG_NAME
		* Deployment descriptor file path is defined as: DEPLOYMENT_CONFIG_PATH
		* Create an instance of XMLParser and load all the given URLs
		*/
		LogInfoHandler::log();
		$XMLParser = new XMLParser();
		$isXMLParserReady = $XMLParser->setXMLParams( DEPLOYMENT_CONFIG_PATH, DEPLOYMENT_CONFIG_NAME );
		
		/*
		* loadXML method of XMLParser class throws exception,
		* hence is called from inside a try block and exceptions are caught in a catch block
		*/
		try{
		  /*
			* Step-1: Verify if the deployment file is present at the given path
			*/
		  $file_exists = file_exists( DEPLOYMENT_CONFIG_PATH.DEPLOYMENT_CONFIG_NAME );
		  if( false === ( bool )$file_exists ){
			 throw new ConfigurationException( DEPLOYMENT_CONFIG_NAME.' was not found in '.DEPLOYMENT_CONFIG_NAME.'| Unable to find deployed URLs!' );
		  }
		  /*
			* Step-2: Verify if XMLParser is ready, if yes then start processing the URLs
			*/
		  else{
			 $XMLParser->loadXML();
			 LogInfoHandler::log( 'Reading deployed URLs!' );
			 while( @$XMLParser->read() ){
				if( $XMLParser->isTagStartElement() ){
					$tagName = $XMLParser->localName;
					switch( $tagName ){
						case( 'view' ):{
						  $view_name    = $XMLParser->getAttribute( 'name' );
						  $view_pattern = $XMLParser->getAttribute( 'url-pattern' );
						  LogInfoHandler::log( 'ViewName: '.$view_name );
						  LogInfoHandler::log( 'ViewPattern: '.$view_pattern );
						  $this->stackDeployedURL( $view_pattern, $view_name );
						}
						break;
					}
				}
			 }
			 return true;
		  }
		} catch ( Exception $ex ) {
		  $ex->log();
		  return false;
		}
	}
	
	public function getDeployedURLList(){
		LogInfoHandler::log();
		return $this->_deployed_URLs;
	}
}
