<?php
/*
 * Copyright 2018 Bhaskar Pramanik <splashingbee@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 * 
 * Description
 * Master implementation of Redirection interface
 */
require_once 'Session.php';
require_once CLASSPATH_ABSTRACT . '/Redirection.php';
class RedirectionImpl implements Redirection{

	public function setInitialURL( $initial_url ) { }
	public function setRedirectionCause( $redirect_cause ) { }
	public function setRedirectionHeaderCode( $redirect_header_code ) { }
	public function setRedirectionLocation( $redirect_location ) { }
	public function setRedirectionType( $redirect_type ) { }

	public function setCallbackURL( $callback_url ) { 
		// Set the callback URL inside the session container for persistence
		LogInfoHandler::log();
		$sessObj = new Session();
		$session = $sessObj->getSession();
		$session->setCallbackURL($callback_url);
		$session->updateSession();
	}

	public function setFormData(array $form_data) {
		LogInfoHandler::log();
		$sessObj = new Session();
		$session = $sessObj->getSession();
		$session->setRedirectionFormData($form_data);
		$session->updateSession();
	}

	public function getFormData() {
		LogInfoHandler::log();
		return $this->_formData;
	}
}
