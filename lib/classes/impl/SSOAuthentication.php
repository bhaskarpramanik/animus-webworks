<?php

/*
 * Copyright 2018 Bhaskar Pramanik <splashingbee@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Description of SSOAuthentication extends AuthenticationImpl
 * Implements SSOAuthentication functions
 *
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 *
 */
require_once CLASSPATH_IMPL.'/AuthenticationImpl.php';
class SSOAuthentication extends AuthenticationImpl {
	
	// Holds the url of the identity provider
	private $_idPUrl;
	
	// Holds the callback URL to handle the SSO response
	private $_ssoResponseUrl;
	
	// Set the handler in response mode
	private $_ssoResponsePhase;
	
	// Holds the authentication options array
	private $_authOptions;
	
	public function __construct() {
		LogInfoHandler::log();
		parent::__construct();
	}
	
	public function setIdpUrl( $idpUrl) {
		LogInfoHandler::log();
		$this->_idPUrl = $idpUrl;
	}
	
	public function getIdpUrl() {
		LogInfoHandler::log();
		return $this->_idPUrl;
	}
	
	public function setSsoResponseUrl( $ssoResponseUrl ) {
		LogInfoHandler::log();
		$this->_ssoResponseUrl = $ssoResponseUrl;
	}

	public function setAssertAuthArray( $assertAuthArray ) {
		LogInfoHandler::log();
		parent::setAssertAuthArray($assertAuthArray);
	}

	public function setAuthVerdict($authVerdict) {
		LogInfoHandler::log();
		parent::setAuthVerdict($authVerdict);
	}

	public function setUserAuthArray( $userAuthArray ) {
		LogInfoHandler::log();
		parent::setUserAuthArray( $userAuthArray );
	}
	
	public function getAuthArtifacts() {
		LogInfoHandler::log();
		return parent::getAuthArtifacts();
	}

	public function getAuthVerdict() {
		LogInfoHandler::log();
		return parent::getAuthVerdict();
	}
	
	public function setAuthOptions( $options ) {
		LogInfoHandler::log();
		// Overridden in child
	}
	
	public function __destruct() {
		LogInfoHandler::log();
		parent::__destruct();
	}

}
