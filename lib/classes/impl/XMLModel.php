<?php
/**
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 * Description of XMLModel
 * XMLModel is an implementation of the Model class, which works with XML input.
 * XML can be passed as URL or as a file
 * By default, XMLModel shall always runs in file mode
 */

class XMLModel extends ModelImpl{
	
	private $_XMLFilemode;
	private $_XMLPath;
	private $_XMLURL;
	private $_XMLName;
	private $_inputKeys;
	private $_dataStructure; // Associative array to map
	
	public function __construct() {
		parent::__construct();
		$this->_XMLFilemode = true;
		$this->_XMLPath = '';
		$this->_inputKeys = array();
		$this->_XMLURL = '';
		$this->_dataStructure = array();
		parent::setXMLWorkflow();
	}

	public function callWorkflow() {
		$this->defineWorkflow();
	}

	public function defineWorkflow() {
		// Workflow definition for XML processing
		// Define all variables
		$dataStructure = $this->_dataStructure;
		$nodeNameArray = array_keys( $dataStructure );
		$innerText     = '';
		
		$XMLParser = new XMLParser();
		$XMLPath   = $this->getWorkflowArtifact( 'inputPath' );
		$XMLURL    = @$this->getWorkflowArtifact( 'inputURL' );	
		$XMLName   = @$this->getWorkflowArtifact( 'inputFileName' );
		$isURL     = !$this->getWorkflowArtifact( 'fileMode' );
		$isParserReady = $XMLParser->setXMLParams( $XMLPath, $XMLName, $isURL, $XMLURL );
		if( $isParserReady ){
		  $tempOutArray = array();
		  foreach( $nodeNameArray as $node ){
			$XMLParser->loadXML();
			$attrArray = $dataStructure[$node];
			$temp = array();
			$attrOutArray = array();
			while( $XMLParser->read() ){
				if( $XMLParser->isTagStartElement() ){
					if( $XMLParser->localName == $node ){
					  if( 0 !== sizeof( $attrArray ) ){
						 foreach( $attrArray as $attr ){
							 $attrValue           = $XMLParser->getAttributeByName( $attr );
							 $attrOutArray[$attr] = $attrValue;
							 $innerText = $XMLParser->readInnerXML();
							 if( 0 !== strlen( $innerText ) ){
								$temp['innerText'] = $innerText;
							 }
						 }
						 array_push( $temp, $attrOutArray );
					  }
					  else{
						 $innerText         = $XMLParser->readInnerXML();
						 $temp['innerText'] = $innerText;
					  }
					}
					else{
					  //Nothing to do here
					}
				}
			}
			$tempOutArray[$node] = $temp;
		  }
		}
		return $tempOutArray;
	}

	public function defineWorkflowArtifacts() {
		// Overridden here
		$isURL   = false;
		$XMLPath = $this->getXMLPath();
		$XMLURL  = null;
		$XMLName = null;
		
		
		if( $this->isFileModeOperation() == false ){
		  $XMLPath = null;
		  $XMLURL  = $this->getXMLURL();
		  $this->setWorkflowArtifact( 'inputURL', $this->getXMLURL() );
		  $this->setWorkflowArtifact( 'inputPath', null );
		  
		}
		else{
		  $XMLPath = $this->getXMLPath();
		  $this->setWorkflowArtifact( 'inputPath', $this->getXMLPath() );
		  $this->setWorkflowArtifact( 'inputURL', null );
		  $this->setWorkflowArtifact( 'inputFileName', $this->getXMLName() );
		}
		$this->setWorkflowArtifact( 'fileMode', $this->isFileModeOperation() );
	}

	public function getDatasetArray() {
		return parent::getDatasetArray();
	}

	public function getDatasetByName( $datasetName ) {
		return parent::getDatasetByName( $datasetName );
	}

	public function initModel() {
		parent::initModel();
	}

	public function isErrorFlagSet() {
		return parent::isErrorFlagSet();
	}

	public function loadWorkflowArtifacts() {
		parent::loadWorkflowArtifacts();
	}

	public function processError() {
		parent::processError();
	}

	public function processWorkflow() {
		parent::processWorkflow();
	}

	public function pushError( $errorText ) {
		parent::pushError( $errorText );
	}

	public function setDatabaseWorkflow() {
		parent::setDatabaseWorkflow();
	}

	public function setDataset( $datasetName, \Dataset $Dataset ) {
		parent::setDataset( $datasetName, $Dataset );
	}

	public function setErrorFlag() {
		parent::setErrorFlag();
	}

	public function setJSONWorkflow() {
		parent::setJSONWorkflow();
	}

	public function setXMLWorkflow() {
		parent::setXMLWorkflow();
	}
	
	public function setFileModeOperation(){
		$this->_XMLFilemode = true;
	}
	
	public function setURLModeOperation(){
		$this->_XMLFilemode = false;
	}
	
	public function isFileModeOperation(){
		return $this->_XMLFilemode;
	}
	
	public function getXMLPath() {
		return $this->_XMLPath;
	}

	public function setXMLPath( $XMLPath ) {
		$this->_XMLPath = $XMLPath;
	}
	
	public function setInputKeyArray( $inputKeyArray ){
		$this->_inputKeys = $inputKeyArray;
	}
	
	public function setIntputKey( $inputKeyName ){
		array_push( $this->_inputKeys, $inputKeyName );
	}
	
	public function getWorkflowArtifact( $artifactName ) {
		return parent::getWorkflowArtifact( $artifactName );
	}

	public function setWorkflowArtifact( $artifactName, $artifact ) {
		parent::setWorkflowArtifact( $artifactName, $artifact );
	}
	
	public function getXMLURL() {
		return $this->_XMLURL;
	}

	public function setXMLURL( $XMLURL ) {
		$this->_XMLURL = $XMLURL;
	}
	
	public function defineStructure( $structureArray ){
		$this->_dataStructure = $structureArray;
	}
	
	public function setXMLName( $xmlName ){
		$this->_XMLName = $xmlName;
	}
	
	public function getXMLName(){
		return $this->_XMLName;
	}
}