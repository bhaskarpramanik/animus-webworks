<?php

/**
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 *
 * Description
 * The start point of the AnimusWebApp
 * 1. Initializes logger.
 * 2. Sets AnimusWebApp environment
 * 3. Starts the app.
 */


// Disable logging by default
if ( !defined ( "LOG_EVENT" ) ) {
	define ( "LOG_EVENT", false );
}

require_once 'LogInfoHandler.php';
require_once 'EventLogger.php';
require_once 'LifeCycleManager.php';
require_once 'Bootloader.php';

class AnimusWebApp{

	/**
	 * @since 1.0
	 * @return void
	 * 
	 * Description
	 * Create an instance of LifeCycleManager and initialize the application lifecycle
	 */
    public static function startApp () {        
        // Load animus environment
        $LifeCycleManager = new LifeCycleManager ();
        $LifeCycleManager->initAppLifecycle ();
    }
}
