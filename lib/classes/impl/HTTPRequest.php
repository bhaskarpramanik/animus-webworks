<?php
/**
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 * 
 * Description: 
 * Enable controlled access to server folder paths (for allowed users), while restricting generic access
 */
class HTTPRequest {
    
    /*
	 * Contain complete URL
	 */
	private $_url;

	/*
	 * Contain the URL end segment
	 */
    private $_url_end_segment;

	/*
	 * Contain GET array
	 */
    private $_input_get_array;

	/*
	 * Contain POST array
	 */
    private $_input_post_array;

	/*
	 * Contain the query string passed with URL
	 */
    private $_server_query_string_array;

	/*
	 * Custom input array
	 */
    private $_custom_input_array; // Handles input for content-type declared items

	/*
	 * Contain form data
	 */
    private $_form_data;

	/*
	 * Contain form data set flag
	 */
    private $_form_data_set;

	/*
	 * Contain session array
	 */
    private $_session;

	/*
	 * Contain server protocol
	 */
    private $_server_protocol;

	/*
	 * Contain if folder request flag
	 */
    private $_is_folder_path_request;
    
    
    public function __construct() {
		
		/*
		 * Initialize instance variables
		 */
        LogInfoHandler::log();
        $this->_input_get_array           = array();
        $this->_input_post_array          = array();
        $this->_server_query_string_array = array();
        $this->_custom_input_array        = array();
        $this->_form_data                 = array();
        $this->_form_data_set             = false;
        $this->_is_folder_path_request    = false;
    }
    
    public function loadInputParams() {
        LogInfoHandler::log();
                            
        // The requested URL is set to the request object
        $this->setRequestURI();
        
        // The end slice of the requested URL is also saved
        $this->setURLEndSegment();
        
         // Input parameters are loaded on to the request object
        if(!empty( $_GET ) ) {
            $this->populateGETArray();
        }
        else if(!empty( $_POST ) ) {
            $this->populatePOSTArray();
        }
        else if(!empty( $_SERVER['QUERY_STRING'] ) ) {
           $this->populateQueryString(); 
        }
        else{
            /*
             * Bug: The loading of input params fail, in case of JSONs
             * Solution: 
             * JSON must be serialized and sent from the sending end, alongwith a header >> 'Content-Type':'application/json'
             * On the server end once the JSON is received, it must be de-serialized after checking the 'Content-Type' header
             */
            if( @$_SERVER['CONTENT_LENGTH'] > 0 ) {
                $contentType = $_SERVER['CONTENT_TYPE'];
                LogInfoHandler::log( 'Content type >> '.$contentType );
                switch( $contentType ){
					case( $contentType == JSON_CONTENT ): {
                        LogInfoHandler::log(file_get_contents('php://input'));
                        $this->populateCustomInputArray( json_decode( file_get_contents( 'php://input' ), true ) );
                    }
					
                    break;
				
                }
            }
            else {
                LogInfoHandler::log('Content length received > '.@$_SERVER['CONTENT_LENGTH']);
            }
        }
        $this->setFormDataFlag();
        $this->setFormData();
    }
    
    public function populateGETArray() {
        $this->setInputGETArray( $_GET );
       LogInfoHandler::log( $this->_input_get_array );
    }
    
    public function populatePOSTArray() {
        $this->setInputPOSTArray( $_POST );
       LogInfoHandler::log( $this->_input_post_array );
    }

    public function populateQueryString(){
        $this->setInputQueryString( $_SERVER['QUERY_STRING'] );
        LogInfoHandler::log( $this->_server_query_string_array );
    }
    
    public function populateCustomInputArray( $input ) {
        $this->setCustomInputArray( $input );
    }
    
    public function getInputGETArray() {
       LogInfoHandler::log();
        return $this->_input_get_array;
    }

    public function setInputGETArray( $input_get_array ) {
        LogInfoHandler::log();
        foreach( $input_get_array as $key => $value ) {
            if(!empty( $value ) ) {
                 $this->_input_get_array[$key] = $value;
                }
            else{
                $this->_input_get_array[$key]  = $value;
            }
        }
    }

    public function setInputQueryString( $input_server_query_string ) {
       LogInfoHandler::log();
        $query_received = urldecode( $input_server_query_string );
        array_push( $this->_server_query_string_array, $query_received );
    }

    public function getInputPOSTArray() {
       LogInfoHandler::log();
        return $this->_input_post_array;
    }

    public function setInputPOSTArray( $input_post_array ) {
       LogInfoHandler::log();
        foreach( $input_post_array as $key=>$value ) {
            if( is_array( $value ) ) {
                $this->_input_post_array[$key]  = $value;
            }
            else{
                 $this->_input_post_array[$key] = $value;
            }
        }
    }
    
    public function injectSession( Session $sessionObj ) {
       LogInfoHandler::log();
        $this->_session = $sessionObj;
    }

    public function setRequestURI() {
        $requestedURI = $_SERVER['REQUEST_URI'];
        // Changes to this function to filter a requested URL and leave out the passed inline GET parameters
        $filterPattern = '#[^\?]*#'; // Return everything till the ?-character from the begining of the URL string
        $match = array();
        $endURISegment = '';
        preg_match($filterPattern, $requestedURI, $match);
            if( strpos($match[0], '/') ) {
                $filteredURL = substr($match[0],0,-1);
            }
            else{
                $filteredURL = $match[0];
            }
        // Set this filtered URL to the instance
            $this->_url = $filteredURL;
       LogInfoHandler::log($this->_url);
    }

    public function getRequestURI() {
        LogInfoHandler::log();
        return $this->_url;
    }
    
    public function setServerProtocol() {
       LogInfoHandler::log( $_SERVER['SERVER_PROTOCOL'] );
        $this->_server_protocol = $_SERVER['SERVER_PROTOCOL'];
    }
    
    public function getServerProtocol() {
       LogInfoHandler::log();
        return $this->_server_protocol;
    }

    public function getCustomInputArray() {
        return $this->_custom_input_array;
    }

    public function setCustomInputArray( $custom_input_array ) {
        array_push( $this->_custom_input_array, $custom_input_array );
    }

    public function setURLEndSegment() {
       LogInfoHandler::log();
        $capturePattern = '/([^\/]+)\.?$/';
        $requestedURI = $this->getRequestURI();
        $match = array();
        $endURISegment = '';
        preg_match( $capturePattern, $requestedURI, $match );
        if(sizeof( $match )!=0 ) {
            if(strpos($match[0], '/')){
                $endURISegment = substr( $match[0],0,-1) ;
            }
            else{
                $endURISegment = $match[0];
            }
           LogInfoHandler::log( 'End URI segment >> ' . $endURISegment );
            $this->_url_end_segment = $endURISegment;
            preg_match( '#(\.[a-z]{1,})#', $requestedURI, $matched_array );
            if( isset( $matched_array[0] ) ) {
                // URL ends in a file extension
               LogInfoHandler::log( 'URL ends in file extension' );
            }
            else{
                // URL doesn't end in a file extension or in a trailing slash. Considered as a system path
               LogInfoHandler::log( 'URL doesn\'t end in a file extension'  );
                $this->serverPathURIRequest();
            }
        }
        else {
                // URL ends in a trailing slash. Considered a system path.
               LogInfoHandler::log( 'No end segment filtered!' );
                // The URL requested could be of a folder!
                $this->serverPathURIRequest();
        }
                    
    }
    
    public function getURLEndSegment() {
        LogInfoHandler::log();
        return $this->_url_end_segment;
    }
    
    public function setFormData() {
       LogInfoHandler::log();
        $this->_form_data = array_merge( $this->_input_get_array, $this->_input_post_array, $this->_server_query_string_array, $this->_custom_input_array );
    }
    
    public function getFormData() {
        return $this->_form_data;
    }
    
    public function isFormDataSet() {
        return $this->_form_data_set;
    }

    public function setFormDataFlag() {
        $this->_form_data_set = true;
    }

    public function serverPathURIRequest() {
        $this->_is_folder_path_request = true;
    }
    
    public function isFolderPathRequest() {
        return $this->_is_folder_path_request;
    }
}
