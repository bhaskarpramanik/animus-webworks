<?php
/**
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 */

class LogInfoHandler{ // Static class
	public static function logException( $file, $line, $message ){
	   @$_eventLogger = new EventLogger( ERROR_LOG );
		$_eventType = '[EXCEPTION]';
		$_file_line = 'File: '.$file.'|'.'Line: '.$line.'|';
		$_eventLogger -> logMessage($_eventType.' @ '.$_file_line.serialize($message));
		unset($_eventLogger);
	   @$_eventLogger = new EventLogger(DEBUG_LOG); 
		$_eventLogger -> logMessage($_eventType.' @ '.$_file_line.$message);
		unset($_eventLogger);
	}

	public static function log( $message=null ){
		if( ( bool ) LOG_EVENT ) {
			@$_eventLogger       = new EventLogger( DEBUG_LOG );
			$_eventType          = '[DEBUG]';
			$_methodName         = debug_backtrace()[1]['function'];
			$_lineNo             = debug_backtrace()[1]['line'];
			$_className          = debug_backtrace()[1]['class'];
			$_args               = debug_backtrace()[1]['args'];
			$_identifier         = 'Class:' . $_className . '|Line:' . $_lineNo . '|Method:' . $_methodName . '|Input:' . serialize($_args);
//            $carriage_adjustment = '\r\n                              ';
			if(!is_null($message)){
				$_eventLogger -> logMessage( $_eventType . ' @ Class:' . $_className . '|Line:' . $_lineNo . '|Method:' . $_methodName . '|' . serialize( $message ) );
			}
			else{
				$_eventLogger -> logMessage( $_eventType . ' @ ' . $_identifier );
			}
			unset( $_eventLogger );
		}
		else{
			// Do Nothing
		}
	}

	public static function error_log( $message=null ){
		$_eventLogger = new EventLogger( DEBUG_LOG );
		$_eventType    = '[DEBUG]';
		$_methodName   = debug_backtrace()[1]['function'];
		$_lineNo       = debug_backtrace()[1]['line'];
		$_className    = debug_backtrace()[1]['class'];
		
		$_identifier         = sprintf( 'Class: %s | Line: %s | Method: %s | Input: %s', 
				$_className,
				$_lineNo,
				$_methodName,
				self::pickle( $message )
			);
		if( !is_null( $message ) ){
			$_eventLogger -> logMessage( $_eventType . ' @ ' . $_identifier );
		}
		else{
			$_eventLogger -> logMessage( $_eventType . ' @ ' . $_identifier );
		}
		unset( $_eventLogger );
	}

	public static function pickle( $_args ) {
		try{
			$_pickled = serialize( $_args );
			return $_pickled;
		} catch (Exception $ex) {
			if( gettype( $_args ) !== "array" ) {
				return get_class( $_args );
			}
		}
	}
}

