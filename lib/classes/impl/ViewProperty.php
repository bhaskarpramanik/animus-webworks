<?php

/**
 * Description of ViewProperty
 *
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 * Description of ViewProperty
 * This class provides an object to hold the defined properties of the view in question and can be manipulated using the
 * ViewPropertyHandler
 */

class ViewProperty {
	private $_authRequired;
	private $_isDynamic;
	private $_needViewParams;
	private $_viewParams;
	private $_authViewURL;
	private $_authSignature;
	private $_viewRedirected;
	private $_viewRedirectCode;
	private $_viewRedirectRescURL;
	private $_phaseMaint;
	private $_phaseMaintStart;
	private $_phaseMaintStop;
	private $_mappedController;
	private $_mappedModels;
	private $_mappedDataset;
	private $_loadStaticAssets;
	private $_mappedStaticAssets;
	private $_overrideDefaultStaticLoading;
	
	public function __construct() {
	  LogInfoHandler::log();
	}
	
	public function getAuthRequired() {
	  LogInfoHandler::log();
		return $this->_authRequired;
	}
	
	public function getViewParams() {
	  LogInfoHandler::log();
		return $this->_viewParams;
	}

	public function getAuthViewURL() {
	  LogInfoHandler::log();
		return $this->_authViewURL;
	}

	public function getAuthSignature() {
	  LogInfoHandler::log();
		return $this->_authSignature;
	}

	public function getViewRedirected() {
	  LogInfoHandler::log();
		return $this->_viewRedirected;
	}

	public function getViewRedirectCode() {
		LogInfoHandler::log();
		return $this->_viewRedirectCode;
	}

	public function getViewRedirectRescURL() {
		LogInfoHandler::log();
		return $this->_viewRedirectRescURL;
	}

	public function getPhaseMaint() {
	  LogInfoHandler::log();
		return $this->_phaseMaint;
	}

	public function getPhaseMaintStart() {
		LogInfoHandler::log();
		return $this->_phaseMaintStart;
	}

	public function getPhaseMaintStop() {
		LogInfoHandler::log();
		return $this->_phaseMaintStop;
	}

	public function getMappedControllers() {
	  LogInfoHandler::log();
		return $this->_mappedController;
	}

	public function getMappedModels() {
		LogInfoHandler::log();
		return $this->_mappedModels;
	}

	public function getMappedDatasets() {
	  LogInfoHandler::log();
		return $this->_mappedDataset;
	}

	public function needViewParams( $isNeeded ) {
	  LogInfoHandler::log();
		$this->_needViewParams = $isNeeded;
	}
	
	public function isViewParamAllowed(){
		return $this->_needViewParams;
	}
	public function setAuthRequired( $authRequired ) {
	  LogInfoHandler::log();
		$this->_authRequired = $authRequired;
	}

	public function setViewParams( $viewParams ) {
	  LogInfoHandler::log();
		$this->_viewParams = $viewParams;
	}

	public function setAuthViewURL( $authViewURL ) {
		LogInfoHandler::log();
		$this->_authViewURL = $authViewURL;
	}

	public function setAuthSignature( $authSignature ) {
	  LogInfoHandler::log();
		$this->_authSignature = $authSignature;
	}

	public function setViewRedirected( $viewRedirected ) {
	  LogInfoHandler::log();
		$this->_viewRedirected = $viewRedirected;
	}

	public function setViewRedirectCode( $viewRedirectCode ) {
	  LogInfoHandler::log();
		$this->_viewRedirectCode = $viewRedirectCode;
	}

	public function setViewRedirectRescURL( $viewRedirectRescURL ) {
		LogInfoHandler::log();
		$this->_viewRedirectRescURL = $viewRedirectRescURL;
	}

	public function setPhaseMaint( $phaseMaint ) {
	  LogInfoHandler::log();
		$this->_phaseMaint = $phaseMaint;
	}

	public function setPhaseMaintStart( $phaseMaintStart ) {
		LogInfoHandler::log();
		$this->_phaseMaintStart = $phaseMaintStart;
	}

	public function setPhaseMaintStop( $phaseMaintStop ) {
		LogInfoHandler::log();
		$this->_phaseMaintStop = $phaseMaintStop;
	}

	public function setMappedControllers( $mappedController ) {
	  LogInfoHandler::log();
		$this->_mappedController = $mappedController;
	}

	public function setMappedModels( $mappedModels ) {
	  LogInfoHandler::log();
		$this->_mappedModels = $mappedModels;
	}

	public function setMappedDatasets( $mappedDataset ) {
		LogInfoHandler::log();
		$this->_mappedDataset = $mappedDataset;
	}
	
	public function setViewAsDynamic( $isDynamic ) {
		LogInfoHandler::log();
		$this->_isDynamic = $isDynamic;
	}
	
	public function isViewDynamic(){
		LogInfoHandler::log();
		return $this->_isDynamic;
	}
	
	public function setLoadStaticAssets( $loadStaticAssets ) {
	  LogInfoHandler::log();
		$this->_loadStaticAssets = $loadStaticAssets;
	}
	
	public function getLoadStaticAssets() {
		LogInfoHandler::log();
		return $this->_loadStaticAssets;
	}
	
	public function setStaticAssets( $staticAssets ) {
	  LogInfoHandler::log();
		$this->_mappedStaticAssets = $staticAssets;
	}
	
	public function getStaticAssets(){
	  LogInfoHandler::log();
		return $this->_mappedStaticAssets;
	}
	
	public function isDefaultStaticLoadOverridden() {
		return $this->_overrideDefaultStaticLoading;
	}

	public function setOverrideDefaultStaticLoad( $overrideDefaultStaticLoading ) {
		$this->_overrideDefaultStaticLoading = $overrideDefaultStaticLoading;
	}

}
