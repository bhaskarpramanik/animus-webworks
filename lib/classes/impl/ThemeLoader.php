<?php

/**
 * Description of ThemeLoader
 *
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 */
class ThemeLoader {

	private $_css;
	private $_js;
	private $_img;
	private $_name;
	private $_author;
	private $_email;
	private $_slug;
	private $_version;
	private $_description;
	private $_path;

	public function __construct() {
		LogInfoHandler::log();
	}

	public function loadTheme( $theme_name ){
		LogInfoHandler::log();
		$theme = THEMEPATH.$theme_name;
		$this->_path = $theme;
		$xmlParser = new XMLParser();
		$isParserReady = $xmlParser->setXMLParams( THEMEPATH.$theme_name, 'manifest.xml' );
		if($isParserReady){
			$xmlParser->loadXML();
			while(@$xmlParser->read()){
				if ( $xmlParser->isTagStartElement() ){
					$current_node = $xmlParser->localName;
					switch($current_node){
						case 'name' : {
							$this->_name        = $xmlParser->readInnerXML();
						}
							break;
						case 'slug-tag': {
							$this->_slug        = $xmlParser->readInnerXML();
						}	
							break;
						case 'description': {
							$this->_description = $xmlParser->readInnerXML();
						}
							break;
						case 'author': {
							$this->_author      = $xmlParser->readInnerXML();
						}
							break;
						case 'email': {
							$this->_email       = $xmlParser->readInnerXML();
						}	
						 break;
					 
						 case 'version': {
							$this->_version     = $xmlParser->readInnerXML();
						}	
						 break;
					 
						case 'css-path': {
							$this->_css         = $xmlParser->readInnerXML();
						}
							break;
						case 'js-path' : {
							$this->_js          = $xmlParser->readInnerXML();
						}
							break;
						case 'image-path' : {
							$this->_img         = $xmlParser->readInnerXML();
						}
							break;
					}
				}
			}
		}
		unset($xmlParser);
	}

	public function getCss() {
		return $this->_css;
	}

	public function getJs() {
		return $this->_js;
	}

	public function getImg() {
		return $this->_img;
	}

	public function getName() {
		return $this->_name;
	}

	public function getAuthor() {
		return $this->_author;
	}

	public function getEmail() {
		return $this->_email;
	}

	public function getSlug() {
		return $this->_slug;
	}

	public function getVersion() {
		return $this->_version;
	}

	public function getDescription() {
		return $this->_description;
	}

	public function getThemePath() {
		return $this->_path;
	}

}
