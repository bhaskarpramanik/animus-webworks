<?php

/**
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 */

/**
 * Description of ControllerImpl
 * Implementation of the Controller class
 */
require_once CLASSPATH_ABSTRACT.'/Controller.php';
class ControllerImpl implements Controller {

	/*
	 * Contain name
	 */
	private $_name;

	/*
	 * Contain form data
	 */
	private $_formData;

	/*
	 * Form data set flag
	 */
	private $_isFormDataSet;

	/*
	 * Form data inject flag
	 */
	private $_injectFormData;

	/*
	 * Contain current user info
	 */
	private $_userInfo;

	/*
	 * Contain datasets
	 */
	private $_datasets;

	/*
	 * Contain models
	 */
	private $_models;

	/*
	 * Contain model name array
	 */
	private $_modelNameArray;

	/*
	 * Contain view sideload feature flag 
	 */
	private $_viewSideload;

	/*
	 * Contain I/O share array
	 */
	private $_ioShare;

	/*
	 * Contain I/O share flag
	 */
	private $_enableIOShare;

	/*
	 * Contain output data
	 */
	private $_outputData;

	/*
	 * Additional form data
	 */
	private $_addlFormData;

	/*
	 * App Context
	 */
	private $_appCtxt;

	/*
	 * DB connection object
	 */
	private $_dbConnObj;
	
	/*
	 * Hold any extra headers - which will be used to inject to view
	 */
	private $_outputHeaders;

	/*
	 * Delegated Callback URIs - Required in case of redirection cases
	 */
	private $_callbackURI;
	
	
	private $_calledURI;
	
	/*
	 * Callback URI Flag - Set this flag if controller needs to redirect to another location after execute()
	 */
	private $_callbackURIFlag;


	public function __construct() {
		LogInfoHandler::log();
		
		/*
		 * Initialize instance variables
		 */
		$this->_name            = get_class(debug_backtrace()[0]['object']);;
		$this->_formData        = array();
		$this->_addlFormData    = array();
		$this->_datasets        = array();
		$this->_models          = array();
		$this->_outputData      = array();
		$this->_formNameArray   = array();
		$this->_modelNameArray  = array();
		$this->_ioShare         = array();
		$this->_outputHeaders   = array();
		$this->_callbackURIFlag = false;
		$this->_injectFormData  = false;
		$this->_isFormDataSet   = false;
		$this->_enableIOShare   = false;
		$this->_viewSideload    = true;
		$this->_appContext      = false;
		$this->_calledURI       = "";
	}

	/*
	 * @override
	 */
	public function init() {
		LogInfoHandler::log();
		// Override this method in child class
	}

	/*
	 * @override
	 */
	public function service() {
		LogInfoHandler::log();
		// Override this method in child class
	}

	/*
	 * @override
	 */
	public function execute(){
		LogInfoHandler::log();
		$this->init();
		$this->service();
		$this->conclude();
	}

	/*
	 * @override
	 */
	public function conclude() {
	   LogInfoHandler::log();
		// Override in child class
	}

	/*
	 * Getters and Setters
	 */
	public function setFormData( $formData ) {
		LogInfoHandler::log();
		if ( 0 == sizeof($this->_formData) ) {
			$this->_formData = $formData;
		}
		else {
			$this->_formData = array_merge( $this->_formData, $formData );
		}
		if ( !$this->_isFormDataSet ) {
			$this->_isFormDataSet = TRUE;
		}
	}

	public function getFormData() {
		LogInfoHandler::log();
		return $this->_formData;
	}

	public function getFormDataByKey( $key ) {
		LogInfoHandler::log();
		if( 0 < sizeof($this->_formData) ){
			return $this->_formData[$key];
		}
	}

	public function setUserInfo( User $user ) {
		LogInfoHandler::log();
		$this->_userInfo = $user;
	}

	public function getUserInfo() {
		LogInfoHandler::log();
		return $this->_userInfo;
	}

	public function setDataset( $datasetName, Dataset $dataset ) {
		LogInfoHandler::log();
		$this->_datasdets[$datasetName] = $dataset;
	}

	public function getDatasetByName( $datasetName ) {
		LogInfoHandler::log();
		return $this->_datasets[$datasetName];
	}

	public function getDataset() {
		return $this->_datasets;
	}

	public function setModel( $modelName, Model $model ) {
		LogInfoHandler::log();
		$this->_models[$modelName] = $model;
	}

	public function getModelByName( $modelName ) {
		LogInfoHandler::log();
		return $this->_models[$modelName];
	}

	public function getModels() {
		LogInfoHandler::log();
		return $this->_models;
	}

	public function getOutputData() {
		LogInfoHandler::log();
		return $this->_outputData;
	}

	public function setOutputData( $outputDataKey, $outputData ) {
		LogInfoHandler::log();
		$this->_outputData[$outputDataKey] = $outputData;
	}

	public function getModelNameArray() {
		LogInfoHandler::log();
		return $this->_modelNameArray;
	}

	public function setModelName( $modelName ) {
		LogInfoHandler::log();
		array_push( $this->_modelNameArray, $modelName );
	}

	public function requireFormData() {
		LogInfoHandler::log();
		$this->_injectFormData = true;
	}

	public function isFormDataNeeded() {
		LogInfoHandler::log();
		return $this->_injectFormData;
	}

	public function isViewSideloadEnabled() {
		LogInfoHandler::log();
		return $this->_viewSideload;
	}

	public function disableViewSideload() {
		$this->_viewSideload = false;
		LogInfoHandler::log( 'View sideloading disabled!' );
	}

	public function enableViewSideload() {
		$this->_viewSideload = true;
		LogInfoHandler::log( 'View sideloading enabled!' );
	}

	public function isFormDataSet() {
		LogInfoHandler::log( $this->_isFormDataSet );
		return $this->_isFormDataSet;
	}

	public function setFormDataSetFlag() {
	   LogInfoHandler::log( 'Form data is set!' );
		$this->_isFormDataSet = true;
	}

	public function getIOShareData() {
		LogInfoHandler::log();
		return $this->_ioShare;
	}

	public function setIOShareData( $ioShareKey, $ioShareValue ) {
		LogInfoHandler::log();
		$this->_ioShare[$ioShareKey] = $ioShareValue;
	}

	public function setIOShareDataArray( $array ) {
		LogInfoHandler::log();
		$this->_ioShare = $array;
	}

	public function getName() {
		LogInfoHandler::log();
		return $this->_name;
	}

	public function setName( $name ) {
		LogInfoHandler::log();
		$this->_name = $name;
	}

	public function isIOShareEnabled() {
		LogInfoHandler::log();
		return $this->_enableIOShare;
	}

	public function enableIOShare() {
		LogInfoHandler::log();
		$this->_enableIOShare = true;
	}

}