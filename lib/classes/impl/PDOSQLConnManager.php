<?php
/**
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 * 
 * Description:
 * Simple class to handle SQL Connections
 */

class PDOSQLConnManager {
	/*
	 * Contain the connection object
	 */
	private $_connObject;

	/*
	 * Contain connection property
	 */
	private $_hostname;

	/*
	 * Contain connection property
	 */
	private $_dbname;

	/*
	 * Contain connection property
	 */
	private $_username;

	/*
	 * Contain connection property
	 */
	private $_password;

	/*
	 * Contain timestamp
	 */
	private $_timestamp;

	public function __construct() {
		
		/*
		 * Initialize everything
		 */
		$this->_connObject = null;
		$this->setHostname( DATABASE_SERVER_ADDRESS );
		$this->setDbname( DATABASE_NAME );
		$this->setUsername( DATABASE_LOGIN_UID );
		$this->setPassword( DATABASE_LOGIN_PW );
	}

	public function getHostname() {
		return $this->_hostname;
	}

	public function getDbname() {
		return $this->_dbname;
	}

	public function getUsername() {
		return $this->_username;
	}

	public function getPassword() {
		return $this->_password;
	}

	public function getConnObject() {
		$connObj = $this->_connObject;
		if( is_null( $connObj ) ) {
			$hostname    = $this->getHostname();
			$dbname      = $this->getDbname();
			$username    = $this->getUsername();
			$password    = $this->getPassword();
			$dsn         = DEFAULT_DB_TYPE . ':host=' . $hostname . ';dbname=' . $dbname;
			try{
				$connObj = new PDO( $dsn, $username, $password );
			} catch ( PDOException $ex ) {
				try{
					throw new SQLException( $ex->getMessage() );
				} catch ( SQLException $e ) {
					$e->log();
				}
			}
			$this->_connObject = $connObj;
		}
        return $this->_connObject;
    }

    public function setHostname( $hostname ) {
        $this->_hostname = $hostname;
    }

    public function setDbname( $dbname ) {
        $this->_dbname = $dbname;
    }

    public function setUsername( $username ) {
        $this->_username = $username;
    }
    
    public function getTimestamp(){
        return $this->_timestamp;
    }

    public function setPassword( $password ) {
        $this->_password = $password;
    }

}
