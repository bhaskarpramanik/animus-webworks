<?php

/**
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 * 
 * Description
 * Simple class which handles permanent redirection extends UnconditionalRedirection
 * 
 */
require_once CLASSPATH_IMPL.'/UnconditionalRedirection.php';
class PermanentRedirection extends UnconditionalRedirection {

	/*
	 * Contain the header code for redirection
	 */
	private $_header_code;
	/*
	 * Contain the redirection flag setting for only headers or page
	 */
	private $_redirection_flag;

	public function __construct() {
	   LogInfoHandler::log();
	}

	public function setRedirectionFlag(){
	   LogInfoHandler::log();
	   LogInfoHandler::log( 'Redirection to fancy error page is in effect!' );
	   $this->_redirection_flag = true;
	}
	public function stackHTTPHeaders(){
	   parent::setRedirectionHeaderCode( $this->_header_code );
	}
	public function setRedirectionHeaderCode( $header_code ){
	   LogInfoHandler::log();
	   $this->_header_code = $header_code;
	}

	public function getRedirectionFlag(){
	   LogInfoHandler::log();
	   return $this->_redirection_flag;
	}

	public function __destruct() {
	   LogInfoHandler::log();
	   parent::__destruct();
	}

	public function getCallbackURL() {
	   LogInfoHandler::log();
	   return parent::getCallbackURL();
	}

	public function getInitialURL() {
	   LogInfoHandler::log();
	   return parent::getInitialURL();
	}

	public function getRedirectionCause() {
	   LogInfoHandler::log();
	   return parent::getRedirectionCause();
	}

	public function getRedirectionHeaderCode() {
	   LogInfoHandler::log();
	   return parent::getRedirectionHeaderCode();
	}

	public function getRedirectionLocation() {
	   LogInfoHandler::log();
	   return parent::getRedirectionLocation();
	}

	public function getRedirectionType() {
	   LogInfoHandler::log();
	   return parent::getRedirectionType();
	}

	public function setCallbackURL( $callback_url ) {
	   LogInfoHandler::log();
	   parent::setCallbackURL( $callback_url );
	}

	public function setInitialURL( $initial_url ) {
	   LogInfoHandler::log();
	   parent::setInitialURL( $initial_url );
	}

	public function setRedirectionCause( $redirect_cause ) {
	   LogInfoHandler::log();
	   parent::setRedirectionCause( $redirect_cause );
	}

	public function setRedirectionLocation( $redirect_location ) {
	   LogInfoHandler::log();
	   parent::setRedirectionLocation( $redirect_location );
	}

	public function setRedirectionType( $redirect_type ) {
	   LogInfoHandler::log();
	   parent::setRedirectionType( $redirect_type );
	}

}
