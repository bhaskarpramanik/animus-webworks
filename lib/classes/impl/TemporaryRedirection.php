<?php

/**
 * Description of TemporaryRedirection extends ConditionalRedirection
 * Class to manage temporary redirection
 *
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 */
require_once CLASSPATH_IMPL.'/ConditionalRedirection.php';

class TemporaryRedirection extends ConditionalRedirection{
	private $_header_code;
	private $_redirection_flag;
	
	public function __construct() {
		LogInfoHandler::log();
		parent::__construct();
	}

	public function __destruct() {
		LogInfoHandler::log();
		parent::__destruct();
	}
	
	public function stackHTTPHeaders( $header_code ) {
		LogInfoHandler::log();
		parent::setRedirectionHeaderCode( $header_code );
	}

	public function getCallbackURL() {
		LogInfoHandler::log();
		return parent::getCallbackURL();
	}

	public function getInitialURL() {
		LogInfoHandler::log();
		return parent::getInitialURL();
	}

	public function getRedirectionCause() {
		LogInfoHandler::log();
		return parent::getRedirectionCause();
	}

	public function getRedirectionHeaderCode() {
		LogInfoHandler::log();
		return parent::getRedirectionHeaderCode();
	}

	public function getRedirectionLocation() {
		LogInfoHandler::log();
		return parent::getRedirectionLocation();
	}

	public function getRedirectionType() {
		LogInfoHandler::log();
		return parent::getRedirectionType();
	}

	public function getUserDataArray() {
		LogInfoHandler::log();
		return parent::getUserDataArray();
	}

	public function setCallbackURL( $callback_url ) {
		LogInfoHandler::log();
		parent::setCallbackURL( $callback_url );
	}

	public function setInitialURL( $initial_url ) {
		LogInfoHandler::log();
		parent::setInitialURL( $initial_url );
	}

	public function setRedirectionCause( $redirect_cause ) {
		LogInfoHandler::log();
		parent::setRedirectionCause( $redirect_cause );
	}

	public function setRedirectionHeaderCode( $redirect_header_code ) {
		LogInfoHandler::log();
		parent::setRedirectionHeaderCode( $redirect_header_code );
	}

	public function setRedirectionLocation( $redirect_location ) {
		LogInfoHandler::log();
		parent::setRedirectionLocation( $redirect_location );
	}

	public function setRedirectionType( $redirect_type ) {
		LogInfoHandler::log();
		parent::setRedirectionType( $redirect_type );
	}

	public function setUserDataArray( $user_data_array ) {
		LogInfoHandler::log();
		parent::setUserDataArray( $user_data_array );
	}
	
	public function setHeaderCode( $header_code ) {
		LogInfoHandler::log();
		$this->_header_code = $header_code;
	}
	
	public function setRedirectionFlag() {
		$this->_redirection_flag = true;
	}
	
	public function getRedirectionFlag() {
		return $this->_redirection_flag;
	}
}
