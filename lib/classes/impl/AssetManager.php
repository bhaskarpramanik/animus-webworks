<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AssetsManager
 * This class provides instances of all assets listed in the system at runtime.
 * Assets = Views, Controllers, Models, Datasets, HTMLS, CSS, JS (anything that is created by the developer)
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 */
class AssetManager {
    
    private $_content_type_header;
    
    public function __construct(){
        LogInfoHandler::log();
    }
    
    public function verifyAssets(){
        //LogInfoHandler:log();
        $XMLParser = new XMLParser();
        $XMLParser = setXMLParams(ASSETS, ASSETS_MAP);
        $XML->loadXML();
        unset($XMLParser);
        LogInfoHandler::log("Exiting ".__METHOD__);
    }
    
    /*
     * Loader for controller type assets
     */
    public function loadController($className){
        LogInfoHandler::log();
        $assetType = "controller";
        $classPath = $this->loadAsset($assetType, $className);
        if($classPath == null){
            throw new NativeException("Requested asset ".$className." was not found in assets.xml!");
        }
        else{
            try{
                require_once ASSETS_CONTROLLERS.$classPath;
                return new $className(); // Returns Object
            } catch (NativeException $ex) {
                $ex->log();
            }
        }
    }
    
    /*
     * Loader for view type assets
     */
    
    public function loadView($className){
        LogInfoHandler::log();
        $assetType = "view";
        $classPath = $this->loadAsset($assetType, $className);
        if($classPath == null){
            throw new NativeException("Requested asset ".$className." was not found in assets.xml!");
        }
        else{
            try{
                require_once ASSETS_VIEWS.$classPath;
                return new $className(); // Returns Object
            } catch (NativeException $ex) {
                $ex->log();
            }
        }
    }
    
    /*
     * Loader for model type assets
     */
    
    public function loadModel($className){
        LogInfoHandler::log();
        $assetType = "model";
        $classPath = $this->loadAsset($assetType, $className);
        if($classPath == null){
            throw new NativeException("Requested asset ".$className." was not found in assets.xml!");
        }
        else{
            try{
                require_once ASSETS_MODELS.$classPath;
                return new $className(); // Returns Object
            } catch (NativeException $ex) {
                $ex->log();
            }
        }
    }
    
    /*
     * Loader for dataset type assets
     */
    
    public function loadDataset($className){
        LogInfoHandler::log();
        $assetType = "dataset";
        $classPath = $this->loadAsset($assetType, $className);
        if($classPath == null){
            throw new NativeException("Requested asset ".$className." was not found in assets.xml!");
        }
        else{
            try{
                require_once $classPath;
                return new ASSETS_DATASETS.$className(); // Returns Object
            } catch (NativeException $ex) {
                $ex->log();
            }
        }
    }
    
    /*
     * Loader for html type assets
     */
    
    public function loadStatic($className){
        LogInfoHandler::log();
        $assetType = "static";
        // Instantiate HeaderInfoHandler and process the content_type header for static assets
        $HeaderInfoHandler = new HeaderInfoHandler();
        $fileExt = explode(".", $className);
        $classPath = $this->loadAsset($assetType, $fileExt[0]);
        /*
         * If - is triggered if partial match with requested URL happens, however the end file doesnt match!
         */
        if($classPath == null){
//            var_dump('bhaskar ---');
            $staticFoundFlag = false;
            switch(end($fileExt)){
                case ("html"):{
                    $pathAlias = constant(end($fileExt));
                    $pathSuffix = constant($pathAlias);
                    ob_start();
                    require_once $pathSuffix.$className;
                    $staticAsset = ob_get_clean();
                    $this->_content_type_header = $HeaderInfoHandler->getStaticAssetHeader(end($fileExt));
                    $staticFoundFlag = true;
                }
                break;
                case ("css"):{
                    $pathAlias = constant(end($fileExt));
                    $pathSuffix = constant($pathAlias);
                    ob_start();
                    require_once $pathSuffix.$className;
                    $staticAsset = ob_get_clean();
                    $this->_content_type_header = $HeaderInfoHandler->getStaticAssetHeader(end($fileExt));
                    $staticFoundFlag = true;
                }
                break;
                case ("js"):{
                    $pathAlias = constant(end($fileExt));
                    $pathSuffix = constant($pathAlias);
                    ob_start();
                    require_once $pathSuffix.$className;
                    $staticAsset = ob_get_clean();
                    $this->_content_type_header = $HeaderInfoHandler->getStaticAssetHeader(end($fileExt));
                    $staticFoundFlag = true;
                }
                break;
                case ("jpg"):{
                        $pathAlias = constant(end($fileExt));
                        $pathSuffix = constant($pathAlias);
                        ob_start();
                        require_once $pathSuffix.$className;
                        $staticAsset = ob_get_clean();
                        $this->_content_type_header = $HeaderInfoHandler->getStaticAssetHeader(end($fileExt));
                        $staticFoundFlag = true;
                    }
                break;
                default:{
                    // Unsupported File Type - Bad Request :(
                    throw new NativeException("Static file type requested is not supported");
                    $staticFoundFlag = false;
                    return null;
                }
            }
            if(!$staticFoundFlag){
                throw new NativeException("Content not found!");
            }
            else{
                return $staticAsset;
            }
        }
        else{
            $fileExt = explode(".", $classPath);
            $fileExt = end($fileExt);
            switch($fileExt){
                case ("html"):{
                    $pathAlias = constant($fileExt);
                    $pathSuffix = constant($pathAlias);
                    ob_start();
                    require_once $pathSuffix.$classPath;
                    $staticAsset = ob_get_clean();
                    $this->_content_type_header = $HeaderInfoHandler->getStaticAssetHeader($fileExt);
                }
                break;
                case ("css"):{
                    $pathAlias = constant($fileExt);
                    $pathSuffix = constant($pathAlias);
                    ob_start();
                    require_once $pathSuffix.$classPath;
                    $staticAsset = ob_get_clean();
                    $this->_content_type_header = $HeaderInfoHandler->getStaticAssetHeader($fileExt);
                }
                break;
                case ("js"):{
                    $pathAlias = constant($fileExt);
                    $pathSuffix = constant($pathAlias);
                    ob_start();
                    require_once $pathSuffix.$classPath;
                    $staticAsset = ob_get_clean();
                    $this->_content_type_header = $HeaderInfoHandler->getStaticAssetHeader($fileExt);
                }
                break;
                case ("jpg"):{
                        $pathAlias = constant($fileExt);
                        $pathSuffix = constant($pathAlias);
                        ob_start();
                        require_once $pathSuffix.$classPath;
                        $staticAsset = ob_get_clean();
                        $this->_content_type_header = $HeaderInfoHandler->getStaticAssetHeader($fileExt);
                    }
                break;
                default:{
                    // Unsupported File Type - Bad Request :(
                    throw new NativeException("Static file type requested is not supported");
                    return null;
                }
            }
                
        }
        return $staticAsset;
    }
    
    public function loadAsset($assetType, $className){
        LogInfoHandler::log();
        $XMLParser = new XMLParser();
        $XMLParser->setXMLParams(ASSETS, ASSETS_MAP);
        $XMLParser->loadXML();
        
        while($XMLParser->read()){
            LogInfoHandler::log("Looping through assets .xml ...");
            if($XMLParser->isTagStartElement()){
                $localName = $XMLParser->localName;
                switch($localName){
                    case $assetType: {
                        if($XMLParser->getAttributeByName("name") == $className){
                             $classPath = $XMLParser->getAttributeByName("path");
                             if(strlen($classPath) != 0) return $classPath; // Returns String
                             else return null; // Returns null
                        }   
                    } 
                    break;
                }
            }
            continue;
        }
    }
    
    public function getContent_type_header() {
        return $this->_content_type_header;
    }

    public function setContent_type_header($content_type_header) {
        $this->_content_type_header = $content_type_header;
    }
    
    public function __destruct() {
        LogInfoHandler::log();
    }
}
