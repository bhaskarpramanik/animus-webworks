<?php

/*
 * Copyright 2019 Bhaskar Pramanik <splashingbee@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Description of AnimusApp
 *
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 */
class AnimusApp {

	private $_formData;

	private $_dbConn;

	public function __construct() {
		LogInfoHandler::log();
	}

	public function register( $appConfig ) {
		LogInfoHandler::log();
		// Register the app in the Animus DB
	}

	public function activate( $appName ) {
		// 
	}

	public function getAppJson() {
		// Override in child
	}

	public function processTemplate( $array ) {
		LogInfoHandler::log();
		$templatePath = APPDATA . $array["appName"] . '/templates/' . $array['template'];
		if( file_exists( $templatePath ) ) {
			return $templatePath;
		}
		else {
			try{
				throw new ConfigurationException( sprintf( "The route: %s, is not described for the app context: %s", $array['appName'], $array['route'] ) );
			} catch ( ConfigurationException $ex ) {
				$ex->log();
				return FALSE;
			}
		}
	}

	public function setFormData( $array ) {
		LogInfoHandler::log();
		if( !is_null( $this->_formData) ) {
			if( sizeof( $this->_formData ) === 0 ) {
				$this->_formData = $array;
			}
			else{
				$this->_formData = array_merge( $this->_formData, $array );
			}
		}	
	}

	public function getFormData() {
		LogInfoHandler::log();
		return $this->_formData;
	}

	public function setDBConnObj( PDO $dbConn ) {
		LogInfoHandler::log();
		$this->_dbConn = $dbConn;
	}

	public function getDBConnObj() {
		return $this->_dbConn;
	}

	public function __destruct() {
		LogInfoHandler::log();
	}
}
