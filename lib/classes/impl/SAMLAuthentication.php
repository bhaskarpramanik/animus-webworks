<?php

/*
 * Copyright 2018 Bhaskar Pramanik <splashingbee@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Description of SAMLAuthentication extends SSOAuthentication
 * Provides framework to implement SSO Authentication 
 * 
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 */
require_once CLASSPATH_IMPL.'/SSOAuthentication.php';
require_once 'Helper.php';

class SAMLAuthentication extends SSOAuthentication {

	// Hold the SAML request XML
	private $_SAMLRequest;

	// Hold the SAML response XML
	private $_SAMLResponse;

	// Holds the samlpNamespace
	private $_samlpNamespace;

	// Holds the samlNamespace
	private $_samlNamespace;

	// Holds the saml ID
	private $_samlID;

	// Holds the SAML version no
	private $_samlVersion;

	// Holds the request instant
	private $_issueInstance;

	// Holds the SAML idP url
	private $_samlDestinationUrl;

	// Holds the assertion consumer URL
	private $_samlConsumerUrl;

	// Holds the response binding protocol
	private $_samlResponseProtocol;

	// Holds the SAML issuer URL
	private $_samlIssuer;

	// Holds the name id policy format
	private $_nameIdPolicyFormat;

	// Hold the auth options array
	private $_authenticationOptions;

	// Holds the name id policy create flag
	private $_nameIdPolicyAllowCreate;

	public function __construct() {
		LogInfoHandler::log();
		parent::__construct();
		$this->_authenticationOptions = array();
		
		// Instantiate member properties
		$this->_SAMLRequest       = '';
		$this->_SAMLResponse      = '';
		$this->_samlVersion       = '2.0';
	}

	public function getAuthVerdict() {
		LogInfoHandler::log();
		return parent::getAuthVerdict();
	}

	public function authenticate() {
		LogInfoHandler::log();
		// Redirect the user to the iDP page for login
		$destinationUrl = $this->getSamlDestinationUrl();
		$samlRequest    = $this->getSAMLRequest();
		$relayState     = $this->getSamlConsumerUrl();
		
		$redirUrl       = sprintf( '%s?SAMLRequest=%s&RelayState=%s',
									$destinationUrl,
									urlencode( base64_encode( $samlRequest ) ),
									$relayState
							);
		
		header( 'Location: ' . $redirUrl );
	}

	public function setAuthOptions( $options ) {
		LogInfoHandler::log();
		if( !is_array( $options ) ) {
			throw new NativeException( 'Expecting authentication options passed as PHP array' );
		}
		else {
			$this->_authenticationOptions = $options;
		}
	}

	public function buildSamlRequest() {
		$samlElement = __gettags( 'samlp:AuthnRequest', 
			array( 
					'xmlns:samlp'     => $this->getSamlpNamespace(),
					'xmlns:saml'      => $this->getSamlNamespace(),
					'ID'		      => $this->getSamlID(),
					'Version'         => $this->getSamlVersion(),
					'IssueInstant'    => sprintf( "%s", date( 'Y-m-d\TH:i:s\Z', $this->getIssueInstance() ) ),
					'Destination'     => $this->getSamlDestinationUrl(),
					'ProtocolBinding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
				),
			array(
					'saml:issuer'        => $this->_samlIssuer, //'http://localhost/Merlin/views/Loopback.php',
					__gettags( 'samlp:NameIDPolicy', 
						array(
							'Format' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
							'AllowCreate' => 'true',
						
						) 
					), 
				)
			);
		$this->setSAMLRequest( $samlElement );
	}
	
	/**
	 * Method to validate a SAML response
	 */
	public function validateSAMLResponse() {
		LogInfoHandler::log();		

		// Code to validate the Reference
		$xmlDoc = new DOMDocument();
		$xmlDoc->loadXML(base64_decode($this->_SAMLResponse));
		
		$xpath = new DOMXPath($xmlDoc);
		$xpath->registerNamespace( 'secdsig', $this::NAMESPACE_SECDSIG );
		$xpath->registerNamespace( 'saml2p', $this::NAMESPACE_SAML2 );
		$xpath->registerNamespace( 'saml2', $this::NAMESPACE_SAML2 );
		$doc = $xmlDoc->documentElement;
		
		// Variables
		$userData = array();
		
		// Define XPATH queries
		$XPathStatusQuery          = '/saml2p:Response/saml2p:Status/saml2p:StatusCode';
		$XPathSignatureQuery       = ".//secdsig:Signature";
		$XPathSignedInfoQuery      = ".//secdsig:SignedInfo";
		$XPathCertificateQuery     = "string(./secdsig:KeyInfo/secdsig:X509Data/secdsig:X509Certificate)";
		$XPathSignatureValueQuery  = "string(./secdsig:SignatureValue)";
		$XPathNamedAttributesQuery = "/saml2p:Response/saml2:Assertion/saml2:AttributeStatement/saml2:Attribute";
		
		$signature  = $xpath->query( $XPathSignatureQuery, $xmlDoc )->item(0);
		$signedInfo = $xpath->query( $XPathSignedInfoQuery, $xmlDoc )->item(0);
		$signatureB64 = base64_decode( $xpath->evaluate($XPathSignatureValueQuery, $signature ) );
		$signedInfoCanonicalized = $signedInfo->C14N(true, false);
		
		// Retrieve the verdict of the Auth transaction
		foreach ($xpath->query($XPathStatusQuery, $doc) as $attr) {
			if(strpos( $attr->getAttribute('Value'), "Success" ) != -1 ) {
				$this->setAuthVerdict(TRUE);
			}
			else {
				$this->setAuthVerdict(FALSE);
			}
		}
		
		// Retrieve the User data array from the response
		try{
			foreach ($xpath->query($XPathNamedAttributesQuery, $doc) as $attr) {
				foreach ($xpath->query('saml2:AttributeValue', $attr) as $value) {
					$userData[$attr->getAttribute( 'Name' )] = $value->textContent;
				}
			}
		} catch (Exception $ex) {
			throw new AuthenticationException( $ex->getMessage() );
		}
		
		$certKeyInfo = $xpath->evaluate( $XPathCertificateQuery, $signature );
		$certKeyInfoPEM = "-----BEGIN CERTIFICATE-----\n"
				. $certKeyInfo . "\n"
				. "-----END CERTIFICATE-----";
		
		// Extract the public key from the shared info
		$certPublicKey = openssl_get_publickey($certKeyInfoPEM);
		
		// Verify the signed section of the SAML transaction with extracted public key
		if( openssl_verify( $signedInfoCanonicalized, $signatureB64, $certPublicKey, "sha256WithRSAEncryption" ) == 0 ) {
			// Error happened - capture the error and log it
			$this->_integrityCheck = FALSE;
			throw new AuthenticationException( openssl_error_string() );
		}
		else{
			$this->_integrityCheck = TRUE;
			parent::setUserDataArray( $userData );
		}
		

	public function setAuthVerdict( $authVerdict ) {
		LogInfoHandler::log();
		parent::setAuthVerdict( $authVerdict );
	}

	public function __destruct() {
		LogInfoHandler::log();
		parent::__destruct();
	}

	// Specific fields for getters and setters

	function getSAMLRequest() {
		LogInfoHandler::log();
		return $this->_SAMLRequest;
	}

	function getSAMLResponse() {
		LogInfoHandler::log();
		return $this->_SAMLResponse;
	}

	function getSamlpNamespace() {
		LogInfoHandler::log();
		return $this->_samlpNamespace;
	}

	function getSamlNamespace() {
		LogInfoHandler::log();
		return $this->_samlNamespace;
	}

	function getSamlID() {
		LogInfoHandler::log();
		return $this->_samlID;
	}

	function getSamlVersion() {
		LogInfoHandler::log();
		return $this->_samlVersion;
	}

	function getIssueInstance() {
		LogInfoHandler::log();
		return $this->_issueInstance;
	}

	function getSamlDestinationUrl() {
		LogInfoHandler::log();
		return $this->_samlDestinationUrl;
	}

	function getSamlConsumerUrl() {
		LogInfoHandler::log();
		return $this->_samlConsumerUrl;
	}

	function getSamlResponseProtocol() {
		LogInfoHandler::log();
		return $this->_samlResponseProtocol;
	}

	function getSamlIssuer() {
		LogInfoHandler::log();
		return $this->_samlIssuer;
	}

	function getNameIdPolicyFormat() {
		LogInfoHandler::log();
		return $this->_nameIdPolicyFormat;
	}

	function getNameIdPolicyAllowCreate() {
		LogInfoHandler::log();
		return $this->_nameIdPolicyAllowCreate;
	}

	function setSAMLRequest( $SAMLRequest ) {
		LogInfoHandler::log();
		$this->_SAMLRequest = $SAMLRequest;
	}

	function setSAMLResponse( $SAMLResponse ) {
		LogInfoHandler::log();
		$this->_SAMLResponse = $SAMLResponse;
	}

	function setSamlpNamespace( $samlpNamespace ) {
		LogInfoHandler::log();
		$this->_samlpNamespace = $samlpNamespace;
	}

	function setSamlNamespace( $samlNamespace ) {
		LogInfoHandler::log();
		$this->_samlNamespace = $samlNamespace;
	}

	/**
	 * Description: Create the SAML ID and store it 
	 * @param type $samlID
	 * @version 1.0.0
	 */
	function setSamlID( $samlID ) {
		LogInfoHandler::log();
		$this->_samlID = sprintf( '_%s', hash( 'sha256', $samlID ) );
	}

	function setSamlVersion( $samlVersion ) {
		LogInfoHandler::log();
		$this->_samlVersion = $samlVersion;
	}

	function setIssueInstance( $issueInstance ) {
		LogInfoHandler::log();
		$this->_issueInstance = $issueInstance;
	}

	function setSamlDestinationUrl( $samlDestinationUrl ) {
		LogInfoHandler::log();
		$this->_samlDestinationUrl = $samlDestinationUrl;
	}

	function setSamlConsumerUrl( $samlConsumerUrl ) {
		LogInfoHandler::log();
		$this->_samlConsumerUrl = $samlConsumerUrl;
	}

	function setSamlResponseProtocol( $samlResponseProtocol ) {
		LogInfoHandler::log();
		$this->_samlResponseProtocol = $samlResponseProtocol;
	}

	function setSamlIssuer( $samlIssuer ) {
		LogInfoHandler::log();
		$this->_samlIssuer = $samlIssuer;
	}

	function setNameIdPolicyFormat( $nameIdPolicyFormat ) {
		LogInfoHandler::log();
		$this->_nameIdPolicyFormat = $nameIdPolicyFormat;
	}

	function setNameIdPolicyAllowCreate( $nameIdPolicyAllowCreate ) {
		LogInfoHandler::log();
		$this->_nameIdPolicyAllowCreate = $nameIdPolicyAllowCreate;
	}
}
