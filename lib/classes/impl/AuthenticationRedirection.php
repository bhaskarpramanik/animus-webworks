<?php

/*
 * /**
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 *
 */

/**
 * Description of AuthenticationRedirection
 * A class which holds the re-direction settings and objects
 */
class AuthenticationRedirection extends TemporaryRedirection {
    
    public function __construct() {
        LogInfoHandler::log();
        parent::__construct();
    }
    
    public function getCallbackURL() {
        return parent::getCallbackURL();
    }

    public function getInitialURL() {
        return parent::getInitialURL();
    }

    public function getRedirectionCause() {
        return parent::getRedirectionCause();
    }

    public function getRedirectionHeaderCode() {
        return parent::getRedirectionHeaderCode();
    }

    public function getRedirectionLocation() {
        return parent::getRedirectionLocation();
    }

    public function getRedirectionType() {
        return parent::getRedirectionType();
    }

    public function getUserDataArray() {
        return parent::getUserDataArray();
    }

    public function setCallbackURL($callback_url) {
        parent::setCallbackURL($callback_url);
    }

    public function setHeaderCode($header_code) {
        parent::setHeaderCode($header_code);
    }

    public function setInitialURL($initial_url) {
        parent::setInitialURL($initial_url);
    }

    public function setRedirectionCause($redirect_cause) {
        parent::setRedirectionCause($redirect_cause);
    }

    public function setRedirectionHeaderCode($redirect_header_code) {
        parent::setRedirectionHeaderCode($redirect_header_code);
    }

    public function setRedirectionLocation($redirect_location) {
        parent::setRedirectionLocation($redirect_location);
    }

    public function setRedirectionType($redirect_type) {
        parent::setRedirectionType($redirect_type);
    }

    public function setUserDataArray($user_data_array) {
        parent::setUserDataArray($user_data_array);
    }

    public function stackHTTPHeaders($header_code) {
        parent::stackHTTPHeaders($header_code);
    }
    public function getRedirectionFlag() {
        return parent::getRedirectionFlag();
    }

    public function setRedirectLocationFlag() {
        $causeCode = parent::getCauseCode();
        if( 401 === $causeCode ) {
            $this->_redirectLocationFlag = REDIRECT_401;
        }
        else if( 403 === $causeCode ) {
            $this->_redirectLocationFlag = REDIRECT_403;
        }
    }
}
