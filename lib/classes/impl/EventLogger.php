<?php
/**
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 * 
 * Description
 * Simple class to log messages supplied to it in string format
 */
class EventLogger {

	/*
	 * Contain the log path
	 */
	private $_log_path;
    
	/*
	 * Contain the log handle
	 */
    private $_log_handle;
    
    public function __construct( $logPath ){
        $this->setLogPath( $logPath );
        $this->getLogHandle();
        return;
    }
    
    public function setLogPath( $path ){
        $this->_log_path = $path;
        return;
    }
    
    public function getLogHandle(){
        $this->_log_handle = fopen( $this->_log_path, 'a+' );
        return;
    }
    

    public function logMessage($message){
        $date = date( 'd/m/Y H:i:s' );    // Setting the date/time format
        $log_string = $date.' '.$message.PHP_EOL;
        fwrite($this->_log_handle, $log_string);
        return;
    }
}
