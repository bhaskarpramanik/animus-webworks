<?php
/**
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 */
class MVCWrapper {
	/*
	 * Contain Controller Array
	 */
	private $_controllers;

	/*
	 * Contain Model Array
	 */
	private $_models;

	/*
	 * Contain Dataset Array
	 */
	// Dataset Array
	private $_datasets;
	// Static array
	private $_statics;
	// Static Header Array
	private $_static_header_array;
	// View Object
	private $_view;

	public function __construct(){
		LogInfoHandler::log();
		
		/*
		 * Initialize instance variables
		 */
		$this->_controllers = array();
		$this->_models = array();
		$this->_static = array();
		$this->_datasets = array();
		$this->_view = null;
		$this->_static_header_array = array();
	}

	public function addController( $assetName, Controller $Controller ){
		LogInfoHandler::log();
		$this->_controllers[$assetName] = $Controller;
	}

	public function addModel( $assetName, Model $model ){
		LogInfoHandler::log();
		$this->_models[$assetName] = $model;
	}

	public function addDataset( $assetName, Dataset $dataset ){
		LogInfoHandler::log();
		$this->_datasets[$assetName] = $dataset;
		
	}

	public function addView( View $view ){
		LogInfoHandler::log();
		$this->_view = $view;
	}

	public function addStatic( $assetName, $static ){
		LogInfoHandler::log();
		$this->_statics[$assetName] = $static;
	}

	public function getControllers() {
		return $this->_controllers;
	}

	public function getModels() {
		return $this->_models;
	}

	public function getModelByName( $modelName ){
		return $this->_models[$modelName];
	}

	public function getDatasets() {
		return $this->_datasets;
	}

	public function getStatic() {
		return $this->_statics;
	}

	public function getView() {
		return $this->_view;
	}

	public function setContentTypeHeader( $staticName, $staticHeaderString ){
		$this->_static_header_array[$staticName] = $staticHeaderString;
	}

	public function getContentTypeHeader( $staticName ){
		return $this->_static_header_array[$staticName];
	}

	public function __destruct() {
		LogInfoHandler::log();
	}
}
