<?php

/**
 * 
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 * 
 * Description
 * Collection of helper functions
 *
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 */

function __t( $string ) {
	// Function stub, for l10n and i18n modules - Futures
	return $string;
}


// Text formatting functions

function __gettags( $string, $attributeList = null, $childList = null ) {
	if( is_null( $attributeList ) ) {
		return sprintf( '<%s></%s>', $string, $string );
	}
	else{
		$tag = sprintf( '<%s', $string );
		foreach( $attributeList as $key => $value ) {
			$tag .= sprintf( ' %s="%s"', $key, $value );
		}
		
		if( is_null( $childList ) ) {
		
			return $tag .= sprintf( '></%s>', $string );
		}
		else{
			$tag .= sprintf( '>' );
			foreach( $childList as $key => $value ) {
				if( !is_numeric( $key ) ) {
					$tag .= sprintf( '<%s>%s</%s>', $key, $value, $key );
				}
				else {
					$tag .= sprintf( '%s', $value );
				}
			}
			return $tag .= sprintf( '</%s>', $string );
		}
	}
}