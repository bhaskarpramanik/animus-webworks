<?php
/**
 * @since 1.0
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 * @license  https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html GPLv2 or later
 * 
 * Description:
 * Simple class to handle maintenance redirection extends TemporaryRedirection
 */

class MaintenanceRedirection extends TemporaryRedirection{

	/*
	 * Contain redirection flag
	 */
	private $_redirectionFlag;

	public function getCallbackURL() {
	   return parent::getCallbackURL();
	}

	public function getInitialURL() {
	   return parent::getInitialURL();
	}

	public function getRedirectionCause() {
	   return parent::getRedirectionCause();
	}

	public function getRedirectionHeaderCode() {
	   return parent::getRedirectionHeaderCode();
	}

	public function getRedirectionLocation() {
	   return parent::getRedirectionLocation();
	}

	public function getRedirectionType() {
	   return parent::getRedirectionType();
	}

	public function getUserDataArray() {
	   return parent::getUserDataArray();
	}

	public function setCallbackURL( $callback_url ) {
	   parent::setCallbackURL( $callback_url );
	}

	public function setHeaderCode( $header_code ) {
	   parent::setHeaderCode( $header_code );
	}

	public function setInitialURL( $initial_url ) {
	   parent::setInitialURL( $initial_url );
	}

	public function setRedirectionCause( $redirect_cause ) {
	   parent::setRedirectionCause( $redirect_cause );
	}

	public function setRedirectionHeaderCode( $redirect_header_code ) {
	   parent::setRedirectionHeaderCode( $redirect_header_code );
	}

	public function setRedirectionLocation( $redirect_location ) {
	   parent::setRedirectionLocation( $redirect_location );
	}

	public function setRedirectionType( $redirect_type ) {
	   parent::setRedirectionType( $redirect_type );
	}

	public function setUserDataArray( $user_data_array ) {
	   parent::setUserDataArray( $user_data_array );
	}

	public function stackHTTPHeaders( $header_code ) {
	   parent::stackHTTPHeaders( $header_code );
	}

	public function getRedirectionFlag() {
	   LogInfoHandler::log($this->_redirectionFlag);
	   return $this->_redirectionFlag;
	}

	public function setRedirectionFlag() {
	   LogInfoHandler::log();
	   $this->_redirectionFlag = REDIRECT_503;
	}
}
