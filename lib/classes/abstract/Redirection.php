<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Redirection
 *
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 */
interface Redirection {
	function setRedirectionCause( $redirect_cause );
	function setRedirectionType( $redirect_type );
	function setRedirectionHeaderCode( $redirect_header_code );
	function setRedirectionLocation( $redirect_location );
	function setInitialURL( $initial_url );
	function setCallbackURL( $callback_url );
}
