<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of View
 *
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 */
interface View {
    public function output();
    public function setUserDataArray( $dataArray );
    public function setHeader( $key, $value );
}
