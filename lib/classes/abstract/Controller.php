<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Controller
 * Lists down the life cycle methods for controller
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 */
interface Controller {
	function init();
	function service();
	function execute();
	function conclude();
}