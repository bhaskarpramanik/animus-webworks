<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model:
 * Model class is upper abstraction layer over multitude of data-sources
 * Currently featured data sources include:
 * 1. SQL Database
 * 2. XML documents
 * 3. JSON documents
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 */

interface Model {
	/*
	* setModelWorkflow(array) - helps program the functionality of the model class
	*/
	function initModel();
	function callWorkflow();
	function processWorkflow();
	function processError();
	function setSQLWorkflow();
	function setXMLWorkflow();
	function setJSONWorkflow();
	function defineWorkflow();
	function loadWorkflowArtifacts();
	function setDataset($datasetName, Dataset $dataset);
	function getDatasetArray();
	function getDatasetByName($name);
}
