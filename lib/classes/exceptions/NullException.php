<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NullException
 *
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 */

require_once 'NativeException.php';
class NullException extends NativeException {
	public function __construct( $message, $code=0, Exception $previous = null ) {
		parent::__construct( $message, $code, $previous );
	}

	public function log() {
		parent::log();
	}
}
