<?php

/* 
 * author: Bhaskar Pramanik <mailto:splashingbee@gmail.com>
 * 
 * NativeExceptionHandler - extends - Exception
 */
//require_once ROOT."/lib/classes/impl/LogInfoHandler.php";

class NativeException extends Exception{
	
	// Save the log instance handler to log the messages
	
	private $_LogInfoHandler;
	
	public function __construct( $message, $code=0, Exception $previous = null ) {
		parent::__construct( $message, $code, $previous );
		$this->_LogInfoHandler = new LogInfoHandler();
	} 
	public function log(){
		$this->_LogInfoHandler->logException( $this->getFile(), $this->getLine(), $this->getMessage() );
		$this->_LogInfoHandler->logException( $this->getFile(), $this->getLine(), $this->getTraceAsString()) ;
	}
}