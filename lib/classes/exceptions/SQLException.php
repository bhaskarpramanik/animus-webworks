<?php
/*
 * author: Bhaskar Pramanik <mailto:splashingbee@gmail.com>
 * 
 */

/**
 * Description of ConfigurationException
 *
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 */

// Define exception namespace

require_once 'NativeException.php';

class SQLException extends NativeException {
	public function __construct( $message, $code=0, Exception $previous = null ) {
		parent::__construct( $message, $code, $previous );
	}

	public function log() {
		parent::log();
	}

}