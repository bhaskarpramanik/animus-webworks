<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebException
 * WebException is used to trigger exceptions to the front end, in the form of error status codes served as headers
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 */
require_once 'NativeException.php';
class WebException extends NativeException {
	public function __construct( $message, $code=0, Exception $previous = null ) {
		parent::__construct( $message, $code, $previous );
		http_response_code( 500 );
	}
	public function log() {
		parent::log();
	}
}