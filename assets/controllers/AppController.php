<?php

/*
 * Copyright 2019 Bhaskar Pramanik <splashingbee@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Description of AppController
 * This controller attempts to load the app that is supplied with the URL
 * 
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 */
class AppController extends ControllerImpl{
	private $_appName;
	private $_route;
	private $_app;
	private $_appConfig;
	private $_appOutput;
	private $_appDataKeys;
	private $_appTemplate;
	
	public function __construct() {
		parent::__construct();
		$this->getName();
		$this->requireFormData();
	}
	
	public function hasAddlDataKeys() {
		LogInfoHandler::log();
		$this->_appDataKeys = array();
		$this->_appName   = isset( $this->getFormData()['appName'] ) ? $this->getFormData()['appName'] : NULL;
		$this->_route     = isset( $this->getFormData()['route'] ) ? $this->getFormData()['route'] : NULL;
						
		// Retreive the extra data-keys required by the app to run, before the controller executes.
		if( !is_null( $this->_appName ) && !is_null( $this->_route ) ) {
			$this->setAppCtxt();
			try {
				$this->_appConfig = json_decode( file_get_contents( APPDATA . "/$this->_appName/app.json" ), TRUE );
				$this->_appTemplate = $this->_appConfig["appData"]["routes"][$this->_route]['template'];
				if( isset( $this->_appConfig["appData"]["routes"][$this->_route]["data-keys"] ) ) {
					$dataKeys           = $this->_appConfig["appData"]["routes"][$this->_route]["data-keys"];
					$this->_appDataKeys = $dataKeys;
					
					if( sizeof( $dataKeys ) > 0 ) {
						return TRUE;
					}
					else{
						return FALSE;
					}
				}
				else {
					return FALSE;
				}
			} catch ( Exception $ex ) {
				try {
					throw new NativeException( sprintf( 'The app.json file associated with this app appears missing. Re-installing the app might fix the issue.' ) );
				}
				catch ( NativeException $ex ) {
					$ex->log();
					return FALSE;
				}
			}			
		}
	}
	
	public function getAddlFormDataKeys() {
		return $this->_appDataKeys;
	}

	public function init() {
		LogInfoHandler::log();
		if( $this->hasAppCtxt() ) {
			if( file_exists( APPDATA . "/$this->_appName/$this->_appName.php" ) ) {
				require_once APPDATA . "/$this->_appName/$this->_appName.php";
				$this->_app = new $this->_appName;
				$this->_app->register( $this->_appConfig );
				if( $this->_appConfig['appData']['enableDBAccess'] ) {
					$this->_app->setDBConnObj( $this->getDBConnObj() );
				}
			}
			else{
				try{
					throw new NativeException( sprintf( 'The requested app: %s, appears absent from the system. Start point couldn\'t be found', $this->_appName ) );
				} catch ( NativeException $ex ) {
					$ex->log();
				}
			}
		}
	}

	public function service() {
		$this->_appOutput = false;
		// Check the context if app is set and proceed accordingly
		if( $this->hasAppCtxt() ) {
			// Step-1: Inject the requested data into the route App
			$appDataToInject = array();

			if( is_array( $this->_appDataKeys ) && sizeof( $this->_appDataKeys ) !== 0 ) {
				foreach( $this->_appDataKeys as $key => $value ) {
					$appDataToInject[$value] = $this->getFormData()[$value];
				}
			}
			$this->_app->setFormData( $appDataToInject );

			if( !is_null( $this->_app ) ) {
				$this->_appOutput = $this->_app->processTemplate( array( 
						"appName"  => $this->_appName, 
						"route"    => $this->_route, 
						"template" => $this->_appTemplate,
					) 
				);
			}
		}
	}
	
	public function conclude() {
		// If app output is false, then the route was not configured properly - Check exception log
		if( $this->hasAppCtxt() ) {
			if( $this->_appOutput === FALSE ) {
				$error_message = sprintf( 'The requested route: %s, isn\'t configured for the app: %s', $this->_route , $this->_appName );
				$this->setOutputData( 'ERROR: ', $error_message );
			}
			else{
				$output = require_once $this->_appOutput;				
				$this->setOutputData( 'appName', $output );
			}
		}
		else {
			$appList = $this->getAvailableApps();
			if( in_array( str_replace( ' ', '', ADMIN_APPNAME ) , array_keys( $appList ) ) ) {
				
			}
			else {
				
			}
		}
//		$this->setOutputData( "appList", $appList );
	}
	
	public function getAvailableApps() {
		// Try to load the appdata folder and verify how many apps are there
		$apps = scandir( APPDATA, 1 );
		$appdata = array_splice( $apps , 0, -2 );
		
		// List all the apps which exist
		$appList = array();
		
		// Check if the class file, and json are available
		foreach( $appdata as $key => $value ) {
			if( file_exists( APPDATA . '/'. $value . '/' . str_replace( ' ', '', $value ) . '.php' ) && file_exists( APPDATA . '/'. $value . '/' . 'app.json' ) ) {
				$appList[str_replace( ' ', '', $value)] = json_decode( file_get_contents( APPDATA . '/'. $value . '/' . 'app.json' ) );
			}
		}
		return $appList;
	}


	
	/**
	 * Check the install status of an app
	 * @param type $appName	The name of the application for which the install status is being probed for
	 */
	
	public function checkAppInstallSatatus( $appName ) {
		return true;
	}
}
