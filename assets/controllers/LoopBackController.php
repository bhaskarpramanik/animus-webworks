<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoopBackController
 *
 * @author Bhaskar Pramanik < splashingbee@gmail.com >
 */
require_once CLASSPATH_IMPL . '/SAMLAuthentication.php';
class LoopBackController extends ControllerImpl{
    public function __construct ( ) {
		parent::__construct( );
		$this->requireFormData( );
		$this->setModelName("LoopBackModel");
	}

	public function conclude ( ) {
		parent::conclude ( );
    }

	public function disableViewSideload ( ) {
		parent::disableViewSideload ( );
    }

	public function enableIOShare ( ) {
		parent::enableIOShare ( );
	}

    public function enableViewSideload ( ) {
        parent::enableViewSideload ( );
    }

	public function execute ( ) {
		parent::execute ( );
    }

	public function getDataset ( ) {
		return parent::getDataset ( );
    }

	public function getDatasetByName ( $datasetName) {
		return parent::getDatasetByName ( $datasetName);
	}

	public function getFormData ( ) {
		return parent::getFormData ( );
	}

	public function getFormDataByKey ( $key) {
		return parent::getFormDataByKey ( $key);
	}

	public function getIOShareData ( ) {
		return parent::getIOShareData ( );
	}

	public function getModelByName ( $modelName) {
		return parent::getModelByName ( $modelName);
	}

	public function getModelNameArray ( ) {
		return parent::getModelNameArray ( );
	}

	public function getModels ( ) {
		return parent::getModels ( );
	}

	public function getName ( ) {
		return parent::getName ( );
	}

	public function getOutputData ( ) {
		return parent::getOutputData ( );
	}

	public function getUserInfo ( ) {
		return parent::getUserInfo ( );
	}

	public function init ( ) {
		parent::init ( );
	}

	public function isFormDataNeeded ( ) {
		return parent::isFormDataNeeded ( );
	}

	public function isFormDataSet ( ) {
		return parent::isFormDataSet ( );
	}

	public function isIOShareEnabled ( ) {
		return parent::isIOShareEnabled ( );
	}

	public function isViewSideloadEnabled ( ) {
		return parent::isViewSideloadEnabled ( );
	}

	public function requireFormData ( ) {
		parent::requireFormData ( );
	}

	public function service ( ) {
		parent::service ( );
//		$model = $this->getModelByName("LoopBackModel");
//		$model->setQueryString("SELECT * FROM merlin.emp");
//        $model->callWorkflow();
//        $output = $model->getOutputArray();
//		
//		$model->resetModel();
//		$model->setQueryString("SELECT * FROM merlin.emp where emp_name='bhaskar'");
//        $model->callWorkflow();
//        $output = $model->getOutputArray();
//		foreach( $output as $key => $value ) {
//			$this->setOutputData( $key, $value );
//		}
		
		$formData = $this->getFormData();
		if( isset( $formData['SAMLResponse'] ) ) {
//			printf( gzinflate( urldecode(base64_decode( $formData['SAMLResponse'] ) ) ) ) ;
//			print_r( explode( '/', base64_decode( $formData['SAMLResponse'] ) ) );
			
			$xml = simplexml_load_string( base64_decode( $formData['SAMLResponse'] ) );
			$dom = dom_import_simplexml( $xml )->ownerDocument;
			$dom->formatOutput = TRUE;
			$formatted = $dom->saveXML();
			
//			echo($formatted);
			unset($xml);
			// Read the xml response using xml parser
			$xml_parser = new XMLParser();
			$xml_parser->XML(base64_decode( $formData['SAMLResponse'] ));
//			var_dump($xml_parser);
			
			while( $xml_parser->read() ) {
				if( $xml_parser->isTagStartElement() ) {
					printf( 'Node Name = %s<br/>', $xml_parser->localName );
				}
			}
			
//			LogInfoHandler::error_log(gzinflate(base64_decode(urldecode( $formData['SAMLResponse'] ) ) ) );
		}
		else {
			$samlrequesttime = time();
			$samlauth = new SAMLAuthentication();
			$samlauth->setSamlpNamespace( 'urn:oasis:names:tc:SAML:2.0:protocol' );
			$samlauth->setSamlNamespace( 'urn:oasis:names:tc:SAML:2.0:assertion' );
			$samlauth->setSamlID( $samlrequesttime . rand() );
			$samlauth->setIssueInstance( $samlrequesttime );
			$samlauth->setSamlDestinationUrl( 'https://dev-277078.oktapreview.com/app/somethingdev277078_animus_1/exkikcq6e45N1UkdR0h7/sso/saml' );
			$samlauth->setSamlIssuer( 'http://localhost/Merlin/views/Loopback.php' );
			$samlauth->setSamlConsumerUrl( 'http://localhost/Merlin/views/Loopback.php' );
			$samlauth->buildSamlRequest();
			$samlauth->authenticate();
			//printf( $samlauth->getSamlID() );
		}		
    }

    public function setDataset ( $datasetName, \Dataset $dataset) {
        parent::setDataset ( $datasetName, $dataset);
    }

    public function setFormData ( $formData) {
        parent::setFormData ( $formData);
    }

    public function setFormDataSetFlag ( ) {
        parent::setFormDataSetFlag ( );
    }

    public function setIOShareData ( $ioShareKey, $ioShareValue) {
        parent::setIOShareData ( $ioShareKey, $ioShareValue);
    }

    public function setIOShareDataArray ( $array) {
        parent::setIOShareDataArray ( $array);
    }

    public function setModel ( $modelName, \Model $model) {
        parent::setModel ( $modelName, $model);
    }

    public function setModelName ( $modelName) {
        parent::setModelName ( $modelName);
    }

    public function setName ( $name) {
        parent::setName ( $name);
    }

    public function setOutputData ( $outputDataKey, $outputData) {
        parent::setOutputData ( $outputDataKey, $outputData);
    }

    public function setUserInfo ( \User $user) {
        parent::setUserInfo ( $user);
    }

}
