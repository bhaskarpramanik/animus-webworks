<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ErrorView
 *
 * @author Bhaskar Pramanik <splashingbee@gmail.com>
 */
require_once CLASSPATH_IMPL.'/StaticView.php';
class ErrorView extends StaticView{
    public function __construct() {
        parent::__construct();
    }

    public function checkSideload() {
        return parent::checkSideload();
    }

    public function getContent() {
        return parent::getContent();
    }

    public function getHeader($key) {
        return parent::getHeader($key);
    }

    public function getHeaderArray() {
        return parent::getHeaderArray();
    }

    public function getUserDataArray() {
        return parent::getUserDataArray();
    }

    public function output() {
        parent::output();
    }

    public function setContent($content, $contentType) {
        parent::setContent($content, $contentType);
    }

    public function setHeader($key, $value) {
        parent::setHeader($key, $value);
    }

    public function setHeaderArray($headerArray) {
        parent::setHeaderArray($headerArray);
    }

    public function setNoSideload() {
        parent::setNoSideload();
    }

    public function setUserData($key, $value) {
        parent::setUserData($key, $value);
    }

    public function setUserDataArray($dataArray) {
        parent::setUserDataArray($dataArray);
    }

}
